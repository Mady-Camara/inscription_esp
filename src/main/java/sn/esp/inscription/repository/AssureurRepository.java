package sn.esp.inscription.repository;

import sn.esp.inscription.domain.Assureur;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.User;

/**
 * Spring Data  repository for the Assureur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssureurRepository extends JpaRepository<Assureur, Long> {
    Assureur findByMatriculeIgnoreCase(String matricule);

    Assureur findByUser(User user);
}
