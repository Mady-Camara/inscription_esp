package sn.esp.inscription.repository;

import sn.esp.inscription.domain.Formation;
import sn.esp.inscription.domain.Niveau;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Niveau entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NiveauRepository extends JpaRepository<Niveau, Long> {
    Niveau findByCodeNiveauIgnoreCase(String codeNiveau);
    Niveau findByLibelleLong(String libelleLong);
    List<Niveau> findByFormation(Formation formation);
}
