package sn.esp.inscription.repository;

import sn.esp.inscription.domain.AgentBiblio;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.User;

/**
 * Spring Data  repository for the AgentBiblio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentBiblioRepository extends JpaRepository<AgentBiblio, Long> {
    AgentBiblio findByMatriculeIgnoreCase(String matricule);

    AgentBiblio findByUser(User user);
}
