package sn.esp.inscription.repository;

import sn.esp.inscription.domain.Departement;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.enumeration.EnumTypeDept;

import java.util.List;

/**
 * Spring Data  repository for the Departement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DepartementRepository extends JpaRepository<Departement, Long> {
    //Departement findById(Long id);
    List<Departement> findByTypeDept(EnumTypeDept typeDept);
    Departement findByCodeDeptIgnoreCase(String codeDept);
}
