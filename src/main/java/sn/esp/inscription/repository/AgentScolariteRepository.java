package sn.esp.inscription.repository;

import sn.esp.inscription.domain.AgentScolarite;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.User;

/**
 * Spring Data  repository for the AgentScolarite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentScolariteRepository extends JpaRepository<AgentScolarite, Long> {
    AgentScolarite findByMatriculeIgnoreCase(String matricule);

    AgentScolarite findByUser(User user);
}
