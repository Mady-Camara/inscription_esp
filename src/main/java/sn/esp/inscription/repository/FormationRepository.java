package sn.esp.inscription.repository;

import sn.esp.inscription.domain.Departement;
import sn.esp.inscription.domain.Formation;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Formation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FormationRepository extends JpaRepository<Formation, Long> {
    Formation findByCodeFormationIgnoreCase(String codeFormation);
    List<Formation> findByLibelleLong(String libelleLong);
    List<Formation> findByDepartement(Departement departement);
}
