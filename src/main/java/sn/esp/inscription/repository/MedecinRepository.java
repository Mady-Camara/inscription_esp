package sn.esp.inscription.repository;

import sn.esp.inscription.domain.Medecin;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.esp.inscription.domain.User;

/**
 * Spring Data  repository for the Medecin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedecinRepository extends JpaRepository<Medecin, Long> {
    Medecin findByMatriculeIgnoreCase(String matricule);

    Medecin findByUser(User user);
}
