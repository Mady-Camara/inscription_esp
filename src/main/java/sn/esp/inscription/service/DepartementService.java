package sn.esp.inscription.service;
 
import java.util.List;
 
import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import sn.esp.inscription.domain.Departement;
import sn.esp.inscription.repository.DepartementRepository;
 
@Service
@Transactional
public class DepartementService {
     
    @Autowired
    private DepartementRepository departementRepository;
     
    public List<Departement> listAll() {
        return departementRepository.findAll();
    }
     
}