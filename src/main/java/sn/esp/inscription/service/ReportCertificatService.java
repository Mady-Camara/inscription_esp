package sn.esp.inscription.service;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import sn.esp.inscription.domain.Formation;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.domain.Niveau;
import sn.esp.inscription.repository.InscriptionRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportCertificatService {

    @Autowired
    private InscriptionRepository inscriptionRepository;

    public String exportReport() throws FileNotFoundException, JRException {
        String reportFormat = "pdf";
        String path = "src/main/java/sn/esp/inscription/jasper_files";
        List<Inscription> inscriptions = inscriptionRepository.findByEstApteTrueAndEstAssureTrueAndEnRegleBiblioTrue();
        File file = ResourceUtils.getFile("src/main/java/sn/esp/inscription/jasper_files/certificat.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(inscriptions);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Create by ", "ESP");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/certificat.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/certificat.pdf");
        }

        return "Rapport genere avec succes : " + path;
    }

    // RECU ASSURANCE

    public String exportReportUnRecuAss(Inscription inscription) throws FileNotFoundException, JRException {
        String reportFormat = "pdf";
        String path = "src/main/java/sn/esp/inscription/jasper_files";
        List<Inscription> inscriptions = new ArrayList<>();
        inscriptions.add(inscription);
        File file = ResourceUtils.getFile("src/main/java/sn/esp/inscription/jasper_files/recuAssurance.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(inscriptions);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Create by ", "ESP");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/recuAssu.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/recuAssu.pdf");
        }

        return "Rapport genere avec succes : " + path;
    }

    public String exportReportrecuAss(String reportFormat) throws FileNotFoundException, JRException {
        String path = "src/main/java/sn/esp/inscription/jasper_files";
        List<Inscription> inscriptions = inscriptionRepository.findAll();
        File file = ResourceUtils.getFile("src/main/java/sn/esp/inscription/jasper_files/recuAssurance.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(inscriptions);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Create by ", "ESP");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/recuAssu.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/recuAssu.pdf");
        }

        return "Rapport genere avec succes : " + path;
    }

    // RECU BIBLIOTHEQUE

    public String exportReportrecuBiblio(String reportFormat) throws FileNotFoundException, JRException {
        String path = "src/main/java/sn/esp/inscription/jasper_files";
        List<Inscription> inscriptions = inscriptionRepository.findAll();
        File file = ResourceUtils.getFile("src/main/java/sn/esp/inscription/jasper_files/recuBiblio.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(inscriptions);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Create by ", "ESP");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/recuBiblio.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/recuBiblio.pdf");
        }

        return "Rapport genere avec succes : " + path;
    }

    public String exportReportUnRecuBiblio(Inscription inscription) throws FileNotFoundException, JRException {
        String path = "src/main/java/sn/esp/inscription/jasper_files";
        String reportFormat = "pdf";
        List<Inscription> inscriptions = new ArrayList<>();
        inscriptions.add(inscription);
        File file = ResourceUtils.getFile("src/main/java/sn/esp/inscription/jasper_files/recuBiblio.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(inscriptions);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Create by ", "ESP");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/recuBiblio.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/recuBiblio.pdf");
        }

        return "Rapport genere avec succes : " + path;
    }

    // Formation par code departement

    public String exportReportFormation(List<Formation> formations) throws FileNotFoundException, JRException {
        String path = "src/main/java/sn/esp/inscription/jasper_files";
        String reportFormat = "pdf";
        File file = ResourceUtils.getFile("src/main/java/sn/esp/inscription/jasper_files/formation.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(formations);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Create by ", "ESP");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/formation.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/formation.pdf");
        }

        return "Rapport genere avec succes : " + path;
    }

    // Niveau par code formation
    public String exportReportNiveau(List<Inscription> inscriptions) throws FileNotFoundException, JRException {
        String path = "src/main/java/sn/esp/inscription/jasper_files";
        String reportFormat = "pdf";
        File file = ResourceUtils.getFile("src/main/java/sn/esp/inscription/jasper_files/niveau.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(inscriptions);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Create by ", "ESP");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/formation.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/niveau.pdf");
        }

        return "Rapport genere avec succes : " + path;
    }
}
