package sn.esp.inscription.service;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sn.esp.inscription.config.Constants;
import sn.esp.inscription.domain.Assureur;
import sn.esp.inscription.domain.Authority;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.repository.AssureurRepository;
import sn.esp.inscription.repository.AuthorityRepository;
import sn.esp.inscription.repository.UserRepository;
import sn.esp.inscription.security.AuthoritiesConstants;
import sn.esp.inscription.web.rest.errors.LoginAlreadyUsedException;
import sn.esp.inscription.web.rest.errors.MatriculeNotFound;

import java.util.HashSet;
import java.util.Set;

@Service
public class AssureurService {

    private final AssureurRepository assureurRepository;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    public AssureurService(AssureurRepository assureurRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository){
        this.assureurRepository = assureurRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }

    public Assureur createAssureur(Assureur assureur){
        if(assureurRepository.findByMatriculeIgnoreCase(assureur.getMatricule()) != null){
            throw new MatriculeNotFound();
        }
        assureur.setMatricule(assureur.getMatricule());

        User user = new User();
        if(userRepository.findOneByLogin(assureur.getEmail()).isPresent()){
            throw new LoginAlreadyUsedException();
        }
        user.setLogin(assureur.getEmail());
        user.setEmail(assureur.getEmail());
        String login = "";
        long i = 1;
        if(assureur.getPrenom() != null && assureur.getNom() != null){
            user.setFirstName(assureur.getPrenom());
            user.setLastName(assureur.getNom());
            login = assureur.getPrenom()+"-"+assureur.getNom();
            while(userRepository.findOneByLogin(login).isPresent()){
                i++;
                login = user.getFirstName()+"-"+user.getLastName()+""+i;
            }
            user.setLogin(login);
        }
        if (user.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(user.getLangKey());
        }
        //Encryptage mot de passe
        String EncodingPassword = passwordEncoder.encode(assureur.getMotDePasse());
        user.setPassword(EncodingPassword);
        //user.setLangKey();
        user.setActivated(true);
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.ASSUREUR).ifPresent(authorities::add);
        user.setAuthorities(authorities);
        assureur.setUser(userRepository.save(user));
        //userRepository.save(user);
        return assureurRepository.save(assureur);
    }
}
