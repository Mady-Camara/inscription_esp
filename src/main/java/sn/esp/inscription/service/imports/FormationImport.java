package sn.esp.inscription.service.imports;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sn.esp.inscription.domain.Departement;
import sn.esp.inscription.domain.Formation;
import sn.esp.inscription.repository.DepartementRepository;
import sn.esp.inscription.repository.FormationRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FormationImport {

    @Autowired
    FormationRepository formationRepository;

    @Autowired
    DepartementRepository departementRepository;

    public List<Formation> getFormation(final MultipartFile multipart) throws IOException {
        List<Formation> FormationList = new ArrayList<>();

        XSSFWorkbook workbook = new XSSFWorkbook(multipart.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 1; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                Formation formation = new Formation();

                XSSFRow row = worksheet.getRow(index);

                //Long id = (long) row.getCell(0).getNumericCellValue();
                //Formation.setId(id.toString());
                //formation.setId(id);

                //formation.setId(row.getCell(0).getNumericCellValue());
                formation.setCodeFormation(row.getCell(0).getStringCellValue());
                formation.setLibelleLong(row.getCell(1).getStringCellValue());
                //Departement departement = departementRepository.findById(row.getCell(3).getStringCellValue());
                //Departement departement = new Departement();
                //departement.setCodeDept(row.getCell(3).getStringCellValue());
                // if(departement != null){
                //     formation.setDepartement(departement);
                // }


                if(formation.getCodeFormation().equals("") && !formation.getCodeFormation().equals("")) {
                    FormationList.add(formation);
                }

                System.out.println("================!!!||||!!!=============== >> |||||| >>x Code formation : "+formation.getCodeFormation());
                System.out.println("================!!!||||!!!=============== >> |||||| >>x Libelle long : "+formation.getLibelleLong());
                // System.out.println("================!!!||||!!!=============== >> |||||| >>x Chef : "+departement.getCodeDept());
                // if(formation.getDepartement().getId() != null) {
                //     System.out.println("================!!!||||!!!=============== >> |||||| >>x Code Departement : " + formation.getDepartement().getId());
                // }
            }
        }
        workbook.close();
        return FormationList;
    }

    public void saveFormation(List<Formation> formations){
        if(!formations.isEmpty()){
            for(Formation formation : formations){
                formationRepository.save(formation);
            }
        }
        else
            System.out.println("Fichier vide ou incorrect");
    }
}
