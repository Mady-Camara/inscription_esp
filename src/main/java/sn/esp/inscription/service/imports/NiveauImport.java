package sn.esp.inscription.service.imports;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sn.esp.inscription.domain.Formation;
import sn.esp.inscription.domain.Niveau;
import sn.esp.inscription.repository.FormationRepository;
import sn.esp.inscription.repository.NiveauRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class NiveauImport {
    @Autowired
    NiveauRepository niveauRepository;

    @Autowired
    FormationRepository formationRepository;

    public List<Niveau> getNiveau(final MultipartFile multipart) throws IOException {
        List<Niveau> NiveauList = new ArrayList<>();

        XSSFWorkbook workbook = new XSSFWorkbook(multipart.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 1; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                Niveau niveau = new Niveau();

                XSSFRow row = worksheet.getRow(index);

                // Long id = (long) row.getCell(0).getNumericCellValue();
                // niveau.setId(id);

                niveau.setCodeNiveau(row.getCell(0).getStringCellValue());
                niveau.setLibelleLong(row.getCell(1).getStringCellValue());
                Formation formation = formationRepository.findByCodeFormationIgnoreCase(row.getCell(2).getStringCellValue());
                if(formation != null) {
                    niveau.setFormation(formation);
                }
                if(niveau.getCodeNiveau() != null && !niveau.getCodeNiveau().equals("")) {
                    NiveauList.add(niveau);
                }
            }
        }
        workbook.close();
        return NiveauList;
    }

    public void saveNiveau(List<Niveau> niveaus) throws IOException {
       if(!niveaus.isEmpty()){
           for(Niveau niveau : niveaus){
               niveauRepository.save(niveau);
           }
       }
    }

}
