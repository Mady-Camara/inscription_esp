package sn.esp.inscription.service.imports;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sn.esp.inscription.domain.*;
import sn.esp.inscription.repository.*;
import sn.esp.inscription.service.EtudiantService;
import sn.esp.inscription.service.UserService;
import sn.esp.inscription.service.dto.UserDTO;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class EtudiantImport {

    @Autowired
    EtudiantRepository etudiantRepository;

    @Autowired
    NiveauRepository niveauRepository;

    @Autowired
    InscriptionRepository inscriptionRepository;

    @Autowired
    UserService userService;

    @Autowired
    AnneeUniversitaireRepository anneeUniversitaireRepository;

    public void getEtudiant(final MultipartFile multipart) throws IOException {
        // List<Etudiant> EtudiantList = new ArrayList<>();
        // List<Inscription> inscriptions = new ArrayList<>();
        // List<User> users = new ArrayList<>();

        XSSFWorkbook workbook = new XSSFWorkbook(multipart.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 1; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                Etudiant etudiant = new Etudiant();
                UserDTO userDTO = new UserDTO();
                Inscription inscription = new Inscription();

                XSSFRow row = worksheet.getRow(index);

                // Long id = (long) row.getCell(0).getNumericCellValue();
                // etudiant.setId(id);
                DateTimeFormatter formatterLocalDate = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                DataFormatter formatter = new DataFormatter();

                etudiant.setNumIdentifiant(formatter.formatCellValue(row.getCell(0)));
                String firstName = row.getCell(1).getStringCellValue();
                String lastName = row.getCell(2).getStringCellValue();
                String date = formatter.formatCellValue(row.getCell(3));
                if(date != null && !date.equals("") && row.getCell(3).getDateCellValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString() != null){
                String vd = row.getCell(3).getDateCellValue().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString();
                System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&??????????????????>>>>>>>>>>>>>>>>>>>>> "+vd);

                    etudiant.setDateNaissance(LocalDate.parse(vd));
                    System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&??????????????????>>>>>>>>>>>>>>>>>>>>> "+etudiant.getDateNaissance());
                }
                etudiant.setLieuNaissance(row.getCell(4).getStringCellValue().toUpperCase());
                String email = row.getCell(5).getStringCellValue();
                Niveau niveau = niveauRepository.findByCodeNiveauIgnoreCase(row.getCell(6).getStringCellValue());

                User user = userService.createUserImport(userDTO, firstName, lastName, email);
                if(user != null) {
                    etudiant.setUser(user);
                }
                // Inscription
                if(niveau != null) {
                    inscription.setNiveau(niveau);
                }
                if(etudiant.getNumIdentifiant() != null) {
                    inscription.setEtudiant(etudiant);
                }
                if(etudiant.getNumIdentifiant() != null && !etudiant.getNumIdentifiant().equals("")) {
                    etudiantRepository.save(etudiant);
                }
                if(inscription.getNiveau() != null) {
                    AnneeUniversitaire anneeUniversitaire = anneeUniversitaireRepository.findOneByIsActiveTrue();
                    if(anneeUniversitaire != null){
                        inscription.setAnneeUniversitaire(anneeUniversitaire);
                    }
                    inscriptionRepository.save(inscription);
                }
                //etudiant.addEtudiant(inscription);
                // EtudiantList.add(etudiant);
                // inscriptions.add(inscription);
                // users.add(user);
            }
        }
        workbook.close();
        // return EtudiantList;
    }

    public void saveEtudiant(List<Etudiant> etudiants){
        for(Etudiant etudiant : etudiants){
            etudiantRepository.save(etudiant);
        }
    }

    // public
}
