package sn.esp.inscription.service.imports;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sn.esp.inscription.domain.Departement;
import sn.esp.inscription.domain.enumeration.EnumTypeDept;
import sn.esp.inscription.repository.DepartementRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class DepartImport {

    @Autowired
    DepartementRepository departementRepository;

    public List<Departement> getDepartement(final MultipartFile multipart) throws IOException {
        List<Departement> DepartementList = new ArrayList<>();

        XSSFWorkbook workbook = new XSSFWorkbook(multipart.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        for (int index = 1; index < worksheet.getPhysicalNumberOfRows(); index++) {
            if (index > 0) {
                Departement departement = new Departement();

                XSSFRow row = worksheet.getRow(index);

                //Long id = (long) row.getCell(0).getNumericCellValue();
                //departement.setId(id.toString());
                //departement.setId(id);

                    departement.setCodeDept(row.getCell(0).getStringCellValue());
                    departement.setLibelleLong(row.getCell(1).getStringCellValue());
                    if (row.getCell(2).getStringCellValue().equalsIgnoreCase("dept") || row.getCell(2).getStringCellValue().equalsIgnoreCase("departement")) {
                        EnumTypeDept type = EnumTypeDept.Departement;
                        departement.setTypeDept(type);
                    }
                    if (row.getCell(2).getStringCellValue().equalsIgnoreCase("serv") || row.getCell(2).getStringCellValue().equalsIgnoreCase("service") || row.getCell(2).getStringCellValue().equalsIgnoreCase("ser")) {
                        EnumTypeDept type = EnumTypeDept.Service;
                        departement.setTypeDept(type);
                    }
                    departement.setChef(row.getCell(3).getStringCellValue());
                    //EnumTypeDept type = row.getCell(2).getStringCellValue().equalsIgnoreCase("Departement") ? EnumTypeDept.Departement : EnumTypeDept.Service;
                    
                    if(departement.getTypeDept() != null && departement.getCodeDept() != null && !departement.getCodeDept().equals("")) {
                        DepartementList.add(departement);
                    }
                System.out.println("================!!!||||!!!=============== >> |||||| >>x Code departement : "+departement.getCodeDept());
                System.out.println("================!!!||||!!!=============== >> |||||| >>x Libelle long : "+departement.getLibelleLong());
                System.out.println("================!!!||||!!!=============== >> |||||| >>x Chef : "+departement.getChef());
                System.out.println("================!!!||||!!!=============== >> |||||| >>x Type Departement : "+departement.getTypeDept());
            }
        }
        workbook.close();
        return DepartementList;
    }

    public void saveDepartement(List<Departement> departements){
        if(!departements.isEmpty()){
            for(Departement departement : departements){
                System.out.println("================!!!||||!!!=============== >> |||||| >>x Code departement : "+departement.getCodeDept());
                System.out.println("================!!!||||!!!=============== >> |||||| >>x Code departement : "+departement.getLibelleLong());
                System.out.println("================!!!||||!!!=============== >> |||||| >>x Code departement : "+departement.getChef());
                System.out.println("================!!!||||!!!=============== >> |||||| >>x Code departement : "+departement.getTypeDept());
                departementRepository.save(departement);
            }
        }
    }
}
