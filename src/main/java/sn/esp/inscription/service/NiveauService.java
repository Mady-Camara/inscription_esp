package sn.esp.inscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.esp.inscription.domain.Formation;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.domain.Niveau;
import sn.esp.inscription.repository.FormationRepository;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.repository.NiveauRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class NiveauService {

    @Autowired
    NiveauRepository niveauRepository;

    @Autowired
    FormationRepository formationRepository;

    @Autowired
    InscriptionRepository inscriptionRepository;

    public List<Inscription> listeNiveau(String codeFormation){
        List<Inscription> inscriptions = new ArrayList<>();
        List<Niveau> niveaux;
        Formation formation = formationRepository.findByCodeFormationIgnoreCase(codeFormation);
        niveaux = niveauRepository.findByFormation(formation);
        for(Niveau niveau: niveaux) {
            inscriptions.addAll(inscriptionRepository.findByNiveau(niveau));
        }
        int effectif = inscriptions.size();
        return inscriptions;
    }
}
