package sn.esp.inscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sn.esp.inscription.domain.Departement;
import sn.esp.inscription.domain.Formation;
import sn.esp.inscription.repository.DepartementRepository;
import sn.esp.inscription.repository.FormationRepository;

import java.util.List;

@Service
public class FormationService {

    @Autowired
    DepartementRepository departementRepository;

    @Autowired
    FormationRepository formationRepository;

    public List<Formation> ListeInscriptionParCodeDepartement(String codeDepartement){
        List<Formation> formations;
        Departement departement = departementRepository.findByCodeDeptIgnoreCase(codeDepartement);
        formations = formationRepository.findByDepartement(departement);
        return formations;
    }
}
