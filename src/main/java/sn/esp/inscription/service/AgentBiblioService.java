package sn.esp.inscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sn.esp.inscription.config.Constants;
import sn.esp.inscription.domain.AgentBiblio;
import sn.esp.inscription.domain.Authority;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.repository.AgentBiblioRepository;
import sn.esp.inscription.repository.AuthorityRepository;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.repository.UserRepository;
import sn.esp.inscription.security.AuthoritiesConstants;
import sn.esp.inscription.web.rest.errors.LoginAlreadyUsedException;
import sn.esp.inscription.web.rest.errors.MatriculeNotFound;

import java.util.HashSet;
import java.util.Set;

@Service
public class AgentBiblioService {
    @Autowired
    AgentBiblioRepository agentBiblioRepository;

    @Autowired
    InscriptionRepository inscriptionRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final AuthorityRepository authorityRepository;

    public AgentBiblioService(UserRepository userRepository, AuthorityRepository authorityRepository){
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
    }


    public AgentBiblio createAgentBiblio(AgentBiblio agentBiblio) {
        if(agentBiblioRepository.findByMatriculeIgnoreCase(agentBiblio.getMatricule()) != null){
            throw new MatriculeNotFound();
        }
        agentBiblio.setMatricule(agentBiblio.getMatricule());

        User user = new User();
        if(userRepository.findOneByLogin(agentBiblio.getEmail()).isPresent()){
            throw new LoginAlreadyUsedException();
        }
        user.setLogin(agentBiblio.getEmail());
        user.setEmail(agentBiblio.getEmail());
        String login = "";
        long i = 1;
        if(agentBiblio.getPrenom() != null && agentBiblio.getNom() != null){
            user.setFirstName(agentBiblio.getPrenom());
            user.setLastName(agentBiblio.getNom());
            login = agentBiblio.getPrenom()+"-"+agentBiblio.getNom();
            while(userRepository.findOneByLogin(login).isPresent()){
                i++;
                login = user.getFirstName()+"-"+user.getLastName()+""+i;
            }
            user.setLogin(login);
        }
        if (user.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(user.getLangKey());
        }
        if(agentBiblio.getPrenom() != null){
            user.setFirstName(agentBiblio.getPrenom());
        }
        if(agentBiblio.getNom() != null){
            user.setLastName(agentBiblio.getNom());
        }
        // Encryptage mot de passe
        String EncodingPassword = passwordEncoder.encode(agentBiblio.getMotDePasse());
        user.setPassword(EncodingPassword);
        // user.setLangKey();
        user.setActivated(true);
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.AGENTBIBLIO).ifPresent(authorities::add);
        user.setAuthorities(authorities);
        agentBiblio.setUser(userRepository.save(user));
        //userRepository.save(user);
        return agentBiblioRepository.save(agentBiblio);
    }
}
