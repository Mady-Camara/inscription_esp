package sn.esp.inscription.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sn.esp.inscription.config.Constants;
import sn.esp.inscription.domain.Authority;
import sn.esp.inscription.domain.Medecin;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.repository.AuthorityRepository;
import sn.esp.inscription.repository.MedecinRepository;
import sn.esp.inscription.repository.UserRepository;
import sn.esp.inscription.security.AuthoritiesConstants;
import sn.esp.inscription.web.rest.errors.LoginAlreadyUsedException;
import sn.esp.inscription.web.rest.errors.MatriculeNotFound;

import java.util.HashSet;
import java.util.Set;

@Service
public class MedecinService {
    @Autowired
    MedecinRepository medecinRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public Medecin createMedecin(Medecin medecin) {
        if(medecinRepository.findByMatriculeIgnoreCase(medecin.getMatricule()) != null){
            throw new MatriculeNotFound();
        }
        medecin.setMatricule(medecin.getMatricule());

        User user = new User();
        if(userRepository.findOneByLogin(medecin.getEmail()).isPresent()){
            throw new LoginAlreadyUsedException();
        }
        user.setLogin(medecin.getEmail());
        user.setEmail(medecin.getEmail());
        String login = "";
        long i = 1;
        if(medecin.getPrenom() != null && medecin.getNom() != null){
            user.setFirstName(medecin.getPrenom());
            user.setLastName(medecin.getNom());
            login = medecin.getPrenom()+"-"+medecin.getNom();
            while(userRepository.findOneByLogin(login).isPresent()){
                i++;
                login = user.getFirstName()+"-"+user.getLastName()+""+i;
            }
            user.setLogin(login);
        }
        if (user.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(user.getLangKey());
        }
        //Encryptage mot de passe
        String EncodingPassword = passwordEncoder.encode(medecin.getMotDePasse());
        user.setPassword(EncodingPassword);
        //user.setLangKey();
        user.setActivated(true);
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.MEDECIN).ifPresent(authorities::add);
        user.setAuthorities(authorities);
        medecin.setUser(userRepository.save(user));
        //userRepository.save(user);
        return medecinRepository.save(medecin);
    }
}
