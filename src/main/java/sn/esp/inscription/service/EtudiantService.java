package sn.esp.inscription.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import sn.esp.inscription.domain.Etudiant;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.repository.EtudiantRepository;
import sn.esp.inscription.repository.InscriptionRepository;

import java.time.LocalDate;
import java.util.List;

@Service
public class EtudiantService {
    @Autowired
    EtudiantRepository etudiantRepository;

    @Autowired
    InscriptionRepository inscriptionRepository;

    public List<Etudiant> findAll(){
        return etudiantRepository.findAll();
    }

    public long count(){
        return etudiantRepository.count();
    }

    public void delete(Etudiant etudiant){
        etudiantRepository.delete(etudiant);
    }

    public Etudiant checkEtudiant(Etudiant etudiant) throws NotFoundException {
        Etudiant find = etudiantRepository.findByNumIdentifiantOrTelephone(etudiant.getNumIdentifiant(), etudiant.getTelephone());
        if(find == null){
            throw new NotFoundException("Cet etudiant n'existe pas");
        }
        return find;
    }

    public Etudiant getEtudiantInfo(@PathVariable String num){
        Etudiant etudiant = etudiantRepository.findByNumIdentifiant(num);
        return etudiant;
    }

    public Inscription InscritionActive(String num){
        Etudiant result = etudiantRepository.findByNumIdentifiantIgnoreCase(num);
        List<Inscription> inscriptions = inscriptionRepository.findByEtudiant(result);
        Inscription inscription = new Inscription();
        for(Inscription trouve : inscriptions){
            if(trouve.getAnneeUniversitaire().isIsActive()){
                 inscription = trouve;
                return inscription;
            }
        }
        return inscription;
    }

    /*
    public LocalDate convertLocalDate(String vdate){
        int taille = vdate.length();
        System.out.println("=========================================================== > "+vdate+" Taille : "+taille);
        String jour = "";
        String mois = "";
        String annee = "";
        jour = vdate.substring(0, 2);
        mois = vdate.substring(3, 2);
        annee = vdate.substring(6, 4);
        vdate = annee+"-"+mois+"-"+jour;
        System.out.println("=========================================================== > "+vdate);
        LocalDate localDate = LocalDate.parse(vdate);
        return localDate;
    }

     */


}
