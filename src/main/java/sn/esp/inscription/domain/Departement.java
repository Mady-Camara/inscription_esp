package sn.esp.inscription.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import sn.esp.inscription.domain.enumeration.EnumTypeDept;

/**
 * A Departement.
 */
@Entity
@Table(name = "departement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Departement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2)
    @Column(name = "code_dept", nullable = false, unique = true)
    private String codeDept;

    @Column(name = "libelle_long")
    private String libelleLong;

    @Column(name = "chef")
    private String chef;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_dept")
    private EnumTypeDept typeDept;

    @OneToMany(mappedBy = "departement")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Formation> formations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeDept() {
        return codeDept;
    }

    public Departement codeDept(String codeDept) {
        this.codeDept = codeDept;
        return this;
    }

    public void setCodeDept(String codeDept) {
        this.codeDept = codeDept;
    }

    public String getLibelleLong() {
        return libelleLong;
    }

    public Departement libelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
        return this;
    }

    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    public String getChef() {
        return chef;
    }

    public Departement chef(String chef) {
        this.chef = chef;
        return this;
    }

    public void setChef(String chef) {
        this.chef = chef;
    }

    public EnumTypeDept getTypeDept() {
        return typeDept;
    }

    public Departement typeDept(EnumTypeDept typeDept) {
        this.typeDept = typeDept;
        return this;
    }

    public void setTypeDept(EnumTypeDept typeDept) {
        this.typeDept = typeDept;
    }

    public Set<Formation> getFormations() {
        return formations;
    }

    public Departement formations(Set<Formation> formations) {
        this.formations = formations;
        return this;
    }

    public Departement addFormation(Formation formation) {
        this.formations.add(formation);
        formation.setDepartement(this);
        return this;
    }

    public Departement removeFormation(Formation formation) {
        this.formations.remove(formation);
        formation.setDepartement(null);
        return this;
    }

    public void setFormations(Set<Formation> formations) {
        this.formations = formations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Departement)) {
            return false;
        }
        return id != null && id.equals(((Departement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Departement{" +
            "id=" + getId() +
            ", codeDept='" + getCodeDept() + "'" +
            ", libelleLong='" + getLibelleLong() + "'" +
            ", chef='" + getChef() + "'" +
            ", typeDept='" + getTypeDept() + "'" +
            "}";
    }
}
