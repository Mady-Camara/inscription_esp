package sn.esp.inscription.domain.enumeration;

/**
 * The EnumBourse enumeration.
 */
public enum EnumBourse {
    NONBOURSIER, BOURSIER, ETRANGER, ETRANGEREXONERE, ETRANGERTARIFNORMAL
}
