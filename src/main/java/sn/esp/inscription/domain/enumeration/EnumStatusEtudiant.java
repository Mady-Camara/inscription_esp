package sn.esp.inscription.domain.enumeration;

/**
 * The EnumStatusEtudiant enumeration.
 */
public enum EnumStatusEtudiant {
    REGIMENORMAL, REGIMESALARIE, REGIMEPARTICULIER, MISEENPOSITIONDESTAGE
}
