package sn.esp.inscription.domain.enumeration;

/**
 * The EnumHoraireTd enumeration.
 */
public enum EnumHoraireTd {
    AVANT18H, APRES18H, REGIMEPARTICULIER
}
