package sn.esp.inscription.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import sn.esp.inscription.domain.*;
import sn.esp.inscription.repository.AuthorityRepository;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.repository.MedecinRepository;
import sn.esp.inscription.repository.UserRepository;
import sn.esp.inscription.security.AuthoritiesConstants;
import sn.esp.inscription.service.MedecinService;
import sn.esp.inscription.service.UserService;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.Medecin}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class MedecinResource {

    @Autowired
    MedecinService medecinService;

    @Autowired
    UserService userService;

    @Autowired
    InscriptionRepository inscriptionRepository;

    private final Logger log = LoggerFactory.getLogger(MedecinResource.class);

    private static final String ENTITY_NAME = "medecin";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MedecinRepository medecinRepository;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    public MedecinResource(MedecinRepository medecinRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository) {
        this.medecinRepository = medecinRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }

    /**
     * {@code POST  /medecins} : Create a new medecin.
     *
     * @param medecin the medecin to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new medecin, or with status {@code 400 (Bad Request)} if the medecin has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/medecins")
    public ResponseEntity<Medecin> createMedecin(@Valid @RequestBody Medecin medecin) throws URISyntaxException {
        log.debug("REST request to save Medecin : {}", medecin);
        if (medecin.getId() != null) {
            throw new BadRequestAlertException("A new medecin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Medecin result = medecinService.createMedecin(medecin);
        return ResponseEntity.created(new URI("/api/medecins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /medecins} : Updates an existing medecin.
     *
     * @param medecin the medecin to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated medecin,
     * or with status {@code 400 (Bad Request)} if the medecin is not valid,
     * or with status {@code 500 (Internal Server Error)} if the medecin couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/medecins")
    public ResponseEntity<Medecin> updateMedecin(@Valid @RequestBody Medecin medecin) throws URISyntaxException {
        log.debug("REST request to update Medecin : {}", medecin);
        if (medecin.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if(medecin.getUser() != null) {
            if (medecin.getPrenom() != null) {
                medecin.getUser().setFirstName(medecin.getPrenom());
            }
            if (medecin.getNom() != null) {
                medecin.getUser().setLastName(medecin.getNom());
            }
            if (medecin.getMotDePasse() != null) {
                medecin.getUser().setPassword(medecin.getMotDePasse());
                String EncodingPassword = passwordEncoder.encode(medecin.getMotDePasse());
                medecin.getUser().setPassword(EncodingPassword);
            }
            if (medecin.getEmail() != null) {
                medecin.getUser().setEmail(medecin.getEmail());
            }
            Set<Authority> authorities = new HashSet<>();
            authorityRepository.findById(AuthoritiesConstants.MEDECIN).ifPresent(authorities::add);
            medecin.getUser().setAuthorities(authorities);
            userRepository.save(medecin.getUser());
        }
        Medecin result = medecinRepository.save(medecin);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, medecin.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /medecins} : get all the medecins.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of medecins in body.
     */
    @GetMapping("/medecins")
    public List<Medecin> getAllMedecins() {
        log.debug("REST request to get all Medecins");
        return medecinRepository.findAll();
    }

    /**
     * {@code GET  /medecins/:id} : get the "id" medecin.
     *
     * @param id the id of the medecin to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the medecin, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/medecins/{id}")
    public ResponseEntity<Medecin> getMedecin(@PathVariable Long id) {
        log.debug("REST request to get Medecin : {}", id);
        Optional<Medecin> medecin = medecinRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(medecin);
    }

    /**
     * {@code DELETE  /medecins/:id} : delete the "id" medecin.
     *
     * @param id the id of the medecin to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/medecins/{id}")
    public ResponseEntity<Void> deleteMedecin(@PathVariable Long id) {
        log.debug("REST request to delete Medecin : {}", id);
        Optional<Medecin> medecin = medecinRepository.findById(id);
        User user = medecin.get().getUser();
        medecinRepository.deleteById(id);
        if(user != null) {
            userRepository.delete(user);
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    // ADD
    @PutMapping("/medecins/estapte")
    public ResponseEntity<Inscription> etudiantestapte(@RequestBody Etudiant etudiant){
        //Etudiant etud = etudiantRepository.findByNumIdentifiant(numIdentifiant);
        List<Inscription> ins = inscriptionRepository.findByEtudiant(etudiant);
        Inscription trouve = new Inscription();
        for(Inscription inscription : ins){
            if(inscription.getAnneeUniversitaire() != null) {
                if (inscription.getAnneeUniversitaire().isIsActive()) {
                    trouve = inscription;
                    trouve.setEstApte(true);
                    inscriptionRepository.save(trouve);
                }
            }
        }
        return ResponseEntity.ok().body(trouve);
    }

    @PutMapping("/medecins/estinapte")
    public ResponseEntity<Inscription> etudiantestinapte(@RequestBody Etudiant etudiant){
        List<Inscription> ins = inscriptionRepository.findByEtudiant(etudiant);
        System.out.println("================|=======|======== >  ================|=======|======== >"+ins.size());
        Inscription trouve = new Inscription();
        for(Inscription inscription : ins){
            if(inscription.getAnneeUniversitaire() != null) {
                if (inscription.getAnneeUniversitaire().isIsActive()) {
                    trouve = inscription;
                    trouve.setEstApte(false);
                    inscriptionRepository.save(trouve);
                }
            }
        }
        return ResponseEntity.ok().body(trouve);
    }

    @GetMapping("/medecins/currentUserId")
    public ResponseEntity<Medecin> idUser(){
        User user = userService.getUserWithAuthorities().get();
        // User user = userRepository.findByLogin(login);
        // System.out.println("========================================> "+login);
        Medecin medecin = medecinRepository.findByUser(user);
        // System.out.println("======================< USER >==================> "+user.getLogin());
        if(medecin == null){
            return ResponseEntity.notFound().build();
        }
        // System.out.println("========================< ETUDIANT >================> "+medecin.getId());
        return ResponseEntity.ok().body(medecin);
    }
}
