package sn.esp.inscription.web.rest.vm;

import sn.esp.inscription.service.dto.UserDTO;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    private LocalDate dateNaissance;

    private String telephone;

    private String numIdentifiant;

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone(){ return telephone;}

    public void setTelephone(String telephone){ this.telephone = telephone;}

    public LocalDate getDateNaissance(){ return dateNaissance;}

    public void setDateNaissance(LocalDate dateNaissance){ this.dateNaissance = dateNaissance;}

    public String getNumIdentifiant(){ return numIdentifiant;}

    public void setNumIdentifiant(String numIdentifiant){ this.numIdentifiant = numIdentifiant;}


    // prettier-ignore
    @Override
    public String toString() {
        return "ManagedUserVM{" + super.toString() + "} ";
    }
}
