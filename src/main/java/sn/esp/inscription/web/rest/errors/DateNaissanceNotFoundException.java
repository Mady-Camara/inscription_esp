package sn.esp.inscription.web.rest.errors;

public class DateNaissanceNotFoundException extends BadRequestAlertException {
    public DateNaissanceNotFoundException() {
        super(ErrorConstants.LOGIN_NOT_FOUND_TYPE, "DateNaissance not found", "userManagement", "Dateontexist");
    }
}
