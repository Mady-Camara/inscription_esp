package sn.esp.inscription.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import sn.esp.inscription.domain.*;
import sn.esp.inscription.repository.*;
import sn.esp.inscription.service.UserService;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.AgentScolarite}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AgentScolariteResource {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    private final Logger log = LoggerFactory.getLogger(AgentScolariteResource.class);

    private static final String ENTITY_NAME = "agentScolarite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentScolariteRepository agentScolariteRepository;

    private final MedecinRepository medecinRepository;

    private final AssureurRepository assureurRepository;

    private final AgentBiblioRepository agentBiblioRepository;

    public AgentScolariteResource(AgentScolariteRepository agentScolariteRepository, MedecinRepository medecinRepository, AssureurRepository assureurRepository, AgentBiblioRepository agentBiblioRepository) {
        this.agentScolariteRepository = agentScolariteRepository;
        this.assureurRepository = assureurRepository;
        this.medecinRepository = medecinRepository;
        this.agentBiblioRepository = agentBiblioRepository;
    }

    /**
     * {@code POST  /agent-scolarites} : Create a new agentScolarite.
     *
     * @param agentScolarite the agentScolarite to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agentScolarite, or with status {@code 400 (Bad Request)} if the agentScolarite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agent-scolarites")
    public ResponseEntity<AgentScolarite> createAgentScolarite(@Valid @RequestBody AgentScolarite agentScolarite) throws URISyntaxException {
        log.debug("REST request to save AgentScolarite : {}", agentScolarite);
        if (agentScolarite.getId() != null) {
            throw new BadRequestAlertException("A new agentScolarite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentScolarite result = agentScolariteRepository.save(agentScolarite);
        return ResponseEntity.created(new URI("/api/agent-scolarites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /agent-scolarites} : Updates an existing agentScolarite.
     *
     * @param agentScolarite the agentScolarite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agentScolarite,
     * or with status {@code 400 (Bad Request)} if the agentScolarite is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agentScolarite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agent-scolarites")
    public ResponseEntity<AgentScolarite> updateAgentScolarite(@Valid @RequestBody AgentScolarite agentScolarite) throws URISyntaxException {
        log.debug("REST request to update AgentScolarite : {}", agentScolarite);
        if (agentScolarite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentScolarite result = agentScolariteRepository.save(agentScolarite);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, agentScolarite.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /agent-scolarites} : get all the agentScolarites.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agentScolarites in body.
     */
    @GetMapping("/agent-scolarites")
    public List<AgentScolarite> getAllAgentScolarites() {
        log.debug("REST request to get all AgentScolarites");
        return agentScolariteRepository.findAll();
    }

    /**
     * {@code GET  /agent-scolarites/:id} : get the "id" agentScolarite.
     *
     * @param id the id of the agentScolarite to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agentScolarite, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agent-scolarites/{id}")
    public ResponseEntity<AgentScolarite> getAgentScolarite(@PathVariable Long id) {
        log.debug("REST request to get AgentScolarite : {}", id);
        Optional<AgentScolarite> agentScolarite = agentScolariteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentScolarite);
    }

    /**
     * {@code DELETE  /agent-scolarites/:id} : delete the "id" agentScolarite.
     *
     * @param id the id of the agentScolarite to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agent-scolarites/{id}")
    public ResponseEntity<Void> deleteAgentScolarite(@PathVariable Long id) {
        log.debug("REST request to delete AgentScolarite : {}", id);
        agentScolariteRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    // ADD
    @GetMapping("/agent-scolarites/getAssureur/{matricule}")
    public ResponseEntity<Assureur> getAssureur(@PathVariable String matricule){
        log.debug("request to get info etudiant");
        Assureur assureur = assureurRepository.findByMatriculeIgnoreCase(matricule);
        if(assureur == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(assureur);
    }

    @GetMapping("/agent-scolarites/getMedecin/{matricule}")
    public ResponseEntity<Medecin> getMedecin(@PathVariable String matricule){
        log.debug("request to get info etudiant");
        Medecin medecin = medecinRepository.findByMatriculeIgnoreCase(matricule);
        if(medecin == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(medecin);
    }

    @GetMapping("/agent-scolarites/getAgentBiblio/{matricule}")
    public ResponseEntity<AgentBiblio> getAgentBiblio(@PathVariable String matricule){
        log.debug("request to get info etudiant");
        AgentBiblio agentBiblio = agentBiblioRepository.findByMatriculeIgnoreCase(matricule);
        if(agentBiblio == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(agentBiblio);
    }

    @GetMapping("/agent-scolarites/currentUserId")
    public ResponseEntity<User> idUser(){
        User user = userService.getUserWithAuthorities().get();
        // User user = userRepository.findByLogin(login);
        // System.out.println("========================================> "+login);
        return ResponseEntity.ok().body(user);
    }
}
