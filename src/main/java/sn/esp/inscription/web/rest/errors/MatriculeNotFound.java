package sn.esp.inscription.web.rest.errors;

public class MatriculeNotFound extends BadRequestAlertException {
    public MatriculeNotFound() {
        super(ErrorConstants.MATRICULE__EXIST_TYPE, "Matricule exist", "assureur", "MatriculExist");
    }
}
