package sn.esp.inscription.web.rest.errors;

public class EmailNotFoundException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public EmailNotFoundException() {
        super(ErrorConstants.LOGIN_NOT_FOUND_TYPE, "Email not found", "userManagement", "Emaildontexist");
    }
}
