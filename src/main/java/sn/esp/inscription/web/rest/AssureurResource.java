package sn.esp.inscription.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import sn.esp.inscription.domain.*;
import sn.esp.inscription.repository.*;
import sn.esp.inscription.security.AuthoritiesConstants;
import sn.esp.inscription.service.AssureurService;
import sn.esp.inscription.service.UserService;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.Assureur}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AssureurResource {

    @Autowired
    AssureurService assureurService;

    @Autowired
    UserService userService;

    private final Logger log = LoggerFactory.getLogger(AssureurResource.class);

    private static final String ENTITY_NAME = "assureur";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssureurRepository assureurRepository;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    private final EtudiantRepository etudiantRepository;

    private final InscriptionRepository inscriptionRepository;

    public AssureurResource(AssureurRepository assureurRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository, EtudiantRepository etudiantRepository, InscriptionRepository inscriptionRepository) {
        this.assureurRepository = assureurRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.etudiantRepository = etudiantRepository;
        this.inscriptionRepository = inscriptionRepository;
    }

    /**
     * {@code POST  /assureurs} : Create a new assureur.
     *
     * @param assureur the assureur to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new assureur, or with status {@code 400 (Bad Request)} if the assureur has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/assureurs")
    public ResponseEntity<Assureur> createAssureur(@Valid @RequestBody Assureur assureur) throws URISyntaxException {
        log.debug("REST request to save Assureur : {}", assureur);
        if (assureur.getId() != null) {
            throw new BadRequestAlertException("A new assureur cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Assureur result = assureurService.createAssureur(assureur);
        return ResponseEntity.created(new URI("/api/assureurs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /assureurs} : Updates an existing assureur.
     *
     * @param assureur the assureur to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assureur,
     * or with status {@code 400 (Bad Request)} if the assureur is not valid,
     * or with status {@code 500 (Internal Server Error)} if the assureur couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/assureurs")
    public ResponseEntity<Assureur> updateAssureur(@Valid @RequestBody Assureur assureur) throws URISyntaxException {
        log.debug("REST request to update Assureur : {}", assureur);
        if (assureur.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if(assureur.getUser() != null) {
            if (assureur.getPrenom() != null) {
                assureur.getUser().setFirstName(assureur.getPrenom());
            }
            if (assureur.getNom() != null) {
                assureur.getUser().setLastName(assureur.getNom());
            }
            if (assureur.getMotDePasse() != null) {
                assureur.getUser().setPassword(assureur.getMotDePasse());
                String EncodingPassword = passwordEncoder.encode(assureur.getMotDePasse());
                assureur.getUser().setPassword(EncodingPassword);
            }
            if (assureur.getEmail() != null) {
                assureur.getUser().setEmail(assureur.getEmail());
            }
            Set<Authority> authorities = new HashSet<>();
            authorityRepository.findById(AuthoritiesConstants.ASSUREUR).ifPresent(authorities::add);
            assureur.getUser().setAuthorities(authorities);
            userRepository.save(assureur.getUser());
        }
        Assureur result = assureurRepository.save(assureur);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, assureur.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /assureurs} : get all the assureurs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of assureurs in body.
     */
    @GetMapping("/assureurs")
    public List<Assureur> getAllAssureurs() {
        log.debug("REST request to get all Assureurs");
        return assureurRepository.findAll();
    }

    /**
     * {@code GET  /assureurs/:id} : get the "id" assureur.
     *
     * @param id the id of the assureur to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the assureur, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/assureurs/{id}")
    public ResponseEntity<Assureur> getAssureur(@PathVariable Long id) {
        log.debug("REST request to get Assureur : {}", id);
        Optional<Assureur> assureur = assureurRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(assureur);
    }

    /**
     * {@code DELETE  /assureurs/:id} : delete the "id" assureur.
     *
     * @param id the id of the assureur to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/assureurs/{id}")
    public ResponseEntity<Void> deleteAssureur(@PathVariable Long id) {
        log.debug("REST request to delete Assureur : {}", id);
        Optional<Assureur> assureur = assureurRepository.findById(id);
        User user = assureur.get().getUser();
        assureurRepository.deleteById(id);
        if(user != null) {
            userRepository.delete(user);
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    //les API ajoutés
    @GetMapping("/assureurs/getEtudiant/{numIdentifiant}")
    public ResponseEntity<Etudiant> getInfo(@PathVariable String numIdentifiant){
        log.debug("request to get info etudiant");
        Etudiant etudiant = etudiantRepository.findByNumIdentifiantIgnoreCase(numIdentifiant);
        if(etudiant == null){
            return ResponseEntity.notFound().build();
            //throw new NumIndentifiantNotFoundException();
        }
        //Optional<Etudiant> etu = Optional.of(etudiant);
        //return ResponseUtil.wrapOrNotFound(etu);

        return ResponseEntity.ok().body(etudiant);
    }

    @GetMapping("/getEtudiant/{numIdentifiant}")
    public ResponseEntity<Etudiant> getetudiant(@PathVariable String numIdentifiant){
        log.debug("request to get info etudiant");
        Etudiant etudiant = etudiantRepository.findByNumIdentifiantIgnoreCase(numIdentifiant);
        if(etudiant == null){
            return ResponseEntity.notFound().build();
            //throw new NumIndentifiantNotFoundException();
        }
        //Optional<Etudiant> etu = Optional.of(etudiant);
        //return ResponseUtil.wrapOrNotFound(etu);

        return ResponseEntity.ok().body(etudiant);
    }

    @PutMapping("/assureurs/estassurer")
    public ResponseEntity<Inscription> etudiantEstAssurer(@RequestBody Etudiant etudiant){
        List<Inscription> ins = inscriptionRepository.findByEtudiant(etudiant);
        System.out.println("================|=======|======== >  ================|=======|======== >"+ins.size());
        Inscription trouve = new Inscription();
        for(Inscription inscription : ins){
            if(inscription.getAnneeUniversitaire() != null) {
                if (inscription.getAnneeUniversitaire().isIsActive()) {
                    trouve = inscription;
                    trouve.setEstAssure(true);
                    inscriptionRepository.save(trouve);
                }
            }
        }
        return ResponseEntity.ok().body(trouve);
    }

    @GetMapping("/assureurs/currentUserId")
    public ResponseEntity<Assureur> idUser(){
        User user = userService.getUserWithAuthorities().get();
        // User user = userRepository.findByLogin(login);
        // System.out.println("========================================> "+login);
        Assureur assureur = assureurRepository.findByUser(user);
        // System.out.println("======================< USER >==================> "+user.getLogin());
        if(assureur == null){
            return ResponseEntity.notFound().build();
        }
        // System.out.println("========================< ETUDIANT >================> "+assureur.getId());
        return ResponseEntity.ok().body(assureur);
    }
}
