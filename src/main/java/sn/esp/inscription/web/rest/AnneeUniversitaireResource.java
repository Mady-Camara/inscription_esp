package sn.esp.inscription.web.rest;

import sn.esp.inscription.domain.AnneeUniversitaire;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.repository.AnneeUniversitaireRepository;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.AnneeUniversitaire}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AnneeUniversitaireResource {

    private final Logger log = LoggerFactory.getLogger(AnneeUniversitaireResource.class);

    private static final String ENTITY_NAME = "anneeUniversitaire";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AnneeUniversitaireRepository anneeUniversitaireRepository;

    private final InscriptionRepository inscriptionRepository;

    public AnneeUniversitaireResource(AnneeUniversitaireRepository anneeUniversitaireRepository, InscriptionRepository inscriptionRepository) {
        this.anneeUniversitaireRepository = anneeUniversitaireRepository;
        this.inscriptionRepository = inscriptionRepository;
    }

    /**
     * {@code POST  /annee-universitaires} : Create a new anneeUniversitaire.
     *
     * @param anneeUniversitaire the anneeUniversitaire to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new anneeUniversitaire, or with status {@code 400 (Bad Request)} if the anneeUniversitaire has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/annee-universitaires")
    public ResponseEntity<AnneeUniversitaire> createAnneeUniversitaire(@RequestBody AnneeUniversitaire anneeUniversitaire) throws URISyntaxException {
        log.debug("REST request to save AnneeUniversitaire : {}", anneeUniversitaire);
        if (anneeUniversitaire.getId() != null) {
            throw new BadRequestAlertException("A new anneeUniversitaire cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if(anneeUniversitaire.isIsActive()){
            List<AnneeUniversitaire> annees = anneeUniversitaireRepository.findByIsActiveTrue();
            for(AnneeUniversitaire annee : annees){
                annee.setIsActive(false);
                anneeUniversitaireRepository.save(annee);
            }
            List<Inscription> inscriptions = inscriptionRepository.findAll();
            for(Inscription inscription : inscriptions){
                inscription.setAnneeUniversitaire(anneeUniversitaire);
                inscriptionRepository.save(inscription);
            }
        }
        AnneeUniversitaire result = anneeUniversitaireRepository.save(anneeUniversitaire);
        return ResponseEntity.created(new URI("/api/annee-universitaires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /annee-universitaires} : Updates an existing anneeUniversitaire.
     *
     * @param anneeUniversitaire the anneeUniversitaire to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated anneeUniversitaire,
     * or with status {@code 400 (Bad Request)} if the anneeUniversitaire is not valid,
     * or with status {@code 500 (Internal Server Error)} if the anneeUniversitaire couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/annee-universitaires")
    public ResponseEntity<AnneeUniversitaire> updateAnneeUniversitaire(@RequestBody AnneeUniversitaire anneeUniversitaire) throws URISyntaxException {
        log.debug("REST request to update AnneeUniversitaire : {}", anneeUniversitaire);
        if (anneeUniversitaire.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if(anneeUniversitaire.isIsActive()){
            List<AnneeUniversitaire> annees = anneeUniversitaireRepository.findByIsActiveTrue();
            for(AnneeUniversitaire annee : annees){
                annee.setIsActive(false);
                anneeUniversitaireRepository.save(annee);
            }
            List<Inscription> inscriptions = inscriptionRepository.findAll();
            for(Inscription inscription : inscriptions){
                inscription.setAnneeUniversitaire(anneeUniversitaire);
                inscriptionRepository.save(inscription);
            }
        }
        AnneeUniversitaire result = anneeUniversitaireRepository.save(anneeUniversitaire);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, anneeUniversitaire.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /annee-universitaires} : get all the anneeUniversitaires.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of anneeUniversitaires in body.
     */
    @GetMapping("/annee-universitaires")
    public ResponseEntity<List<AnneeUniversitaire>> getAllAnneeUniversitaires(Pageable pageable) {
        log.debug("REST request to get a page of AnneeUniversitaires");
        Page<AnneeUniversitaire> page = anneeUniversitaireRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /annee-universitaires/:id} : get the "id" anneeUniversitaire.
     *
     * @param id the id of the anneeUniversitaire to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the anneeUniversitaire, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/annee-universitaires/{id}")
    public ResponseEntity<AnneeUniversitaire> getAnneeUniversitaire(@PathVariable Long id) {
        log.debug("REST request to get AnneeUniversitaire : {}", id);
        Optional<AnneeUniversitaire> anneeUniversitaire = anneeUniversitaireRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(anneeUniversitaire);
    }

    /**
     * {@code DELETE  /annee-universitaires/:id} : delete the "id" anneeUniversitaire.
     *
     * @param id the id of the anneeUniversitaire to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/annee-universitaires/{id}")
    public ResponseEntity<Void> deleteAnneeUniversitaire(@PathVariable Long id) {
        log.debug("REST request to delete AnneeUniversitaire : {}", id);
        anneeUniversitaireRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
