package sn.esp.inscription.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import sn.esp.inscription.domain.*;
import sn.esp.inscription.repository.AgentBiblioRepository;
import sn.esp.inscription.repository.AuthorityRepository;
import sn.esp.inscription.repository.InscriptionRepository;
import sn.esp.inscription.repository.UserRepository;
import sn.esp.inscription.security.AuthoritiesConstants;
import sn.esp.inscription.service.AgentBiblioService;
import sn.esp.inscription.service.UserService;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.AgentBiblio}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AgentBiblioResource {

    @Autowired
    AgentBiblioService agentBiblioService;

    @Autowired
    InscriptionRepository inscriptionRepository;

    @Autowired
    UserService userService;

    private final Logger log = LoggerFactory.getLogger(AgentBiblioResource.class);

    private static final String ENTITY_NAME = "agentBiblio";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentBiblioRepository agentBiblioRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final AuthorityRepository authorityRepository;

    public AgentBiblioResource(AgentBiblioRepository agentBiblioRepository, PasswordEncoder passwordEncoder, UserRepository userRepository, AuthorityRepository authorityRepository) {
        this.agentBiblioRepository = agentBiblioRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
    }

    /**
     * {@code POST  /agent-biblios} : Create a new agentBiblio.
     *
     * @param agentBiblio the agentBiblio to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agentBiblio, or with status {@code 400 (Bad Request)} if the agentBiblio has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agent-biblios")
    public ResponseEntity<AgentBiblio> createAgentBiblio(@Valid @RequestBody AgentBiblio agentBiblio) throws URISyntaxException {
        log.debug("REST request to save AgentBiblio : {}", agentBiblio);
        if (agentBiblio.getId() != null) {
            throw new BadRequestAlertException("A new agentBiblio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentBiblio result = agentBiblioService.createAgentBiblio(agentBiblio);
        return ResponseEntity.created(new URI("/api/agent-biblios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /agent-biblios} : Updates an existing agentBiblio.
     *
     * @param agentBiblio the agentBiblio to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agentBiblio,
     * or with status {@code 400 (Bad Request)} if the agentBiblio is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agentBiblio couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agent-biblios")
    public ResponseEntity<AgentBiblio> updateAgentBiblio(@Valid @RequestBody AgentBiblio agentBiblio) throws URISyntaxException {
        log.debug("REST request to update AgentBiblio : {}", agentBiblio);
        if (agentBiblio.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if(agentBiblio.getUser() != null) {
            if (agentBiblio.getPrenom() != null) {
                agentBiblio.getUser().setFirstName(agentBiblio.getPrenom());
            }
            if (agentBiblio.getNom() != null) {
                agentBiblio.getUser().setLastName(agentBiblio.getNom());
            }
            if (agentBiblio.getMotDePasse() != null) {
                agentBiblio.getUser().setPassword(agentBiblio.getMotDePasse());
                String EncodingPassword = passwordEncoder.encode(agentBiblio.getMotDePasse());
                agentBiblio.getUser().setPassword(EncodingPassword);
            }
            if (agentBiblio.getEmail() != null) {
                agentBiblio.getUser().setEmail(agentBiblio.getEmail());
            }
            Set<Authority> authorities = new HashSet<>();
            authorityRepository.findById(AuthoritiesConstants.AGENTBIBLIO).ifPresent(authorities::add);
            agentBiblio.getUser().setAuthorities(authorities);
            userRepository.save(agentBiblio.getUser());
            userRepository.save(agentBiblio.getUser());
        }
        AgentBiblio result = agentBiblioRepository.save(agentBiblio);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, agentBiblio.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /agent-biblios} : get all the agentBiblios.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agentBiblios in body.
     */
    @GetMapping("/agent-biblios")
    public List<AgentBiblio> getAllAgentBiblios() {
        log.debug("REST request to get all AgentBiblios");
        return agentBiblioRepository.findAll();
    }

    /**
     * {@code GET  /agent-biblios/:id} : get the "id" agentBiblio.
     *
     * @param id the id of the agentBiblio to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agentBiblio, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agent-biblios/{id}")
    public ResponseEntity<AgentBiblio> getAgentBiblio(@PathVariable Long id) {
        log.debug("REST request to get AgentBiblio : {}", id);
        Optional<AgentBiblio> agentBiblio = agentBiblioRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentBiblio);
    }

    /**
     * {@code DELETE  /agent-biblios/:id} : delete the "id" agentBiblio.
     *
     * @param id the id of the agentBiblio to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agent-biblios/{id}")
    public ResponseEntity<Void> deleteAgentBiblio(@PathVariable Long id) {
        log.debug("REST request to delete AgentBiblio : {}", id);
        Optional<AgentBiblio> agentBiblio = agentBiblioRepository.findById(id);
        User user = agentBiblio.get().getUser();
        agentBiblioRepository.deleteById(id);
        if(user != null) {
            userRepository.delete(user);
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    // ADD
    @PutMapping("/agent-biblios/reglebiblio")
    public ResponseEntity<Inscription> etudiantestinapte(@RequestBody Etudiant etudiant){
        List<Inscription> ins = inscriptionRepository.findByEtudiant(etudiant);
        System.out.println("================|=======|======== >  ================|=======|======== >"+ins.size());
        Inscription trouve = new Inscription();
        for(Inscription inscription : ins){
            if(inscription.getAnneeUniversitaire() != null) {
                if (inscription.getAnneeUniversitaire().isIsActive()) {
                    trouve = inscription;
                    trouve.setEnRegleBiblio(true);
                    inscriptionRepository.save(trouve);
                }
            }
        }
        return ResponseEntity.ok().body(trouve);
    }

    @GetMapping("/agent-biblios/currentUserId")
    public ResponseEntity<AgentBiblio> idUser(){
        User user = userService.getUserWithAuthorities().get();
        // User user = userRepository.findByLogin(login);
        // System.out.println("========================================> "+login);
        AgentBiblio agentBiblio = agentBiblioRepository.findByUser(user);
        // System.out.println("======================< USER >==================> "+user.getLogin());
        if(agentBiblio == null){
            return ResponseEntity.notFound().build();
        }
        // System.out.println("========================< ETUDIANT >================> "+agentBiblio.getId());
        return ResponseEntity.ok().body(agentBiblio);
    }
}
