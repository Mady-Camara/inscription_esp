import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { SERVER_API_URL } from 'app/app.constants';
import { Login } from 'app/core/login/login.model';

import { EtudiantService } from 'app/entities/etudiant/etudiant.service';
import { AssureurService } from 'app/entities/assureur/assureur.service';
import { MedecinService } from 'app/entities/medecin/medecin.service';
import { AgentBiblioService } from 'app/entities/agent-biblio/agent-biblio.service';
import { AgentScolariteService } from 'app/entities/agent-scolarite/agent-scolarite.service';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { IAssureur } from 'app/shared/model/assureur.model';
import { IMedecin } from 'app/shared/model/medecin.model';
import { IAgentBiblio } from 'app/shared/model/agent-biblio.model';
import { IAgentScolarite } from 'app/shared/model/agent-scolarite.model';
// import { HttpResponse } from '@angular/common/http';

type JwtToken = {
  id_token: string;
};

@Injectable({ providedIn: 'root' })
export class AuthServerProvider {
  etudiant: IEtudiant | null = null;
  assureur: IAssureur | null = null;
  medecin: IMedecin | null = null;
  agentScolarite: IAgentScolarite | null = null;
  agentBiblio: IAgentBiblio | null = null;

  constructor(
    private http: HttpClient,
    private $localStorage: LocalStorageService,
    private $sessionStorage: SessionStorageService,
    private etudiantServe: EtudiantService,
    private assureurService: AssureurService,
    private medecinService: MedecinService,
    private agentBiblioService: AgentBiblioService,
    private agentScolariteService: AgentScolariteService
  ) {}

  getToken(): string {
    return this.$localStorage.retrieve('authenticationToken') || this.$sessionStorage.retrieve('authenticationToken') || '';
  }

  login(credentials: Login): Observable<void> {
    return this.http
      .post<JwtToken>(SERVER_API_URL + 'api/authenticate', credentials)
      .pipe(map(response => this.authenticateSuccess(response, credentials.rememberMe)));
  }

  logout(): Observable<void> {
    return new Observable(observer => {
      this.$localStorage.clear('authenticationToken');
      this.$sessionStorage.clear('authenticationToken');

      this.$sessionStorage.clear('etudiantSession');
      observer.complete();
    });
  }

  private authenticateSuccess(response: JwtToken, rememberMe: boolean): void {
    const jwt = response.id_token;
    /*
      this.etudiantServe.findEtudiantId().subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
      this.etudiantServe.findEtudiantId().subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
      this.assureurService.findAssureurId().subscribe((res1: HttpResponse<IAssureur>) => (this.assureur = res1.body));
      this.medecinService.findMedecinId().subscribe((res2: HttpResponse<IMedecin>) => (this.medecin = res2.body));
      this.agentBiblioService.findAgentBiblioId().subscribe((res3: HttpResponse<IAgentBiblio>) => (this.agentBiblio = res3.body));
      this.agentScolariteService.findAgentScolariteId().subscribe((res4: HttpResponse<IAgentScolarite>) => (this.agentScolarite = res4.body));
    */
    if (rememberMe) {
      this.$localStorage.store('authenticationToken', jwt);
    } else {
      this.$sessionStorage.store('authenticationToken', jwt);
      /*
        this.$sessionStorage.store('etudiantSession', this.etudiant);
        this.$sessionStorage.store('etudiantSession', this.etudiant);
        this.$sessionStorage.store('assureurSession', this.assureur);
        this.$sessionStorage.store('medecinSession', this.medecin);
        this.$sessionStorage.store('agentBiblioSession', this.agentBiblio);
        this.$sessionStorage.store('agentScolariteSession', this.agentScolarite);
      */
    }
  }
}
