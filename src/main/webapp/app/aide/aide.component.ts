import { Component, OnInit } from '@angular/core';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'jhi-aide',
  templateUrl: './aide.component.html',
  styleUrls: ['./aide.scss'],
})
export class AideComponent implements OnInit {
  public isCollapsed = false;
  public isCollapsedTwo = true;
  public isCollapsedThree = true;
  faQuestionCircle = faQuestionCircle;

  partOne: string;
  partTwo: string;
  partThree: string;
  linkcolor: string;
  linkcolortwo: string;
  linkcolorThree: string;
  iconcolor: string;
  iconcolortwo: string;
  iconcolorThree: string;

  constructor() {
    this.partOne = '#74D0F1';
    this.partTwo = '#EFEFEF';
    this.partThree = '#EFEFEF';
    this.linkcolor = 'white';
    this.linkcolortwo = '#318CE7';
    this.linkcolorThree = '#318CE7';
    this.iconcolor = 'green';
    this.iconcolortwo = 'gray';
    this.iconcolorThree = 'gray';
  }

  ngOnInit(): void {}

  onClick(): void {
    if (this.isCollapsed === false) {
      this.isCollapsed = true;
      this.isCollapsedTwo = true;
      this.isCollapsedThree = true;
    } else {
      this.isCollapsed = false;
      this.isCollapsedTwo = true;
      this.isCollapsedThree = true;
    }
    if (this.partOne === '#74D0F1') {
      this.partOne = '#74D0F1';
      this.partTwo = '#EFEFEF';
      this.partThree = '#EFEFEF';
      this.linkcolor = 'white';
      this.linkcolortwo = '#318CE7';
      this.linkcolorThree = '#318CE7';
      this.iconcolor = 'green';
      this.iconcolortwo = 'gray';
      this.iconcolorThree = 'gray';
    } else {
      this.partOne = '#74D0F1';
      this.partTwo = '#EFEFEF';
      this.partThree = '#EFEFEF';
      this.linkcolor = 'white';
      this.linkcolortwo = '#318CE7';
      this.linkcolorThree = '#318CE7';
      this.iconcolor = 'green';
      this.iconcolortwo = 'gray';
      this.iconcolorThree = 'gray';
    }
  }

  onClicktwo(): void {
    if (this.isCollapsedTwo === false) {
      this.isCollapsedTwo = true;
      this.isCollapsed = true;
      this.isCollapsedThree = true;
    } else {
      this.isCollapsedTwo = false;
      this.isCollapsed = true;
      this.isCollapsedThree = true;
    }
    if (this.partTwo === '#74D0F1') {
      this.partTwo = '#74D0F1';
      this.partOne = '#EFEFEF';
      this.partThree = '#EFEFEF';
      this.linkcolortwo = 'white';
      this.linkcolor = '#318CE7';
      this.linkcolorThree = '#318CE7';
      this.iconcolortwo = 'green';
      this.iconcolor = 'gray';
      this.iconcolorThree = 'gray';
    } else {
      this.partTwo = '#74D0F1';
      this.partOne = '#EFEFEF';
      this.partThree = '#EFEFEF';
      this.linkcolortwo = 'white';
      this.linkcolor = '#318CE7';
      this.linkcolorThree = '#318CE7';
      this.iconcolortwo = 'green';
      this.iconcolor = 'gray';
      this.iconcolorThree = 'gray';
    }
  }

  onClickThree(): void {
    if (this.isCollapsedThree === false) {
      this.isCollapsedThree = true;
      this.isCollapsed = true;
      this.isCollapsedTwo = true;
    } else {
      this.isCollapsedThree = false;
      this.isCollapsed = true;
      this.isCollapsedTwo = true;
    }
    if (this.partThree === '#74D0F1') {
      this.partThree = '#74D0F1';
      this.partOne = '#EFEFEF';
      this.partTwo = '#EFEFEF';
      this.linkcolorThree = 'white';
      this.linkcolor = '#318CE7';
      this.linkcolortwo = '#318CE7';
      this.iconcolorThree = 'green';
      this.iconcolor = 'gray';
      this.iconcolortwo = 'gray';
    } else {
      this.partThree = '#74D0F1';
      this.partOne = '#EFEFEF';
      this.partTwo = '#EFEFEF';
      this.linkcolorThree = 'white';
      this.linkcolor = '#318CE7';
      this.linkcolortwo = '#318CE7';
      this.iconcolorThree = 'green';
      this.iconcolor = 'gray';
      this.iconcolortwo = 'gray';
    }
  }
}
