import { Component, AfterViewInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { JhiLanguageService } from 'ng-jhipster';

import {
  EMAIL_ALREADY_USED_TYPE,
  LOGIN_ALREADY_USED_TYPE,
  LOGIN_NOT_FOUND_TYPE,
  IDENTIFIANT_NOT_FOUND_TYPE,
} from 'app/shared/constants/error.constants';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { RegisterService } from './register.service';

@Component({
  selector: 'jhi-register',
  templateUrl: './register.component.html',
})
export class RegisterComponent implements AfterViewInit {
  /*  @ViewChild('login', { static: false })
  login?: ElementRef; */

  doNotMatch = false;
  error = false;
  errorEmailExists = false;
  errorUserExists = false;
  success = false;
  erroIdentifiantExists = false;
  errorEmailFound = false;

  registerForm = this.fb.group({
    numIdentifiant: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(9)]],
    dateNaissance: ['', [Validators.required]],
    telephone: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(12)]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
  });

  constructor(
    private languageService: JhiLanguageService,
    private loginModalService: LoginModalService,
    private registerService: RegisterService,
    private fb: FormBuilder
  ) {}

  ngAfterViewInit(): void {}

  register(): void {
    this.doNotMatch = false;
    this.error = false;
    this.errorEmailExists = false;
    this.errorUserExists = false;
    this.erroIdentifiantExists = false;
    this.errorEmailFound = false;

    const password = this.registerForm.get(['password'])!.value;
    if (password !== this.registerForm.get(['confirmPassword'])!.value) {
      this.doNotMatch = true;
    } else {
      const numIdentifiant = this.registerForm.get(['numIdentifiant'])!.value;
      const dateNaissance = this.registerForm.get(['dateNaissance'])!.value;
      const telephone = this.registerForm.get(['telephone'])!.value;
      const email = this.registerForm.get(['email'])!.value;
      const login = email;
      this.registerService
        .save({ login, numIdentifiant, dateNaissance, telephone, email, password, langKey: this.languageService.getCurrentLanguage() })
        .subscribe(
          () => (this.success = true),
          response => this.processError(response)
        );
    }
  }

  openLogin(): void {
    this.loginModalService.open();
  }

  private processError(response: HttpErrorResponse): void {
    if (response.status === 400 && response.error.type === LOGIN_ALREADY_USED_TYPE) {
      this.errorUserExists = true;
    } else if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
      this.errorEmailExists = true;
    } else if (response.status === 400 && response.error.type === LOGIN_NOT_FOUND_TYPE) {
      this.errorEmailFound = true;
    } else if (response.status === 400 && response.error.type === IDENTIFIANT_NOT_FOUND_TYPE) {
      this.erroIdentifiantExists = true;
    } else {
      this.error = true;
    }
  }
}
