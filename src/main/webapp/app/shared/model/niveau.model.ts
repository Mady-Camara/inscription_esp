import { IInscription } from 'app/shared/model/inscription.model';
import { IFormation } from 'app/shared/model/formation.model';

export interface INiveau {
  id?: number;
  codeNiveau?: string;
  libelleLong?: string;
  inscriptions?: IInscription[];
  formation?: IFormation;
}

export class Niveau implements INiveau {
  constructor(
    public id?: number,
    public codeNiveau?: string,
    public libelleLong?: string,
    public inscriptions?: IInscription[],
    public formation?: IFormation
  ) {}
}
