import { IUser } from 'app/core/user/user.model';

export interface IAgentScolarite {
  id?: number;
  matricule?: string;
  email?: string;
  motDePasse?: string;
  nom?: string;
  prenom?: string;
  user?: IUser;
}

export class AgentScolarite implements IAgentScolarite {
  constructor(
    public id?: number,
    public matricule?: string,
    public email?: string,
    public motDePasse?: string,
    public nom?: string,
    public prenom?: string,
    public user?: IUser
  ) {}
}
