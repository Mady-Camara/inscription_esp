import { INiveau } from 'app/shared/model/niveau.model';
import { IDepartement } from 'app/shared/model/departement.model';

export interface IFormation {
  id?: number;
  codeFormation?: string;
  libelleLong?: string;
  niveaus?: INiveau[];
  departement?: IDepartement;
}

export class Formation implements IFormation {
  constructor(
    public id?: number,
    public codeFormation?: string,
    public libelleLong?: string,
    public niveaus?: INiveau[],
    public departement?: IDepartement
  ) {}
}
