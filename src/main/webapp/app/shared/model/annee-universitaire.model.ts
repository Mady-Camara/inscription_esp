import { IInscription } from 'app/shared/model/inscription.model';

export interface IAnneeUniversitaire {
  id?: number;
  isActive?: boolean;
  libelle?: string;
  inscriptions?: IInscription[];
}

export class AnneeUniversitaire implements IAnneeUniversitaire {
  constructor(public id?: number, public isActive?: boolean, public libelle?: string, public inscriptions?: IInscription[]) {
    this.isActive = this.isActive || false;
  }
}
