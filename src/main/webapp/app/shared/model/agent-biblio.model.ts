import { IUser } from 'app/core/user/user.model';

export interface IAgentBiblio {
  id?: number;
  matricule?: string;
  email?: string;
  motDePasse?: string;
  nom?: string;
  prenom?: string;
  user?: IUser;
}

export class AgentBiblio implements IAgentBiblio {
  constructor(
    public id?: number,
    public matricule?: string,
    public email?: string,
    public motDePasse?: string,
    public nom?: string,
    public prenom?: string,
    public user?: IUser
  ) {}
}
