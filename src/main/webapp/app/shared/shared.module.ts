import { NgModule } from '@angular/core';
import { InscriptionEspSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';

// import { InscriptionEspAppMaterielModule } from '../app-materiel.module';

@NgModule({
  imports: [InscriptionEspSharedLibsModule /* InscriptionEspAppMaterielModule */],
  declarations: [FindLanguageFromKeyPipe, AlertComponent, AlertErrorComponent, LoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [LoginModalComponent],
  exports: [
    InscriptionEspSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    // InscriptionEspAppMaterielModule,
  ],
})
export class InscriptionEspSharedModule {}
