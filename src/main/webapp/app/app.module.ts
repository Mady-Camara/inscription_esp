import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { InscriptionEspSharedModule } from 'app/shared/shared.module';
import { InscriptionEspCoreModule } from 'app/core/core.module';
import { InscriptionEspAppRoutingModule } from './app-routing.module';
import { InscriptionEspHomeModule } from './home/home.module';
import { InscriptionEspEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

import { AideComponent } from './aide/aide.component';
// import { InscriptionEspAppMaterielModule } from './app-materiel.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserModule,
    InscriptionEspSharedModule,
    InscriptionEspCoreModule,
    InscriptionEspHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    InscriptionEspEntityModule,
    InscriptionEspAppRoutingModule,
    // InscriptionEspAppMaterielModule,
    BrowserAnimationsModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent, AideComponent],
  bootstrap: [MainComponent],
})
export class InscriptionEspAppModule {}
