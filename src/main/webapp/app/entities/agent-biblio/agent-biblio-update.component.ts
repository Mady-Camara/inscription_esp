import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAgentBiblio, AgentBiblio } from 'app/shared/model/agent-biblio.model';
import { AgentBiblioService } from './agent-biblio.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-agent-biblio-update',
  templateUrl: './agent-biblio-update.component.html',
})
export class AgentBiblioUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    matricule: [null, [Validators.required]],
    email: [null, [Validators.required]],
    motDePasse: [null, [Validators.required]],
    nom: [],
    prenom: [],
    user: [],
  });

  constructor(
    protected agentBiblioService: AgentBiblioService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentBiblio }) => {
      this.updateForm(agentBiblio);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(agentBiblio: IAgentBiblio): void {
    this.editForm.patchValue({
      id: agentBiblio.id,
      matricule: agentBiblio.matricule,
      email: agentBiblio.email,
      motDePasse: agentBiblio.motDePasse,
      nom: agentBiblio.nom,
      prenom: agentBiblio.prenom,
      user: agentBiblio.user,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const agentBiblio = this.createFromForm();
    if (agentBiblio.id !== undefined) {
      this.subscribeToSaveResponse(this.agentBiblioService.update(agentBiblio));
    } else {
      this.subscribeToSaveResponse(this.agentBiblioService.create(agentBiblio));
    }
  }

  private createFromForm(): IAgentBiblio {
    return {
      ...new AgentBiblio(),
      id: this.editForm.get(['id'])!.value,
      matricule: this.editForm.get(['matricule'])!.value,
      email: this.editForm.get(['email'])!.value,
      motDePasse: this.editForm.get(['motDePasse'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgentBiblio>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
