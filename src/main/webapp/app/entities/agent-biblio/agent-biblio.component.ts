import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAgentBiblio } from 'app/shared/model/agent-biblio.model';
import { AgentBiblioService } from './agent-biblio.service';
import { AgentBiblioDeleteDialogComponent } from './agent-biblio-delete-dialog.component';

@Component({
  selector: 'jhi-agent-biblio',
  templateUrl: './agent-biblio.component.html',
})
export class AgentBiblioComponent implements OnInit, OnDestroy {
  agentBiblios?: IAgentBiblio[];
  eventSubscriber?: Subscription;

  constructor(
    protected agentBiblioService: AgentBiblioService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.agentBiblioService.query().subscribe((res: HttpResponse<IAgentBiblio[]>) => (this.agentBiblios = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAgentBiblios();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAgentBiblio): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAgentBiblios(): void {
    this.eventSubscriber = this.eventManager.subscribe('agentBiblioListModification', () => this.loadAll());
  }

  delete(agentBiblio: IAgentBiblio): void {
    const modalRef = this.modalService.open(AgentBiblioDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.agentBiblio = agentBiblio;
  }
}
