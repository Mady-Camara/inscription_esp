import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAgentBiblio } from 'app/shared/model/agent-biblio.model';

@Component({
  selector: 'jhi-agent-biblio-detail',
  templateUrl: './agent-biblio-detail.component.html',
})
export class AgentBiblioDetailComponent implements OnInit {
  agentBiblio: IAgentBiblio | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentBiblio }) => (this.agentBiblio = agentBiblio));
  }

  previousState(): void {
    window.history.back();
  }
}
