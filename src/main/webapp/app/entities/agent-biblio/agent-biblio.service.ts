import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAgentBiblio } from 'app/shared/model/agent-biblio.model';

type EntityResponseType = HttpResponse<IAgentBiblio>;
type EntityArrayResponseType = HttpResponse<IAgentBiblio[]>;

import { IEtudiant } from 'app/shared/model/etudiant.model';
import { IInscription } from 'app/shared/model/inscription.model';

@Injectable({ providedIn: 'root' })
export class AgentBiblioService {
  public resourceUrl = SERVER_API_URL + 'api/agent-biblios';

  public uri = SERVER_API_URL + 'api/agent-biblios/currentUserId';

  constructor(protected http: HttpClient) {}

  create(agentBiblio: IAgentBiblio): Observable<EntityResponseType> {
    return this.http.post<IAgentBiblio>(this.resourceUrl, agentBiblio, { observe: 'response' });
  }

  update(agentBiblio: IAgentBiblio): Observable<EntityResponseType> {
    return this.http.put<IAgentBiblio>(this.resourceUrl, agentBiblio, { observe: 'response' });
  }

  findAgentBiblioId(): Observable<HttpResponse<IAgentBiblio>> {
    return this.http.get<IAgentBiblio>(`${this.uri}`, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAgentBiblio>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAgentBiblio[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  regler(etudiant: IEtudiant): Observable<EntityResponseType> {
    return this.http.put<IInscription>(`${this.resourceUrl}/reglebiblio`, etudiant, { observe: 'response' });
  }
}
