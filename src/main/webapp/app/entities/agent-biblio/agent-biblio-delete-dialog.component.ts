import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAgentBiblio } from 'app/shared/model/agent-biblio.model';
import { AgentBiblioService } from './agent-biblio.service';

@Component({
  templateUrl: './agent-biblio-delete-dialog.component.html',
})
export class AgentBiblioDeleteDialogComponent {
  agentBiblio?: IAgentBiblio;

  constructor(
    protected agentBiblioService: AgentBiblioService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.agentBiblioService.delete(id).subscribe(() => {
      this.eventManager.broadcast('agentBiblioListModification');
      this.activeModal.close();
    });
  }
}
