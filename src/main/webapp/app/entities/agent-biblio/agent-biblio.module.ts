import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { InscriptionEspSharedModule } from 'app/shared/shared.module';
import { AgentBiblioComponent } from './agent-biblio.component';
import { AgentBiblioDetailComponent } from './agent-biblio-detail.component';
import { AgentBiblioUpdateComponent } from './agent-biblio-update.component';
import { AgentBiblioDeleteDialogComponent } from './agent-biblio-delete-dialog.component';
import { agentBiblioRoute } from './agent-biblio.route';
import { AgentBiblioDashboardComponent } from './agent-biblio-dashboard.component';

@NgModule({
  imports: [InscriptionEspSharedModule, RouterModule.forChild(agentBiblioRoute)],
  declarations: [
    AgentBiblioComponent,
    AgentBiblioDetailComponent,
    AgentBiblioUpdateComponent,
    AgentBiblioDeleteDialogComponent,
    AgentBiblioDashboardComponent,
  ],
  entryComponents: [AgentBiblioDeleteDialogComponent],
})
export class InscriptionEspAgentBiblioModule {}
