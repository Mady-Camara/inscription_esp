import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IInscription } from 'app/shared/model/inscription.model';
import { InscriptionService } from 'app/entities/inscription/inscription.service';
import { EtudiantService } from 'app/entities/etudiant/etudiant.service';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { HttpResponse } from '@angular/common/http';
@Component({
  selector: 'jhi-etudiant-dashboard',
  templateUrl: './etudiant-dashboard.component.html',
  styleUrls: ['./etudiants.scss'],
})
export class EtudiantDashboardComponent implements OnInit {
  ins: IInscription | null = null;
  etatAssu: string | null = null;
  etudiant: IEtudiant | null = null;
  student: IEtudiant | null = null;
  etude: IEtudiant | null = null;
  isBoursier = false;
  success = false;
  inscription: IInscription | null = null;
  firstFormGroup = this._formBuilder.group({
    numeroCarteEtudiant: [''],
    numeroQuittance: [''],
    numeroCarteIdentite: [''],
  });

  secondFormGroup = this._formBuilder.group({
    nom: ['', Validators.required],
    prenom: ['', Validators.required],
    nomMari: [''],
    dateNaiss: ['', Validators.required],
    sexe: ['', Validators.required],
    lieuNaiss: ['', Validators.required],
    paysNaiss: ['', Validators.required],
    regionNaiss: ['', Validators.required],
    nationalite: ['', Validators.required],
  });

  thirdFormGroup = this._formBuilder.group({
    adresse: ['', Validators.required],
    boitePostale: [''],
    telephone: ['', Validators.required],
    portable: ['', Validators.required],
    email: ['', Validators.required],
  });

  forFormGroup = this._formBuilder.group({
    actSalarieExist: ['', Validators.required],
    statutEtudiant: ['', Validators.required],
    catProfession: [''],
  });

  fiveFormGroup = this._formBuilder.group({
    situationFamiliale: [''],
    nbrEnfant: [''],
  });

  sixFormGroup = this._formBuilder.group({
    anneeEtude: ['', Validators.required],
    cycle: ['', Validators.required],
    departement: ['', Validators.required],
    nbrInscriptionAnt: ['', Validators.required],
    redoublerExist: ['', Validators.required],
    horaireTD: [''],
  });

  sevenFormGroup = this._formBuilder.group({
    bourse: ['', Validators.required],
    typeBourse: [''],
    montantBourse: [''],
    organismeBousier: [''],
    pourcentageExonore: [''],
  });

  heightFormGroup = this._formBuilder.group({
    bac: ['', Validators.required],
    anneeBac: ['', Validators.required],
    mentionBac: ['', Validators.required],
    lieuBac: ['', Validators.required],
    dut: [''],
    anneeDut: [''],
    mentionDut: [''],
    lieuDut: [''],
    licence: [''],
    anneeLicence: [''],
    mentionLicence: [''],
    lieuLicence: [''],
    master: [''],
    anneeMaster: [''],
    mentionMaster: [''],
    lieuMaster: [''],
    doctorat: [''],
    anneeDoctorat: [''],
    mentionDoctorat: [''],
    lieuDoctorat: [''],
  });

  nineFormGroup = this._formBuilder.group({
    nomResponsable: ['', Validators.required],
    prenomResponsable: ['', Validators.required],
    nomMariResponsable: [''],
    lienParente: ['', Validators.required],
    rueQuartier: ['', Validators.required],
    ville: ['', Validators.required],
    boitePostaleResponsable: [''],
    telephoneResponsable: ['', Validators.required],
    portableResponsable: [''],
    faxResponsable: [''],
    emailResponsable: ['', Validators.required],
    resEtudiantTF: ['', Validators.required],
    resAContacterTF: ['', Validators.required],
  });

  tenFormGroup = this._formBuilder.group({
    isApte: ['', Validators.required],
  });

  constructor(private _formBuilder: FormBuilder, private inservice: InscriptionService, private etudiantServe: EtudiantService) {}

  etat(): void {
    if (this.etudiant?.numIdentifiant != null) {
      this.inservice.etat(this.etudiant?.numIdentifiant).subscribe((res: HttpResponse<IInscription>) => (this.inscription = res.body));
    }
    if (this.inscription?.estAssure) {
      this.etatAssu = '';
    } else {
      this.etatAssu = 'bg-secondary  progress-bar-striped progress-bar-animated';
    }
  }

  formulaire(): void {
    const ine = this.firstFormGroup.get(['numeroCarteIdentite'])!.value;
    const nomDuMari = this.secondFormGroup.get(['nomMari'])!.value;
    const sexe = this.secondFormGroup.get(['sexe'])!.value;
    const paysDeNaissance = this.secondFormGroup.get(['paysNaiss'])!.value;
    const regionDeNaissance = this.secondFormGroup.get(['regionNaiss'])!.value;
    const nationalite = this.secondFormGroup.get(['nationalite'])!.value;

    const addresseADakar = this.thirdFormGroup.get(['adresse'])!.value;
    const boitePostal = this.thirdFormGroup.get(['boitePostale'])!.value;
    const telephone = this.thirdFormGroup.get(['telephone'])!.value;
    const portable = this.thirdFormGroup.get(['portable'])!.value;

    const activiteSalariee = this.forFormGroup.get(['actSalarieExist'])!.value;
    const statusEtudiant = this.forFormGroup.get(['statutEtudiant'])!.value;
    const categorieSocioprofessionnelle = this.forFormGroup.get(['catProfession'])!.value;

    const nombreEnfant = this.fiveFormGroup.get(['nbrEnfant'])!.value;

    const bourse = this.sevenFormGroup.get(['bourse'])!.value;
    const natureBourse = this.sevenFormGroup.get(['typeBourse'])!.value;
    const montantBourse = this.sevenFormGroup.get(['montantBourse'])!.value;
    const organismeBoursier = this.sevenFormGroup.get(['organismeBousier'])!.value;

    const nomPersonneAContacter = this.nineFormGroup.get(['nomResponsable'])!.value;
    const prenomPersonneAContacter = this.nineFormGroup.get(['prenomResponsable'])!.value;
    const nomDuMariPersonneAContacter = this.nineFormGroup.get(['nomMariResponsable'])!.value;
    const lienDeParentePersonneAContacter = this.nineFormGroup.get(['lienParente'])!.value;
    const rueQuartierPersonneAContacter = this.nineFormGroup.get(['rueQuartier'])!.value;
    const villePersonneAContacter = this.nineFormGroup.get(['ville'])!.value;
    const boitePostalePersonneAContacter = this.nineFormGroup.get(['boitePostaleResponsable'])!.value;
    const telephonePersonneAContacter = this.nineFormGroup.get(['telephoneResponsable'])!.value;
    const portPersonneAContacter = this.nineFormGroup.get(['portableResponsable'])!.value;
    const faxPersonneAContacter = this.nineFormGroup.get(['faxResponsable'])!.value;
    const emailPersonneAContacter = this.nineFormGroup.get(['emailResponsable'])!.value;
    const responsableEstEtudiant = this.nineFormGroup.get(['resEtudiantTF'])!.value;
    const personneAContacter = this.nineFormGroup.get(['resAContacterTF'])!.value;

    if (bourse === 'Boursier') {
      this.isBoursier = true;
    }

    this.etudiantServe
      .saveE({
        ine,
        nomDuMari,
        sexe,
        paysDeNaissance,
        regionDeNaissance,
        nationalite,
        addresseADakar,
        boitePostal,
        telephone,
        portable,
        activiteSalariee,
        statusEtudiant,
        categorieSocioprofessionnelle,
        nombreEnfant,
        bourse,
        natureBourse,
        montantBourse,
        organismeBoursier,
        nomPersonneAContacter,
        prenomPersonneAContacter,
        nomDuMariPersonneAContacter,
        lienDeParentePersonneAContacter,
        rueQuartierPersonneAContacter,
        villePersonneAContacter,
        boitePostalePersonneAContacter,
        telephonePersonneAContacter,
        portPersonneAContacter,
        faxPersonneAContacter,
        emailPersonneAContacter,
        responsableEstEtudiant,
        personneAContacter,
      })
      .subscribe((res: HttpResponse<IInscription>) => (this.inscription = res.body));

    this.etudiantServe.saveI(this.inscription).subscribe(
      () => (this.success = true),
      () => (this.success = false)
    );
  }

  ngOnInit(): void {
    this.etudiantServe.findEtudiantId().subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
  }
}
