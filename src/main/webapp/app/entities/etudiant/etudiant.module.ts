import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { InscriptionEspSharedModule } from 'app/shared/shared.module';
import { EtudiantComponent } from './etudiant.component';
import { EtudiantDetailComponent } from './etudiant-detail.component';
import { EtudiantUpdateComponent } from './etudiant-update.component';
import { EtudiantDeleteDialogComponent } from './etudiant-delete-dialog.component';
import { etudiantRoute } from './etudiant.route';

import { EtudiantDashboardComponent } from './etudiant-dashboard.component';
import '../../vendor';
import { InscriptionEspAppMaterielModule } from '../../app-materiel.module';

@NgModule({
  imports: [InscriptionEspSharedModule, RouterModule.forChild(etudiantRoute), InscriptionEspAppMaterielModule],
  declarations: [
    EtudiantComponent,
    EtudiantDetailComponent,
    EtudiantUpdateComponent,
    EtudiantDeleteDialogComponent,
    EtudiantDashboardComponent,
  ],
  entryComponents: [EtudiantDeleteDialogComponent],
})
export class InscriptionEspEtudiantModule {}
