import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IEtudiant, Etudiant } from 'app/shared/model/etudiant.model';
import { EtudiantService } from './etudiant.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-etudiant-update',
  templateUrl: './etudiant-update.component.html',
})
export class EtudiantUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  dateNaissanceDp: any;

  editForm = this.fb.group({
    id: [],
    numIdentifiant: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(9)]],
    dateNaissance: [],
    lieuNaissance: [],
    sexe: [],
    ine: [],
    telephone: [null, [Validators.minLength(7)]],
    nomDuMari: [],
    regionDeNaissance: [],
    prenom2: [],
    prenom3: [],
    paysDeNaissance: [],
    nationalite: [],
    addresseADakar: [],
    boitePostal: [],
    portable: [],
    activiteSalariee: [],
    categorieSocioprofessionnelle: [],
    situationFamiliale: [],
    nombreEnfant: [],
    bourse: [],
    natureBourse: [],
    montantBourse: [],
    organismeBoursier: [],
    serieDiplome: [],
    anneeDiplome: [],
    mentionDiplome: [],
    lieuDiplome: [],
    serieDuelDuesDutBts: [],
    anneeDuelDuesDutBts: [],
    mentionDuelDuesDutBts: [],
    lieuDuelDuesDutBts: [],
    serieLicenceComplete: [],
    anneeLicenceComplete: [],
    mentionLicenceComplete: [],
    lieuLicenceComplete: [],
    serieMaster: [],
    anneeMaster: [],
    mentionMaster: [],
    lieu: [],
    serieDoctorat: [],
    anneeDoctorat: [],
    mentionDoctorat: [],
    lieuDoctorat: [],
    nomPersonneAContacter: [],
    nomDuMariPersonneAContacter: [],
    lienDeParentePersonneAContacter: [],
    adressePersonneAContacter: [],
    rueQuartierPersonneAContacter: [],
    villePersonneAContacter: [],
    telephonePersonneAContacter: [],
    prenomPersonneAContacter: [],
    prenom2PersonneAContacter: [],
    prenom3PersonneAContacter: [],
    boitePostalePersonneAContacter: [],
    portPersonneAContacter: [],
    faxPersonneAContacter: [],
    emailPersonneAContacter: [],
    responsableEstEtudiant: [],
    personneAContacter: [],
    statusEtudiant: [],
    user: [],
  });

  constructor(
    protected etudiantService: EtudiantService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ etudiant }) => {
      this.updateForm(etudiant);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(etudiant: IEtudiant): void {
    this.editForm.patchValue({
      id: etudiant.id,
      numIdentifiant: etudiant.numIdentifiant,
      dateNaissance: etudiant.dateNaissance,
      lieuNaissance: etudiant.lieuNaissance,
      sexe: etudiant.sexe,
      ine: etudiant.ine,
      telephone: etudiant.telephone,
      nomDuMari: etudiant.nomDuMari,
      regionDeNaissance: etudiant.regionDeNaissance,
      prenom2: etudiant.prenom2,
      prenom3: etudiant.prenom3,
      paysDeNaissance: etudiant.paysDeNaissance,
      nationalite: etudiant.nationalite,
      addresseADakar: etudiant.addresseADakar,
      boitePostal: etudiant.boitePostal,
      portable: etudiant.portable,
      activiteSalariee: etudiant.activiteSalariee,
      categorieSocioprofessionnelle: etudiant.categorieSocioprofessionnelle,
      situationFamiliale: etudiant.situationFamiliale,
      nombreEnfant: etudiant.nombreEnfant,
      bourse: etudiant.bourse,
      natureBourse: etudiant.natureBourse,
      montantBourse: etudiant.montantBourse,
      organismeBoursier: etudiant.organismeBoursier,
      serieDiplome: etudiant.serieDiplome,
      anneeDiplome: etudiant.anneeDiplome,
      mentionDiplome: etudiant.mentionDiplome,
      lieuDiplome: etudiant.lieuDiplome,
      serieDuelDuesDutBts: etudiant.serieDuelDuesDutBts,
      anneeDuelDuesDutBts: etudiant.anneeDuelDuesDutBts,
      mentionDuelDuesDutBts: etudiant.mentionDuelDuesDutBts,
      lieuDuelDuesDutBts: etudiant.lieuDuelDuesDutBts,
      serieLicenceComplete: etudiant.serieLicenceComplete,
      anneeLicenceComplete: etudiant.anneeLicenceComplete,
      mentionLicenceComplete: etudiant.mentionLicenceComplete,
      lieuLicenceComplete: etudiant.lieuLicenceComplete,
      serieMaster: etudiant.serieMaster,
      anneeMaster: etudiant.anneeMaster,
      mentionMaster: etudiant.mentionMaster,
      lieu: etudiant.lieu,
      serieDoctorat: etudiant.serieDoctorat,
      anneeDoctorat: etudiant.anneeDoctorat,
      mentionDoctorat: etudiant.mentionDoctorat,
      lieuDoctorat: etudiant.lieuDoctorat,
      nomPersonneAContacter: etudiant.nomPersonneAContacter,
      nomDuMariPersonneAContacter: etudiant.nomDuMariPersonneAContacter,
      lienDeParentePersonneAContacter: etudiant.lienDeParentePersonneAContacter,
      adressePersonneAContacter: etudiant.adressePersonneAContacter,
      rueQuartierPersonneAContacter: etudiant.rueQuartierPersonneAContacter,
      villePersonneAContacter: etudiant.villePersonneAContacter,
      telephonePersonneAContacter: etudiant.telephonePersonneAContacter,
      prenomPersonneAContacter: etudiant.prenomPersonneAContacter,
      prenom2PersonneAContacter: etudiant.prenom2PersonneAContacter,
      prenom3PersonneAContacter: etudiant.prenom3PersonneAContacter,
      boitePostalePersonneAContacter: etudiant.boitePostalePersonneAContacter,
      portPersonneAContacter: etudiant.portPersonneAContacter,
      faxPersonneAContacter: etudiant.faxPersonneAContacter,
      emailPersonneAContacter: etudiant.emailPersonneAContacter,
      responsableEstEtudiant: etudiant.responsableEstEtudiant,
      personneAContacter: etudiant.personneAContacter,
      statusEtudiant: etudiant.statusEtudiant,
      user: etudiant.user,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const etudiant = this.createFromForm();
    if (etudiant.id !== undefined) {
      this.subscribeToSaveResponse(this.etudiantService.update(etudiant));
    } else {
      this.subscribeToSaveResponse(this.etudiantService.create(etudiant));
    }
  }

  private createFromForm(): IEtudiant {
    return {
      ...new Etudiant(),
      id: this.editForm.get(['id'])!.value,
      numIdentifiant: this.editForm.get(['numIdentifiant'])!.value,
      dateNaissance: this.editForm.get(['dateNaissance'])!.value,
      lieuNaissance: this.editForm.get(['lieuNaissance'])!.value,
      sexe: this.editForm.get(['sexe'])!.value,
      ine: this.editForm.get(['ine'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      nomDuMari: this.editForm.get(['nomDuMari'])!.value,
      regionDeNaissance: this.editForm.get(['regionDeNaissance'])!.value,
      prenom2: this.editForm.get(['prenom2'])!.value,
      prenom3: this.editForm.get(['prenom3'])!.value,
      paysDeNaissance: this.editForm.get(['paysDeNaissance'])!.value,
      nationalite: this.editForm.get(['nationalite'])!.value,
      addresseADakar: this.editForm.get(['addresseADakar'])!.value,
      boitePostal: this.editForm.get(['boitePostal'])!.value,
      portable: this.editForm.get(['portable'])!.value,
      activiteSalariee: this.editForm.get(['activiteSalariee'])!.value,
      categorieSocioprofessionnelle: this.editForm.get(['categorieSocioprofessionnelle'])!.value,
      situationFamiliale: this.editForm.get(['situationFamiliale'])!.value,
      nombreEnfant: this.editForm.get(['nombreEnfant'])!.value,
      bourse: this.editForm.get(['bourse'])!.value,
      natureBourse: this.editForm.get(['natureBourse'])!.value,
      montantBourse: this.editForm.get(['montantBourse'])!.value,
      organismeBoursier: this.editForm.get(['organismeBoursier'])!.value,
      serieDiplome: this.editForm.get(['serieDiplome'])!.value,
      anneeDiplome: this.editForm.get(['anneeDiplome'])!.value,
      mentionDiplome: this.editForm.get(['mentionDiplome'])!.value,
      lieuDiplome: this.editForm.get(['lieuDiplome'])!.value,
      serieDuelDuesDutBts: this.editForm.get(['serieDuelDuesDutBts'])!.value,
      anneeDuelDuesDutBts: this.editForm.get(['anneeDuelDuesDutBts'])!.value,
      mentionDuelDuesDutBts: this.editForm.get(['mentionDuelDuesDutBts'])!.value,
      lieuDuelDuesDutBts: this.editForm.get(['lieuDuelDuesDutBts'])!.value,
      serieLicenceComplete: this.editForm.get(['serieLicenceComplete'])!.value,
      anneeLicenceComplete: this.editForm.get(['anneeLicenceComplete'])!.value,
      mentionLicenceComplete: this.editForm.get(['mentionLicenceComplete'])!.value,
      lieuLicenceComplete: this.editForm.get(['lieuLicenceComplete'])!.value,
      serieMaster: this.editForm.get(['serieMaster'])!.value,
      anneeMaster: this.editForm.get(['anneeMaster'])!.value,
      mentionMaster: this.editForm.get(['mentionMaster'])!.value,
      lieu: this.editForm.get(['lieu'])!.value,
      serieDoctorat: this.editForm.get(['serieDoctorat'])!.value,
      anneeDoctorat: this.editForm.get(['anneeDoctorat'])!.value,
      mentionDoctorat: this.editForm.get(['mentionDoctorat'])!.value,
      lieuDoctorat: this.editForm.get(['lieuDoctorat'])!.value,
      nomPersonneAContacter: this.editForm.get(['nomPersonneAContacter'])!.value,
      nomDuMariPersonneAContacter: this.editForm.get(['nomDuMariPersonneAContacter'])!.value,
      lienDeParentePersonneAContacter: this.editForm.get(['lienDeParentePersonneAContacter'])!.value,
      adressePersonneAContacter: this.editForm.get(['adressePersonneAContacter'])!.value,
      rueQuartierPersonneAContacter: this.editForm.get(['rueQuartierPersonneAContacter'])!.value,
      villePersonneAContacter: this.editForm.get(['villePersonneAContacter'])!.value,
      telephonePersonneAContacter: this.editForm.get(['telephonePersonneAContacter'])!.value,
      prenomPersonneAContacter: this.editForm.get(['prenomPersonneAContacter'])!.value,
      prenom2PersonneAContacter: this.editForm.get(['prenom2PersonneAContacter'])!.value,
      prenom3PersonneAContacter: this.editForm.get(['prenom3PersonneAContacter'])!.value,
      boitePostalePersonneAContacter: this.editForm.get(['boitePostalePersonneAContacter'])!.value,
      portPersonneAContacter: this.editForm.get(['portPersonneAContacter'])!.value,
      faxPersonneAContacter: this.editForm.get(['faxPersonneAContacter'])!.value,
      emailPersonneAContacter: this.editForm.get(['emailPersonneAContacter'])!.value,
      responsableEstEtudiant: this.editForm.get(['responsableEstEtudiant'])!.value,
      personneAContacter: this.editForm.get(['personneAContacter'])!.value,
      statusEtudiant: this.editForm.get(['statusEtudiant'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEtudiant>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
