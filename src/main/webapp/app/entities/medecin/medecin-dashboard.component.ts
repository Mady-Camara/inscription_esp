import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAssureur } from 'app/shared/model/assureur.model';

import { IEtudiant } from 'app/shared/model/etudiant.model';
import { EtudiantService } from 'app/entities/etudiant/etudiant.service';
import { IInscription } from 'app/shared/model/inscription.model';
import { HttpResponse } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { MedecinService } from 'app/entities/medecin/medecin.service';
import { InscriptionService } from 'app/entities/inscription/inscription.service';

@Component({
  selector: 'jhi-medecin-dashboard',
  templateUrl: './medecin-dashboard.component.html',
})
export class MedecinDashboardComponent implements OnInit {
  assureur: IAssureur | null = null;
  etudiant: IEtudiant | null = null;
  inscription: IInscription | null = null;

  search = new FormControl();
  isDone = false;
  isNotDone = false;
  isApte = false;
  isNotApte = false;
  buttonInapte = true;
  notEtudiant = false;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected service: EtudiantService,
    protected serve: MedecinService,
    protected inservice: InscriptionService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assureur }) => (this.assureur = assureur));
  }

  previousState(): void {
    window.history.back();
  }

  rechercher(): void {
    this.etudiant = null;
    this.isDone = false;
    this.isNotDone = false;
    this.isApte = false;
    this.isNotApte = false;
    this.buttonInapte = true;
    this.notEtudiant = false;
    this.service.findEtudiant(this.search.value).subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
    this.inservice.etat(this.search.value).subscribe((res: HttpResponse<IInscription>) => (this.inscription = res.body));
  }

  apte(): void {
    if (this.etudiant != null) {
      this.serve.apte(this.etudiant).subscribe((res: HttpResponse<IInscription>) => (this.inscription = res.body));
      this.isDone = true;
    } else {
      this.isNotDone = true;
    }
  }

  inapte(): void {
    if (this.etudiant != null) {
      this.serve.inapte(this.etudiant).subscribe((res: HttpResponse<IInscription>) => (this.inscription = res.body));
      this.isApte = true;
    } else {
      this.notEtudiant = true;
    }
  }

  desc(): void {
    this.isDone = false;
    this.isNotDone = false;
    this.isApte = false;
    this.isNotApte = true;
    this.isApte = false;
    this.buttonInapte = false;
    this.notEtudiant = false;
  }
}
