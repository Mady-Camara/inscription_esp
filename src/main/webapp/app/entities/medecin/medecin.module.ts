import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { InscriptionEspSharedModule } from 'app/shared/shared.module';
import { MedecinComponent } from './medecin.component';
import { MedecinDetailComponent } from './medecin-detail.component';
import { MedecinUpdateComponent } from './medecin-update.component';
import { MedecinDeleteDialogComponent } from './medecin-delete-dialog.component';
import { medecinRoute } from './medecin.route';
import { MedecinDashboardComponent } from './medecin-dashboard.component';

@NgModule({
  imports: [InscriptionEspSharedModule, RouterModule.forChild(medecinRoute)],
  declarations: [MedecinComponent, MedecinDetailComponent, MedecinUpdateComponent, MedecinDeleteDialogComponent, MedecinDashboardComponent],
  entryComponents: [MedecinDeleteDialogComponent],
})
export class InscriptionEspMedecinModule {}
