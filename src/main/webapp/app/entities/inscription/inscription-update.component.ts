import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IInscription, Inscription } from 'app/shared/model/inscription.model';
import { InscriptionService } from './inscription.service';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { EtudiantService } from 'app/entities/etudiant/etudiant.service';
import { INiveau } from 'app/shared/model/niveau.model';
import { NiveauService } from 'app/entities/niveau/niveau.service';
import { IAnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';
import { AnneeUniversitaireService } from 'app/entities/annee-universitaire/annee-universitaire.service';

type SelectableEntity = IEtudiant | INiveau | IAnneeUniversitaire;

@Component({
  selector: 'jhi-inscription-update',
  templateUrl: './inscription-update.component.html',
})
export class InscriptionUpdateComponent implements OnInit {
  isSaving = false;
  etudiants: IEtudiant[] = [];
  niveaus: INiveau[] = [];
  anneeuniversitaires: IAnneeUniversitaire[] = [];

  editForm = this.fb.group({
    id: [],
    estApte: [],
    estBoursier: [],
    estAssure: [],
    enRegleBiblio: [],
    situationMatrimoniale: [],
    anneeEtude: [],
    cycle: [],
    departement: [],
    optionChoisie: [],
    nombreInscriptionAnterieur: [],
    redoublezVous: [],
    horaireDesTd: [],
    etudiant: [],
    niveau: [],
    anneeUniversitaire: [],
  });

  constructor(
    protected inscriptionService: InscriptionService,
    protected etudiantService: EtudiantService,
    protected niveauService: NiveauService,
    protected anneeUniversitaireService: AnneeUniversitaireService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ inscription }) => {
      this.updateForm(inscription);

      this.etudiantService.query().subscribe((res: HttpResponse<IEtudiant[]>) => (this.etudiants = res.body || []));

      this.niveauService.query().subscribe((res: HttpResponse<INiveau[]>) => (this.niveaus = res.body || []));

      this.anneeUniversitaireService
        .query()
        .subscribe((res: HttpResponse<IAnneeUniversitaire[]>) => (this.anneeuniversitaires = res.body || []));
    });
  }

  updateForm(inscription: IInscription): void {
    this.editForm.patchValue({
      id: inscription.id,
      estApte: inscription.estApte,
      estBoursier: inscription.estBoursier,
      estAssure: inscription.estAssure,
      enRegleBiblio: inscription.enRegleBiblio,
      situationMatrimoniale: inscription.situationMatrimoniale,
      anneeEtude: inscription.anneeEtude,
      cycle: inscription.cycle,
      departement: inscription.departement,
      optionChoisie: inscription.optionChoisie,
      nombreInscriptionAnterieur: inscription.nombreInscriptionAnterieur,
      redoublezVous: inscription.redoublezVous,
      horaireDesTd: inscription.horaireDesTd,
      etudiant: inscription.etudiant,
      niveau: inscription.niveau,
      anneeUniversitaire: inscription.anneeUniversitaire,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const inscription = this.createFromForm();
    if (inscription.id !== undefined) {
      this.subscribeToSaveResponse(this.inscriptionService.update(inscription));
    } else {
      this.subscribeToSaveResponse(this.inscriptionService.create(inscription));
    }
  }

  private createFromForm(): IInscription {
    return {
      ...new Inscription(),
      id: this.editForm.get(['id'])!.value,
      estApte: this.editForm.get(['estApte'])!.value,
      estBoursier: this.editForm.get(['estBoursier'])!.value,
      estAssure: this.editForm.get(['estAssure'])!.value,
      enRegleBiblio: this.editForm.get(['enRegleBiblio'])!.value,
      situationMatrimoniale: this.editForm.get(['situationMatrimoniale'])!.value,
      anneeEtude: this.editForm.get(['anneeEtude'])!.value,
      cycle: this.editForm.get(['cycle'])!.value,
      departement: this.editForm.get(['departement'])!.value,
      optionChoisie: this.editForm.get(['optionChoisie'])!.value,
      nombreInscriptionAnterieur: this.editForm.get(['nombreInscriptionAnterieur'])!.value,
      redoublezVous: this.editForm.get(['redoublezVous'])!.value,
      horaireDesTd: this.editForm.get(['horaireDesTd'])!.value,
      etudiant: this.editForm.get(['etudiant'])!.value,
      niveau: this.editForm.get(['niveau'])!.value,
      anneeUniversitaire: this.editForm.get(['anneeUniversitaire'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInscription>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
