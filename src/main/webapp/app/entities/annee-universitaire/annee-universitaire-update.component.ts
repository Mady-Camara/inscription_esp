import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAnneeUniversitaire, AnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';
import { AnneeUniversitaireService } from './annee-universitaire.service';

@Component({
  selector: 'jhi-annee-universitaire-update',
  templateUrl: './annee-universitaire-update.component.html',
})
export class AnneeUniversitaireUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    isActive: [],
    libelle: [],
  });

  constructor(
    protected anneeUniversitaireService: AnneeUniversitaireService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ anneeUniversitaire }) => {
      this.updateForm(anneeUniversitaire);
    });
  }

  updateForm(anneeUniversitaire: IAnneeUniversitaire): void {
    this.editForm.patchValue({
      id: anneeUniversitaire.id,
      isActive: anneeUniversitaire.isActive,
      libelle: anneeUniversitaire.libelle,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const anneeUniversitaire = this.createFromForm();
    if (anneeUniversitaire.id !== undefined) {
      this.subscribeToSaveResponse(this.anneeUniversitaireService.update(anneeUniversitaire));
    } else {
      this.subscribeToSaveResponse(this.anneeUniversitaireService.create(anneeUniversitaire));
    }
  }

  private createFromForm(): IAnneeUniversitaire {
    return {
      ...new AnneeUniversitaire(),
      id: this.editForm.get(['id'])!.value,
      isActive: this.editForm.get(['isActive'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAnneeUniversitaire>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
