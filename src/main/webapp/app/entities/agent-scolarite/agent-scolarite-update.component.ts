import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAgentScolarite, AgentScolarite } from 'app/shared/model/agent-scolarite.model';
import { AgentScolariteService } from './agent-scolarite.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-agent-scolarite-update',
  templateUrl: './agent-scolarite-update.component.html',
})
export class AgentScolariteUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    matricule: [null, [Validators.required]],
    email: [null, [Validators.required]],
    motDePasse: [null, [Validators.required]],
    nom: [],
    prenom: [],
    user: [],
  });

  constructor(
    protected agentScolariteService: AgentScolariteService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentScolarite }) => {
      this.updateForm(agentScolarite);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(agentScolarite: IAgentScolarite): void {
    this.editForm.patchValue({
      id: agentScolarite.id,
      matricule: agentScolarite.matricule,
      email: agentScolarite.email,
      motDePasse: agentScolarite.motDePasse,
      nom: agentScolarite.nom,
      prenom: agentScolarite.prenom,
      user: agentScolarite.user,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const agentScolarite = this.createFromForm();
    if (agentScolarite.id !== undefined) {
      this.subscribeToSaveResponse(this.agentScolariteService.update(agentScolarite));
    } else {
      this.subscribeToSaveResponse(this.agentScolariteService.create(agentScolarite));
    }
  }

  private createFromForm(): IAgentScolarite {
    return {
      ...new AgentScolarite(),
      id: this.editForm.get(['id'])!.value,
      matricule: this.editForm.get(['matricule'])!.value,
      email: this.editForm.get(['email'])!.value,
      motDePasse: this.editForm.get(['motDePasse'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAgentScolarite>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
