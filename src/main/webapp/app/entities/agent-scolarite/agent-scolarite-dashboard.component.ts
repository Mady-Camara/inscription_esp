import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IAgentScolarite } from 'app/shared/model/agent-scolarite.model';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';

import { IAssureur } from 'app/shared/model/assureur.model';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { IMedecin } from 'app/shared/model/medecin.model';
import { IAgentBiblio } from 'app/shared/model/agent-biblio.model';
import { AgentScolariteService } from './agent-scolarite.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EtudiantDeleteDialogComponent } from '../etudiant/etudiant-delete-dialog.component';
import { SERVER_API_URL } from 'app/app.constants';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';

@Component({
  selector: 'jhi-agent-scolarite-dashboard',
  templateUrl: './agent-scolarite-dashboard.component.html',
})
export class AgentScolariteDashboardComponent implements OnInit {
  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  agentScolarite: IAgentScolarite | null = null;
  arrayBuffer: any;
  file: File;
  keys: string[];
  etudiant: IEtudiant | null = null;
  agentBiblio: IAgentBiblio | null = null;
  medecin: IMedecin | null = null;
  assureur: IAssureur | null = null;
  choix = new FormControl();
  search = new FormControl();
  message = '';
  fileUploadForm: FormGroup;
  fileInputLabel: string;
  public resourceUrl = SERVER_API_URL + 'api/departements/save-import';
  public resourceUrl1 = SERVER_API_URL + 'api/formations/save-import';
  public resourceUrlEmail = SERVER_API_URL + 'api/formations/send-mail';
  public resourceUrlDept = SERVER_API_URL + 'api/departements/export';

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected agentScolariteService: AgentScolariteService,
    protected modalService: NgbModal,
    protected router: Router,
    protected http: HttpClient,
    protected formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ agentScolarite }) => (this.agentScolarite = agentScolarite));
    this.fileUploadForm = this.formBuilder.group({
      myfile: [''],
    });
  }

  previousState(): void {
    window.history.back();
  }

  incomingfile(event) {
    this.file = event.target.files[0];
  }

  Upload() {
    const reader: FileReader = new FileReader();
    reader.onload = () => {
      this.arrayBuffer = reader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = [];
      for (let i = 0; i !== data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, { type: 'binary' });
      const firstSheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[firstSheetName];
      this.keys = XLSX.utils.sheet_to_json(worksheet);
    };
    reader.readAsArrayBuffer(this.file);
  }

  rechercher(): void {
    this.etudiant = null;
    this.assureur = null;
    this.medecin = null;
    this.agentBiblio = null;
    if (this.choix.value === 'etudiant') {
      this.agentScolariteService.findEtudiant(this.search.value).subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
    } else if (this.choix.value === 'assureur') {
      this.agentScolariteService.findAssureur(this.search.value).subscribe((res: HttpResponse<IAssureur>) => (this.assureur = res.body));
    } else if (this.choix.value === 'medecin') {
      this.agentScolariteService.findMedecin(this.search.value).subscribe((res: HttpResponse<IMedecin>) => (this.medecin = res.body));
    } else if (this.choix.value === 'agent') {
      this.agentScolariteService
        .findAgentBiblio(this.search.value)
        .subscribe((res: HttpResponse<IAgentBiblio>) => (this.agentBiblio = res.body));
    } else {
      this.message = 'Selectionez une entite';
    }
  }

  trackId(item: IEtudiant): number {
    return item.id;
  }

  delete(etudiant: IEtudiant): void {
    const modalRef = this.modalService.open(EtudiantDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.etudiant = etudiant;
  }
  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.fileUploadForm.get('myfile').setValue(file);
    }
  }

  onFormSubmit() {
    if (!this.fileUploadForm.get('myfile').value) {
      alert('Please fill valid details!');
      return false;
    }
    const formData = new FormData();
    formData.append('myfile', this.fileUploadForm.get('myfile').value);

    this.http.post<any>(this.resourceUrl, formData).subscribe();
  }

  onFormSubmit1() {
    if (!this.fileUploadForm.get('myfile').value) {
      alert('Please fill valid details!');
      return false;
    }
    const formData = new FormData();
    formData.append('file', this.fileUploadForm.get('myfile').value);

    this.http.post<any>(this.resourceUrl1, formData).subscribe();
    // this.http.post<any>(this.resourceUrlEmail, "").subscribe();
  }

  /*onExport(){
    //Pour faire une get sur un REST API avec angular, utilise ça directement
    //this.resourceUrlDept;
    //this.http.get<any>(this.resourceUrlDept).subscribe();
    window.location.href = this.resourceUrlDept;
    window.open(this.resourceUrlDept);
    //console.log('export', this.resourceUrlDept);
  }*/

  public onExport(): void {
    const url: string = this.resourceUrlDept;
    this.http.get(url, { responseType: 'blob' }).subscribe((response: Blob) => saveAs(response, 'fileName.xlsx'));
  }
  /*onExport () {
    const blob = new Blob([document.getElementById('exportable').innerHTML], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, "Report.xlsx");
  }*/

  //   //console.log(response);
  //   if (response.statusCode === 200) {
  //     // Reset the file input
  //     this.uploadFileInput.nativeElement.value = "";
  //     this.fileInputLabel = undefined;
  //   }
  // }, error => {
  //   //console.log(error);

  //});
}
