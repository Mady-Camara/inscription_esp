import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAgentScolarite } from 'app/shared/model/agent-scolarite.model';

type EntityResponseType = HttpResponse<IAgentScolarite>;
type EntityArrayResponseType = HttpResponse<IAgentScolarite[]>;

import { IAssureur } from 'app/shared/model/assureur.model';
import { IEtudiant } from 'app/shared/model/etudiant.model';
import { IMedecin } from 'app/shared/model/medecin.model';
import { IAgentBiblio } from 'app/shared/model/agent-biblio.model';

@Injectable({ providedIn: 'root' })
export class AgentScolariteService {
  public resourceUrl = SERVER_API_URL + 'api/agent-scolarites';

  public uri = SERVER_API_URL + 'api/agent-scolarites/currentUserId';

  public medecin = SERVER_API_URL + 'api/agent-scolarites/getMedecin';

  public assureur = SERVER_API_URL + 'api/agent-scolarites/getAssureur';

  public etudiant = SERVER_API_URL + 'api/assureurs/getEtudiant';

  public agentBiblio = SERVER_API_URL + 'api/agent-scolarites/getAgentBiblio';

  constructor(protected http: HttpClient) {}

  create(agentScolarite: IAgentScolarite): Observable<EntityResponseType> {
    return this.http.post<IAgentScolarite>(this.resourceUrl, agentScolarite, { observe: 'response' });
  }

  update(agentScolarite: IAgentScolarite): Observable<EntityResponseType> {
    return this.http.put<IAgentScolarite>(this.resourceUrl, agentScolarite, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAgentScolarite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAgentScolarite[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  // **** ADD ****

  findAgentScolariteId(): Observable<HttpResponse<IAgentScolarite>> {
    return this.http.get<IAgentScolarite>(`${this.uri}`, { observe: 'response' });
  }

  // Recherche des entites
  findEtudiant(id: String): Observable<HttpResponse<IEtudiant>> {
    return this.http.get<IEtudiant>(`${this.etudiant}/${id}`, { observe: 'response' });
  }

  findAgentBiblio(id: String): Observable<HttpResponse<IAgentBiblio>> {
    return this.http.get<IAgentBiblio>(`${this.agentBiblio}/${id}`, { observe: 'response' });
  }

  findAssureur(id: String): Observable<HttpResponse<IAssureur>> {
    return this.http.get<IAssureur>(`${this.assureur}/${id}`, { observe: 'response' });
  }

  findMedecin(id: String): Observable<HttpResponse<IMedecin>> {
    return this.http.get<IMedecin>(`${this.medecin}/${id}`, { observe: 'response' });
  }
}
