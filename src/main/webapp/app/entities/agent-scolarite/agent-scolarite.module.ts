import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { InscriptionEspSharedModule } from 'app/shared/shared.module';
import { AgentScolariteComponent } from './agent-scolarite.component';
import { AgentScolariteDetailComponent } from './agent-scolarite-detail.component';
import { AgentScolariteUpdateComponent } from './agent-scolarite-update.component';
import { AgentScolariteDeleteDialogComponent } from './agent-scolarite-delete-dialog.component';
import { agentScolariteRoute } from './agent-scolarite.route';
import { AgentScolariteDashboardComponent } from './agent-scolarite-dashboard.component';

@NgModule({
  imports: [InscriptionEspSharedModule, RouterModule.forChild(agentScolariteRoute)],
  declarations: [
    AgentScolariteComponent,
    AgentScolariteDetailComponent,
    AgentScolariteUpdateComponent,
    AgentScolariteDeleteDialogComponent,
    AgentScolariteDashboardComponent,
  ],
  entryComponents: [AgentScolariteDeleteDialogComponent],
})
export class InscriptionEspAgentScolariteModule {}
