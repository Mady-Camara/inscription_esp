import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAssureur } from 'app/shared/model/assureur.model';
import { AssureurService } from './assureur.service';
import { AssureurDeleteDialogComponent } from './assureur-delete-dialog.component';

@Component({
  selector: 'jhi-assureur',
  templateUrl: './assureur.component.html',
})
export class AssureurComponent implements OnInit, OnDestroy {
  assureurs?: IAssureur[];
  eventSubscriber?: Subscription;

  constructor(protected assureurService: AssureurService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.assureurService.query().subscribe((res: HttpResponse<IAssureur[]>) => (this.assureurs = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAssureurs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAssureur): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAssureurs(): void {
    this.eventSubscriber = this.eventManager.subscribe('assureurListModification', () => this.loadAll());
  }

  delete(assureur: IAssureur): void {
    const modalRef = this.modalService.open(AssureurDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.assureur = assureur;
  }
}
