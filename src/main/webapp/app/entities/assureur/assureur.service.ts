import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAssureur } from 'app/shared/model/assureur.model';

type EntityResponseType = HttpResponse<IAssureur>;
type EntityArrayResponseType = HttpResponse<IAssureur[]>;

import { IEtudiant } from 'app/shared/model/etudiant.model';
import { IInscription } from 'app/shared/model/inscription.model';

@Injectable({ providedIn: 'root' })
export class AssureurService {
  public resourceUrl = SERVER_API_URL + 'api/assureurs';

  public uri = SERVER_API_URL + 'api/assureurs/currentUserId';

  constructor(protected http: HttpClient) {}

  create(assureur: IAssureur): Observable<EntityResponseType> {
    return this.http.post<IAssureur>(this.resourceUrl, assureur, { observe: 'response' });
  }

  update(assureur: IAssureur): Observable<EntityResponseType> {
    return this.http.put<IAssureur>(this.resourceUrl, assureur, { observe: 'response' });
  }

  findAssureurId(): Observable<HttpResponse<IEtudiant>> {
    return this.http.get<IAssureur>(`${this.uri}`, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAssureur>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAssureur[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  payer(etudiant: IEtudiant): Observable<EntityResponseType> {
    return this.http.put<IInscription>(`${this.resourceUrl}/estassurer`, etudiant, { observe: 'response' });
  }
}
