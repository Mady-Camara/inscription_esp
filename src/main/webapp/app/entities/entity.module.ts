import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'etudiant',
        loadChildren: () => import('./etudiant/etudiant.module').then(m => m.InscriptionEspEtudiantModule),
      },
      {
        path: 'assureur',
        loadChildren: () => import('./assureur/assureur.module').then(m => m.InscriptionEspAssureurModule),
      },
      {
        path: 'agent-biblio',
        loadChildren: () => import('./agent-biblio/agent-biblio.module').then(m => m.InscriptionEspAgentBiblioModule),
      },
      {
        path: 'medecin',
        loadChildren: () => import('./medecin/medecin.module').then(m => m.InscriptionEspMedecinModule),
      },
      {
        path: 'agent-scolarite',
        loadChildren: () => import('./agent-scolarite/agent-scolarite.module').then(m => m.InscriptionEspAgentScolariteModule),
      },
      {
        path: 'departement',
        loadChildren: () => import('./departement/departement.module').then(m => m.InscriptionEspDepartementModule),
      },
      {
        path: 'inscription',
        loadChildren: () => import('./inscription/inscription.module').then(m => m.InscriptionEspInscriptionModule),
      },
      {
        path: 'formation',
        loadChildren: () => import('./formation/formation.module').then(m => m.InscriptionEspFormationModule),
      },
      {
        path: 'annee-universitaire',
        loadChildren: () => import('./annee-universitaire/annee-universitaire.module').then(m => m.InscriptionEspAnneeUniversitaireModule),
      },
      {
        path: 'niveau',
        loadChildren: () => import('./niveau/niveau.module').then(m => m.InscriptionEspNiveauModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class InscriptionEspEntityModule {}
