import { Component, OnInit, OnDestroy, Injectable } from '@angular/core';
import { faAngleRight, faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';

import { IAssureur } from 'app/shared/model/assureur.model';

import { IEtudiant } from 'app/shared/model/etudiant.model';
import { EtudiantService } from 'app/entities/etudiant/etudiant.service';
import { AssureurService } from 'app/entities/assureur/assureur.service';
import { HttpResponse } from '@angular/common/http';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
@Injectable({ providedIn: 'root' })
export class HomeComponent implements OnInit, OnDestroy {
  faAngleRight = faAngleRight;
  faAngleDown = faAngleDown;
  etatpos = 'faAngleRight';
  account: Account | null = null;
  authSubscription?: Subscription;

  assureur: IAssureur | null = null;
  etudiant: IEtudiant | null = null;

  search = new FormControl();
  isDone = false;

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    protected service: EtudiantService,
    protected serve: AssureurService
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.service.findEtudiantId().subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  etat(): void {
    if (this.etatpos === 'faAngleRight') {
      this.etatpos = 'faAngleDown';
    } else {
      this.etatpos = 'faAngleRight';
    }
  }

  rechercher(): void {
    this.service.findEtudiant(this.search.value).subscribe((res: HttpResponse<IEtudiant>) => (this.etudiant = res.body));
  }

  payer(): void {
    this.serve.payer(this.search.value);
    this.isDone = true;
  }
}
