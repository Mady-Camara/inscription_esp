package sn.esp.inscription.web.rest;

import sn.esp.inscription.InscriptionEspApp;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.repository.InscriptionRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.esp.inscription.domain.enumeration.EnumSituation;
import sn.esp.inscription.domain.enumeration.EnumNombreInscriptionAnterieure;
import sn.esp.inscription.domain.enumeration.EnumHoraireTd;
/**
 * Integration tests for the {@link InscriptionResource} REST controller.
 */
@SpringBootTest(classes = InscriptionEspApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class InscriptionResourceIT {

    private static final Boolean DEFAULT_EST_APTE = false;
    private static final Boolean UPDATED_EST_APTE = true;

    private static final Boolean DEFAULT_EST_BOURSIER = false;
    private static final Boolean UPDATED_EST_BOURSIER = true;

    private static final Boolean DEFAULT_EST_ASSURE = false;
    private static final Boolean UPDATED_EST_ASSURE = true;

    private static final Boolean DEFAULT_EN_REGLE_BIBLIO = false;
    private static final Boolean UPDATED_EN_REGLE_BIBLIO = true;

    private static final EnumSituation DEFAULT_SITUATION_MATRIMONIALE = EnumSituation.Celibataire;
    private static final EnumSituation UPDATED_SITUATION_MATRIMONIALE = EnumSituation.MarieSansEnfant;

    private static final String DEFAULT_ANNEE_ETUDE = "AAAAAAAAAA";
    private static final String UPDATED_ANNEE_ETUDE = "BBBBBBBBBB";

    private static final Integer DEFAULT_CYCLE = 1;
    private static final Integer UPDATED_CYCLE = 2;

    private static final String DEFAULT_DEPARTEMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTEMENT = "BBBBBBBBBB";

    private static final String DEFAULT_OPTION_CHOISIE = "AAAAAAAAAA";
    private static final String UPDATED_OPTION_CHOISIE = "BBBBBBBBBB";

    private static final EnumNombreInscriptionAnterieure DEFAULT_NOMBRE_INSCRIPTION_ANTERIEUR = EnumNombreInscriptionAnterieure.ANNEE1;
    private static final EnumNombreInscriptionAnterieure UPDATED_NOMBRE_INSCRIPTION_ANTERIEUR = EnumNombreInscriptionAnterieure.ANNEE2;

    private static final Boolean DEFAULT_REDOUBLEZ_VOUS = false;
    private static final Boolean UPDATED_REDOUBLEZ_VOUS = true;

    private static final EnumHoraireTd DEFAULT_HORAIRE_DES_TD = EnumHoraireTd.AVANT18H;
    private static final EnumHoraireTd UPDATED_HORAIRE_DES_TD = EnumHoraireTd.APRES18H;

    @Autowired
    private InscriptionRepository inscriptionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInscriptionMockMvc;

    private Inscription inscription;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Inscription createEntity(EntityManager em) {
        Inscription inscription = new Inscription()
            .estApte(DEFAULT_EST_APTE)
            .estBoursier(DEFAULT_EST_BOURSIER)
            .estAssure(DEFAULT_EST_ASSURE)
            .enRegleBiblio(DEFAULT_EN_REGLE_BIBLIO)
            .situationMatrimoniale(DEFAULT_SITUATION_MATRIMONIALE)
            .anneeEtude(DEFAULT_ANNEE_ETUDE)
            .cycle(DEFAULT_CYCLE)
            .departement(DEFAULT_DEPARTEMENT)
            .optionChoisie(DEFAULT_OPTION_CHOISIE)
            .nombreInscriptionAnterieur(DEFAULT_NOMBRE_INSCRIPTION_ANTERIEUR)
            .redoublezVous(DEFAULT_REDOUBLEZ_VOUS)
            .horaireDesTd(DEFAULT_HORAIRE_DES_TD);
        return inscription;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Inscription createUpdatedEntity(EntityManager em) {
        Inscription inscription = new Inscription()
            .estApte(UPDATED_EST_APTE)
            .estBoursier(UPDATED_EST_BOURSIER)
            .estAssure(UPDATED_EST_ASSURE)
            .enRegleBiblio(UPDATED_EN_REGLE_BIBLIO)
            .situationMatrimoniale(UPDATED_SITUATION_MATRIMONIALE)
            .anneeEtude(UPDATED_ANNEE_ETUDE)
            .cycle(UPDATED_CYCLE)
            .departement(UPDATED_DEPARTEMENT)
            .optionChoisie(UPDATED_OPTION_CHOISIE)
            .nombreInscriptionAnterieur(UPDATED_NOMBRE_INSCRIPTION_ANTERIEUR)
            .redoublezVous(UPDATED_REDOUBLEZ_VOUS)
            .horaireDesTd(UPDATED_HORAIRE_DES_TD);
        return inscription;
    }

    @BeforeEach
    public void initTest() {
        inscription = createEntity(em);
    }

    @Test
    @Transactional
    public void createInscription() throws Exception {
        int databaseSizeBeforeCreate = inscriptionRepository.findAll().size();
        // Create the Inscription
        restInscriptionMockMvc.perform(post("/api/inscriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inscription)))
            .andExpect(status().isCreated());

        // Validate the Inscription in the database
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeCreate + 1);
        Inscription testInscription = inscriptionList.get(inscriptionList.size() - 1);
        assertThat(testInscription.isEstApte()).isEqualTo(DEFAULT_EST_APTE);
        assertThat(testInscription.isEstBoursier()).isEqualTo(DEFAULT_EST_BOURSIER);
        assertThat(testInscription.isEstAssure()).isEqualTo(DEFAULT_EST_ASSURE);
        assertThat(testInscription.isEnRegleBiblio()).isEqualTo(DEFAULT_EN_REGLE_BIBLIO);
        assertThat(testInscription.getSituationMatrimoniale()).isEqualTo(DEFAULT_SITUATION_MATRIMONIALE);
        assertThat(testInscription.getAnneeEtude()).isEqualTo(DEFAULT_ANNEE_ETUDE);
        assertThat(testInscription.getCycle()).isEqualTo(DEFAULT_CYCLE);
        assertThat(testInscription.getDepartement()).isEqualTo(DEFAULT_DEPARTEMENT);
        assertThat(testInscription.getOptionChoisie()).isEqualTo(DEFAULT_OPTION_CHOISIE);
        assertThat(testInscription.getNombreInscriptionAnterieur()).isEqualTo(DEFAULT_NOMBRE_INSCRIPTION_ANTERIEUR);
        assertThat(testInscription.isRedoublezVous()).isEqualTo(DEFAULT_REDOUBLEZ_VOUS);
        assertThat(testInscription.getHoraireDesTd()).isEqualTo(DEFAULT_HORAIRE_DES_TD);
    }

    @Test
    @Transactional
    public void createInscriptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inscriptionRepository.findAll().size();

        // Create the Inscription with an existing ID
        inscription.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInscriptionMockMvc.perform(post("/api/inscriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inscription)))
            .andExpect(status().isBadRequest());

        // Validate the Inscription in the database
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllInscriptions() throws Exception {
        // Initialize the database
        inscriptionRepository.saveAndFlush(inscription);

        // Get all the inscriptionList
        restInscriptionMockMvc.perform(get("/api/inscriptions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inscription.getId().intValue())))
            .andExpect(jsonPath("$.[*].estApte").value(hasItem(DEFAULT_EST_APTE.booleanValue())))
            .andExpect(jsonPath("$.[*].estBoursier").value(hasItem(DEFAULT_EST_BOURSIER.booleanValue())))
            .andExpect(jsonPath("$.[*].estAssure").value(hasItem(DEFAULT_EST_ASSURE.booleanValue())))
            .andExpect(jsonPath("$.[*].enRegleBiblio").value(hasItem(DEFAULT_EN_REGLE_BIBLIO.booleanValue())))
            .andExpect(jsonPath("$.[*].situationMatrimoniale").value(hasItem(DEFAULT_SITUATION_MATRIMONIALE.toString())))
            .andExpect(jsonPath("$.[*].anneeEtude").value(hasItem(DEFAULT_ANNEE_ETUDE)))
            .andExpect(jsonPath("$.[*].cycle").value(hasItem(DEFAULT_CYCLE)))
            .andExpect(jsonPath("$.[*].departement").value(hasItem(DEFAULT_DEPARTEMENT)))
            .andExpect(jsonPath("$.[*].optionChoisie").value(hasItem(DEFAULT_OPTION_CHOISIE)))
            .andExpect(jsonPath("$.[*].nombreInscriptionAnterieur").value(hasItem(DEFAULT_NOMBRE_INSCRIPTION_ANTERIEUR.toString())))
            .andExpect(jsonPath("$.[*].redoublezVous").value(hasItem(DEFAULT_REDOUBLEZ_VOUS.booleanValue())))
            .andExpect(jsonPath("$.[*].horaireDesTd").value(hasItem(DEFAULT_HORAIRE_DES_TD.toString())));
    }
    
    @Test
    @Transactional
    public void getInscription() throws Exception {
        // Initialize the database
        inscriptionRepository.saveAndFlush(inscription);

        // Get the inscription
        restInscriptionMockMvc.perform(get("/api/inscriptions/{id}", inscription.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(inscription.getId().intValue()))
            .andExpect(jsonPath("$.estApte").value(DEFAULT_EST_APTE.booleanValue()))
            .andExpect(jsonPath("$.estBoursier").value(DEFAULT_EST_BOURSIER.booleanValue()))
            .andExpect(jsonPath("$.estAssure").value(DEFAULT_EST_ASSURE.booleanValue()))
            .andExpect(jsonPath("$.enRegleBiblio").value(DEFAULT_EN_REGLE_BIBLIO.booleanValue()))
            .andExpect(jsonPath("$.situationMatrimoniale").value(DEFAULT_SITUATION_MATRIMONIALE.toString()))
            .andExpect(jsonPath("$.anneeEtude").value(DEFAULT_ANNEE_ETUDE))
            .andExpect(jsonPath("$.cycle").value(DEFAULT_CYCLE))
            .andExpect(jsonPath("$.departement").value(DEFAULT_DEPARTEMENT))
            .andExpect(jsonPath("$.optionChoisie").value(DEFAULT_OPTION_CHOISIE))
            .andExpect(jsonPath("$.nombreInscriptionAnterieur").value(DEFAULT_NOMBRE_INSCRIPTION_ANTERIEUR.toString()))
            .andExpect(jsonPath("$.redoublezVous").value(DEFAULT_REDOUBLEZ_VOUS.booleanValue()))
            .andExpect(jsonPath("$.horaireDesTd").value(DEFAULT_HORAIRE_DES_TD.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingInscription() throws Exception {
        // Get the inscription
        restInscriptionMockMvc.perform(get("/api/inscriptions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInscription() throws Exception {
        // Initialize the database
        inscriptionRepository.saveAndFlush(inscription);

        int databaseSizeBeforeUpdate = inscriptionRepository.findAll().size();

        // Update the inscription
        Inscription updatedInscription = inscriptionRepository.findById(inscription.getId()).get();
        // Disconnect from session so that the updates on updatedInscription are not directly saved in db
        em.detach(updatedInscription);
        updatedInscription
            .estApte(UPDATED_EST_APTE)
            .estBoursier(UPDATED_EST_BOURSIER)
            .estAssure(UPDATED_EST_ASSURE)
            .enRegleBiblio(UPDATED_EN_REGLE_BIBLIO)
            .situationMatrimoniale(UPDATED_SITUATION_MATRIMONIALE)
            .anneeEtude(UPDATED_ANNEE_ETUDE)
            .cycle(UPDATED_CYCLE)
            .departement(UPDATED_DEPARTEMENT)
            .optionChoisie(UPDATED_OPTION_CHOISIE)
            .nombreInscriptionAnterieur(UPDATED_NOMBRE_INSCRIPTION_ANTERIEUR)
            .redoublezVous(UPDATED_REDOUBLEZ_VOUS)
            .horaireDesTd(UPDATED_HORAIRE_DES_TD);

        restInscriptionMockMvc.perform(put("/api/inscriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedInscription)))
            .andExpect(status().isOk());

        // Validate the Inscription in the database
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeUpdate);
        Inscription testInscription = inscriptionList.get(inscriptionList.size() - 1);
        assertThat(testInscription.isEstApte()).isEqualTo(UPDATED_EST_APTE);
        assertThat(testInscription.isEstBoursier()).isEqualTo(UPDATED_EST_BOURSIER);
        assertThat(testInscription.isEstAssure()).isEqualTo(UPDATED_EST_ASSURE);
        assertThat(testInscription.isEnRegleBiblio()).isEqualTo(UPDATED_EN_REGLE_BIBLIO);
        assertThat(testInscription.getSituationMatrimoniale()).isEqualTo(UPDATED_SITUATION_MATRIMONIALE);
        assertThat(testInscription.getAnneeEtude()).isEqualTo(UPDATED_ANNEE_ETUDE);
        assertThat(testInscription.getCycle()).isEqualTo(UPDATED_CYCLE);
        assertThat(testInscription.getDepartement()).isEqualTo(UPDATED_DEPARTEMENT);
        assertThat(testInscription.getOptionChoisie()).isEqualTo(UPDATED_OPTION_CHOISIE);
        assertThat(testInscription.getNombreInscriptionAnterieur()).isEqualTo(UPDATED_NOMBRE_INSCRIPTION_ANTERIEUR);
        assertThat(testInscription.isRedoublezVous()).isEqualTo(UPDATED_REDOUBLEZ_VOUS);
        assertThat(testInscription.getHoraireDesTd()).isEqualTo(UPDATED_HORAIRE_DES_TD);
    }

    @Test
    @Transactional
    public void updateNonExistingInscription() throws Exception {
        int databaseSizeBeforeUpdate = inscriptionRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInscriptionMockMvc.perform(put("/api/inscriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(inscription)))
            .andExpect(status().isBadRequest());

        // Validate the Inscription in the database
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInscription() throws Exception {
        // Initialize the database
        inscriptionRepository.saveAndFlush(inscription);

        int databaseSizeBeforeDelete = inscriptionRepository.findAll().size();

        // Delete the inscription
        restInscriptionMockMvc.perform(delete("/api/inscriptions/{id}", inscription.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Inscription> inscriptionList = inscriptionRepository.findAll();
        assertThat(inscriptionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
