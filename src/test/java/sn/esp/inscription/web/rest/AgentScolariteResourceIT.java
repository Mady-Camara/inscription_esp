package sn.esp.inscription.web.rest;

import sn.esp.inscription.InscriptionEspApp;
import sn.esp.inscription.domain.AgentScolarite;
import sn.esp.inscription.repository.AgentScolariteRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AgentScolariteResource} REST controller.
 */
@SpringBootTest(classes = InscriptionEspApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AgentScolariteResourceIT {

    private static final String DEFAULT_MATRICULE = "AAAAAAAAAA";
    private static final String UPDATED_MATRICULE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_MOT_DE_PASSE = "AAAAAAAAAA";
    private static final String UPDATED_MOT_DE_PASSE = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    @Autowired
    private AgentScolariteRepository agentScolariteRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAgentScolariteMockMvc;

    private AgentScolarite agentScolarite;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentScolarite createEntity(EntityManager em) {
        AgentScolarite agentScolarite = new AgentScolarite()
            .matricule(DEFAULT_MATRICULE)
            .email(DEFAULT_EMAIL)
            .motDePasse(DEFAULT_MOT_DE_PASSE)
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM);
        return agentScolarite;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AgentScolarite createUpdatedEntity(EntityManager em) {
        AgentScolarite agentScolarite = new AgentScolarite()
            .matricule(UPDATED_MATRICULE)
            .email(UPDATED_EMAIL)
            .motDePasse(UPDATED_MOT_DE_PASSE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM);
        return agentScolarite;
    }

    @BeforeEach
    public void initTest() {
        agentScolarite = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgentScolarite() throws Exception {
        int databaseSizeBeforeCreate = agentScolariteRepository.findAll().size();
        // Create the AgentScolarite
        restAgentScolariteMockMvc.perform(post("/api/agent-scolarites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isCreated());

        // Validate the AgentScolarite in the database
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeCreate + 1);
        AgentScolarite testAgentScolarite = agentScolariteList.get(agentScolariteList.size() - 1);
        assertThat(testAgentScolarite.getMatricule()).isEqualTo(DEFAULT_MATRICULE);
        assertThat(testAgentScolarite.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAgentScolarite.getMotDePasse()).isEqualTo(DEFAULT_MOT_DE_PASSE);
        assertThat(testAgentScolarite.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testAgentScolarite.getPrenom()).isEqualTo(DEFAULT_PRENOM);
    }

    @Test
    @Transactional
    public void createAgentScolariteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agentScolariteRepository.findAll().size();

        // Create the AgentScolarite with an existing ID
        agentScolarite.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgentScolariteMockMvc.perform(post("/api/agent-scolarites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isBadRequest());

        // Validate the AgentScolarite in the database
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMatriculeIsRequired() throws Exception {
        int databaseSizeBeforeTest = agentScolariteRepository.findAll().size();
        // set the field null
        agentScolarite.setMatricule(null);

        // Create the AgentScolarite, which fails.


        restAgentScolariteMockMvc.perform(post("/api/agent-scolarites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isBadRequest());

        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = agentScolariteRepository.findAll().size();
        // set the field null
        agentScolarite.setEmail(null);

        // Create the AgentScolarite, which fails.


        restAgentScolariteMockMvc.perform(post("/api/agent-scolarites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isBadRequest());

        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMotDePasseIsRequired() throws Exception {
        int databaseSizeBeforeTest = agentScolariteRepository.findAll().size();
        // set the field null
        agentScolarite.setMotDePasse(null);

        // Create the AgentScolarite, which fails.


        restAgentScolariteMockMvc.perform(post("/api/agent-scolarites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isBadRequest());

        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAgentScolarites() throws Exception {
        // Initialize the database
        agentScolariteRepository.saveAndFlush(agentScolarite);

        // Get all the agentScolariteList
        restAgentScolariteMockMvc.perform(get("/api/agent-scolarites?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agentScolarite.getId().intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].motDePasse").value(hasItem(DEFAULT_MOT_DE_PASSE)))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)));
    }
    
    @Test
    @Transactional
    public void getAgentScolarite() throws Exception {
        // Initialize the database
        agentScolariteRepository.saveAndFlush(agentScolarite);

        // Get the agentScolarite
        restAgentScolariteMockMvc.perform(get("/api/agent-scolarites/{id}", agentScolarite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(agentScolarite.getId().intValue()))
            .andExpect(jsonPath("$.matricule").value(DEFAULT_MATRICULE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.motDePasse").value(DEFAULT_MOT_DE_PASSE))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM));
    }
    @Test
    @Transactional
    public void getNonExistingAgentScolarite() throws Exception {
        // Get the agentScolarite
        restAgentScolariteMockMvc.perform(get("/api/agent-scolarites/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgentScolarite() throws Exception {
        // Initialize the database
        agentScolariteRepository.saveAndFlush(agentScolarite);

        int databaseSizeBeforeUpdate = agentScolariteRepository.findAll().size();

        // Update the agentScolarite
        AgentScolarite updatedAgentScolarite = agentScolariteRepository.findById(agentScolarite.getId()).get();
        // Disconnect from session so that the updates on updatedAgentScolarite are not directly saved in db
        em.detach(updatedAgentScolarite);
        updatedAgentScolarite
            .matricule(UPDATED_MATRICULE)
            .email(UPDATED_EMAIL)
            .motDePasse(UPDATED_MOT_DE_PASSE)
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM);

        restAgentScolariteMockMvc.perform(put("/api/agent-scolarites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAgentScolarite)))
            .andExpect(status().isOk());

        // Validate the AgentScolarite in the database
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeUpdate);
        AgentScolarite testAgentScolarite = agentScolariteList.get(agentScolariteList.size() - 1);
        assertThat(testAgentScolarite.getMatricule()).isEqualTo(UPDATED_MATRICULE);
        assertThat(testAgentScolarite.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAgentScolarite.getMotDePasse()).isEqualTo(UPDATED_MOT_DE_PASSE);
        assertThat(testAgentScolarite.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testAgentScolarite.getPrenom()).isEqualTo(UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void updateNonExistingAgentScolarite() throws Exception {
        int databaseSizeBeforeUpdate = agentScolariteRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAgentScolariteMockMvc.perform(put("/api/agent-scolarites")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(agentScolarite)))
            .andExpect(status().isBadRequest());

        // Validate the AgentScolarite in the database
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAgentScolarite() throws Exception {
        // Initialize the database
        agentScolariteRepository.saveAndFlush(agentScolarite);

        int databaseSizeBeforeDelete = agentScolariteRepository.findAll().size();

        // Delete the agentScolarite
        restAgentScolariteMockMvc.perform(delete("/api/agent-scolarites/{id}", agentScolarite.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AgentScolarite> agentScolariteList = agentScolariteRepository.findAll();
        assertThat(agentScolariteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
