package sn.esp.inscription.web.rest;

import sn.esp.inscription.InscriptionEspApp;
import sn.esp.inscription.domain.Etudiant;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.repository.EtudiantRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.esp.inscription.domain.enumeration.EnumSexe;
import sn.esp.inscription.domain.enumeration.EnumBourse;
import sn.esp.inscription.domain.enumeration.EnumNatureBourse;
import sn.esp.inscription.domain.enumeration.EnumStatusEtudiant;
/**
 * Integration tests for the {@link EtudiantResource} REST controller.
 */
@SpringBootTest(classes = InscriptionEspApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class EtudiantResourceIT {

    private static final String DEFAULT_NUM_IDENTIFIANT = "AAAAAAAAA";
    private static final String UPDATED_NUM_IDENTIFIANT = "BBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_NAISSANCE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_NAISSANCE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LIEU_NAISSANCE = "AAAAAAAAAA";
    private static final String UPDATED_LIEU_NAISSANCE = "BBBBBBBBBB";

    private static final EnumSexe DEFAULT_SEXE = EnumSexe.Masculin;
    private static final EnumSexe UPDATED_SEXE = EnumSexe.Feminin;

    private static final String DEFAULT_INE = "AAAAAAAAAA";
    private static final String UPDATED_INE = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBB";

    private static final String DEFAULT_NOM_DU_MARI = "AAAAAAAAAA";
    private static final String UPDATED_NOM_DU_MARI = "BBBBBBBBBB";

    private static final String DEFAULT_REGION_DE_NAISSANCE = "AAAAAAAAAA";
    private static final String UPDATED_REGION_DE_NAISSANCE = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM_2 = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM_2 = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM_3 = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM_3 = "BBBBBBBBBB";

    private static final String DEFAULT_PAYS_DE_NAISSANCE = "AAAAAAAAAA";
    private static final String UPDATED_PAYS_DE_NAISSANCE = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONALITE = "AAAAAAAAAA";
    private static final String UPDATED_NATIONALITE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESSE_A_DAKAR = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESSE_A_DAKAR = "BBBBBBBBBB";

    private static final String DEFAULT_BOITE_POSTAL = "AAAAAAAAAA";
    private static final String UPDATED_BOITE_POSTAL = "BBBBBBBBBB";

    private static final String DEFAULT_PORTABLE = "AAAAAAAAAA";
    private static final String UPDATED_PORTABLE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVITE_SALARIEE = false;
    private static final Boolean UPDATED_ACTIVITE_SALARIEE = true;

    private static final String DEFAULT_CATEGORIE_SOCIOPROFESSIONNELLE = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORIE_SOCIOPROFESSIONNELLE = "BBBBBBBBBB";

    private static final String DEFAULT_SITUATION_FAMILIALE = "AAAAAAAAAA";
    private static final String UPDATED_SITUATION_FAMILIALE = "BBBBBBBBBB";

    private static final Integer DEFAULT_NOMBRE_ENFANT = 1;
    private static final Integer UPDATED_NOMBRE_ENFANT = 2;

    private static final EnumBourse DEFAULT_BOURSE = EnumBourse.NONBOURSIER;
    private static final EnumBourse UPDATED_BOURSE = EnumBourse.BOURSIER;

    private static final EnumNatureBourse DEFAULT_NATURE_BOURSE = EnumNatureBourse.NATIONALE;
    private static final EnumNatureBourse UPDATED_NATURE_BOURSE = EnumNatureBourse.ETRANGERE;

    private static final Integer DEFAULT_MONTANT_BOURSE = 1;
    private static final Integer UPDATED_MONTANT_BOURSE = 2;

    private static final String DEFAULT_ORGANISME_BOURSIER = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISME_BOURSIER = "BBBBBBBBBB";

    private static final String DEFAULT_SERIE_DIPLOME = "AAAAAAAAAA";
    private static final String UPDATED_SERIE_DIPLOME = "BBBBBBBBBB";

    private static final String DEFAULT_ANNEE_DIPLOME = "AAAAAAAAAA";
    private static final String UPDATED_ANNEE_DIPLOME = "BBBBBBBBBB";

    private static final String DEFAULT_MENTION_DIPLOME = "AAAAAAAAAA";
    private static final String UPDATED_MENTION_DIPLOME = "BBBBBBBBBB";

    private static final String DEFAULT_LIEU_DIPLOME = "AAAAAAAAAA";
    private static final String UPDATED_LIEU_DIPLOME = "BBBBBBBBBB";

    private static final String DEFAULT_SERIE_DUEL_DUES_DUT_BTS = "AAAAAAAAAA";
    private static final String UPDATED_SERIE_DUEL_DUES_DUT_BTS = "BBBBBBBBBB";

    private static final String DEFAULT_ANNEE_DUEL_DUES_DUT_BTS = "AAAAAAAAAA";
    private static final String UPDATED_ANNEE_DUEL_DUES_DUT_BTS = "BBBBBBBBBB";

    private static final String DEFAULT_MENTION_DUEL_DUES_DUT_BTS = "AAAAAAAAAA";
    private static final String UPDATED_MENTION_DUEL_DUES_DUT_BTS = "BBBBBBBBBB";

    private static final String DEFAULT_LIEU_DUEL_DUES_DUT_BTS = "AAAAAAAAAA";
    private static final String UPDATED_LIEU_DUEL_DUES_DUT_BTS = "BBBBBBBBBB";

    private static final String DEFAULT_SERIE_LICENCE_COMPLETE = "AAAAAAAAAA";
    private static final String UPDATED_SERIE_LICENCE_COMPLETE = "BBBBBBBBBB";

    private static final String DEFAULT_ANNEE_LICENCE_COMPLETE = "AAAAAAAAAA";
    private static final String UPDATED_ANNEE_LICENCE_COMPLETE = "BBBBBBBBBB";

    private static final String DEFAULT_MENTION_LICENCE_COMPLETE = "AAAAAAAAAA";
    private static final String UPDATED_MENTION_LICENCE_COMPLETE = "BBBBBBBBBB";

    private static final String DEFAULT_LIEU_LICENCE_COMPLETE = "AAAAAAAAAA";
    private static final String UPDATED_LIEU_LICENCE_COMPLETE = "BBBBBBBBBB";

    private static final String DEFAULT_SERIE_MASTER = "AAAAAAAAAA";
    private static final String UPDATED_SERIE_MASTER = "BBBBBBBBBB";

    private static final String DEFAULT_ANNEE_MASTER = "AAAAAAAAAA";
    private static final String UPDATED_ANNEE_MASTER = "BBBBBBBBBB";

    private static final String DEFAULT_MENTION_MASTER = "AAAAAAAAAA";
    private static final String UPDATED_MENTION_MASTER = "BBBBBBBBBB";

    private static final String DEFAULT_LIEU = "AAAAAAAAAA";
    private static final String UPDATED_LIEU = "BBBBBBBBBB";

    private static final String DEFAULT_SERIE_DOCTORAT = "AAAAAAAAAA";
    private static final String UPDATED_SERIE_DOCTORAT = "BBBBBBBBBB";

    private static final String DEFAULT_ANNEE_DOCTORAT = "AAAAAAAAAA";
    private static final String UPDATED_ANNEE_DOCTORAT = "BBBBBBBBBB";

    private static final String DEFAULT_MENTION_DOCTORAT = "AAAAAAAAAA";
    private static final String UPDATED_MENTION_DOCTORAT = "BBBBBBBBBB";

    private static final String DEFAULT_LIEU_DOCTORAT = "AAAAAAAAAA";
    private static final String UPDATED_LIEU_DOCTORAT = "BBBBBBBBBB";

    private static final String DEFAULT_NOM_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_NOM_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_NOM_DU_MARI_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_NOM_DU_MARI_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_LIEN_DE_PARENTE_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_LIEN_DE_PARENTE_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_RUE_QUARTIER_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_RUE_QUARTIER_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_VILLE_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_VILLE_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM_2_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM_2_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM_3_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM_3_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_BOITE_POSTALE_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_BOITE_POSTALE_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_PORT_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_PORT_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_FAX_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_FAX_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_PERSONNE_A_CONTACTER = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_PERSONNE_A_CONTACTER = "BBBBBBBBBB";

    private static final Boolean DEFAULT_RESPONSABLE_EST_ETUDIANT = false;
    private static final Boolean UPDATED_RESPONSABLE_EST_ETUDIANT = true;

    private static final Boolean DEFAULT_PERSONNE_A_CONTACTER = false;
    private static final Boolean UPDATED_PERSONNE_A_CONTACTER = true;

    private static final EnumStatusEtudiant DEFAULT_STATUS_ETUDIANT = EnumStatusEtudiant.REGIMENORMAL;
    private static final EnumStatusEtudiant UPDATED_STATUS_ETUDIANT = EnumStatusEtudiant.REGIMESALARIE;

    @Autowired
    private EtudiantRepository etudiantRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEtudiantMockMvc;

    private Etudiant etudiant;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Etudiant createEntity(EntityManager em) {
        Etudiant etudiant = new Etudiant()
            .numIdentifiant(DEFAULT_NUM_IDENTIFIANT)
            .dateNaissance(DEFAULT_DATE_NAISSANCE)
            .lieuNaissance(DEFAULT_LIEU_NAISSANCE)
            .sexe(DEFAULT_SEXE)
            .ine(DEFAULT_INE)
            .telephone(DEFAULT_TELEPHONE)
            .nomDuMari(DEFAULT_NOM_DU_MARI)
            .regionDeNaissance(DEFAULT_REGION_DE_NAISSANCE)
            .prenom2(DEFAULT_PRENOM_2)
            .prenom3(DEFAULT_PRENOM_3)
            .paysDeNaissance(DEFAULT_PAYS_DE_NAISSANCE)
            .nationalite(DEFAULT_NATIONALITE)
            .addresseADakar(DEFAULT_ADDRESSE_A_DAKAR)
            .boitePostal(DEFAULT_BOITE_POSTAL)
            .portable(DEFAULT_PORTABLE)
            .activiteSalariee(DEFAULT_ACTIVITE_SALARIEE)
            .categorieSocioprofessionnelle(DEFAULT_CATEGORIE_SOCIOPROFESSIONNELLE)
            .situationFamiliale(DEFAULT_SITUATION_FAMILIALE)
            .nombreEnfant(DEFAULT_NOMBRE_ENFANT)
            .bourse(DEFAULT_BOURSE)
            .natureBourse(DEFAULT_NATURE_BOURSE)
            .montantBourse(DEFAULT_MONTANT_BOURSE)
            .organismeBoursier(DEFAULT_ORGANISME_BOURSIER)
            .serieDiplome(DEFAULT_SERIE_DIPLOME)
            .anneeDiplome(DEFAULT_ANNEE_DIPLOME)
            .mentionDiplome(DEFAULT_MENTION_DIPLOME)
            .lieuDiplome(DEFAULT_LIEU_DIPLOME)
            .serieDuelDuesDutBts(DEFAULT_SERIE_DUEL_DUES_DUT_BTS)
            .anneeDuelDuesDutBts(DEFAULT_ANNEE_DUEL_DUES_DUT_BTS)
            .mentionDuelDuesDutBts(DEFAULT_MENTION_DUEL_DUES_DUT_BTS)
            .lieuDuelDuesDutBts(DEFAULT_LIEU_DUEL_DUES_DUT_BTS)
            .serieLicenceComplete(DEFAULT_SERIE_LICENCE_COMPLETE)
            .anneeLicenceComplete(DEFAULT_ANNEE_LICENCE_COMPLETE)
            .mentionLicenceComplete(DEFAULT_MENTION_LICENCE_COMPLETE)
            .lieuLicenceComplete(DEFAULT_LIEU_LICENCE_COMPLETE)
            .serieMaster(DEFAULT_SERIE_MASTER)
            .anneeMaster(DEFAULT_ANNEE_MASTER)
            .mentionMaster(DEFAULT_MENTION_MASTER)
            .lieu(DEFAULT_LIEU)
            .serieDoctorat(DEFAULT_SERIE_DOCTORAT)
            .anneeDoctorat(DEFAULT_ANNEE_DOCTORAT)
            .mentionDoctorat(DEFAULT_MENTION_DOCTORAT)
            .lieuDoctorat(DEFAULT_LIEU_DOCTORAT)
            .nomPersonneAContacter(DEFAULT_NOM_PERSONNE_A_CONTACTER)
            .nomDuMariPersonneAContacter(DEFAULT_NOM_DU_MARI_PERSONNE_A_CONTACTER)
            .lienDeParentePersonneAContacter(DEFAULT_LIEN_DE_PARENTE_PERSONNE_A_CONTACTER)
            .adressePersonneAContacter(DEFAULT_ADRESSE_PERSONNE_A_CONTACTER)
            .rueQuartierPersonneAContacter(DEFAULT_RUE_QUARTIER_PERSONNE_A_CONTACTER)
            .villePersonneAContacter(DEFAULT_VILLE_PERSONNE_A_CONTACTER)
            .telephonePersonneAContacter(DEFAULT_TELEPHONE_PERSONNE_A_CONTACTER)
            .prenomPersonneAContacter(DEFAULT_PRENOM_PERSONNE_A_CONTACTER)
            .prenom2PersonneAContacter(DEFAULT_PRENOM_2_PERSONNE_A_CONTACTER)
            .prenom3PersonneAContacter(DEFAULT_PRENOM_3_PERSONNE_A_CONTACTER)
            .boitePostalePersonneAContacter(DEFAULT_BOITE_POSTALE_PERSONNE_A_CONTACTER)
            .portPersonneAContacter(DEFAULT_PORT_PERSONNE_A_CONTACTER)
            .faxPersonneAContacter(DEFAULT_FAX_PERSONNE_A_CONTACTER)
            .emailPersonneAContacter(DEFAULT_EMAIL_PERSONNE_A_CONTACTER)
            .responsableEstEtudiant(DEFAULT_RESPONSABLE_EST_ETUDIANT)
            .personneAContacter(DEFAULT_PERSONNE_A_CONTACTER)
            .statusEtudiant(DEFAULT_STATUS_ETUDIANT);
        // Add required entity
        Inscription inscription;
        if (TestUtil.findAll(em, Inscription.class).isEmpty()) {
            inscription = InscriptionResourceIT.createEntity(em);
            em.persist(inscription);
            em.flush();
        } else {
            inscription = TestUtil.findAll(em, Inscription.class).get(0);
        }
        etudiant.getEtudiants().add(inscription);
        return etudiant;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Etudiant createUpdatedEntity(EntityManager em) {
        Etudiant etudiant = new Etudiant()
            .numIdentifiant(UPDATED_NUM_IDENTIFIANT)
            .dateNaissance(UPDATED_DATE_NAISSANCE)
            .lieuNaissance(UPDATED_LIEU_NAISSANCE)
            .sexe(UPDATED_SEXE)
            .ine(UPDATED_INE)
            .telephone(UPDATED_TELEPHONE)
            .nomDuMari(UPDATED_NOM_DU_MARI)
            .regionDeNaissance(UPDATED_REGION_DE_NAISSANCE)
            .prenom2(UPDATED_PRENOM_2)
            .prenom3(UPDATED_PRENOM_3)
            .paysDeNaissance(UPDATED_PAYS_DE_NAISSANCE)
            .nationalite(UPDATED_NATIONALITE)
            .addresseADakar(UPDATED_ADDRESSE_A_DAKAR)
            .boitePostal(UPDATED_BOITE_POSTAL)
            .portable(UPDATED_PORTABLE)
            .activiteSalariee(UPDATED_ACTIVITE_SALARIEE)
            .categorieSocioprofessionnelle(UPDATED_CATEGORIE_SOCIOPROFESSIONNELLE)
            .situationFamiliale(UPDATED_SITUATION_FAMILIALE)
            .nombreEnfant(UPDATED_NOMBRE_ENFANT)
            .bourse(UPDATED_BOURSE)
            .natureBourse(UPDATED_NATURE_BOURSE)
            .montantBourse(UPDATED_MONTANT_BOURSE)
            .organismeBoursier(UPDATED_ORGANISME_BOURSIER)
            .serieDiplome(UPDATED_SERIE_DIPLOME)
            .anneeDiplome(UPDATED_ANNEE_DIPLOME)
            .mentionDiplome(UPDATED_MENTION_DIPLOME)
            .lieuDiplome(UPDATED_LIEU_DIPLOME)
            .serieDuelDuesDutBts(UPDATED_SERIE_DUEL_DUES_DUT_BTS)
            .anneeDuelDuesDutBts(UPDATED_ANNEE_DUEL_DUES_DUT_BTS)
            .mentionDuelDuesDutBts(UPDATED_MENTION_DUEL_DUES_DUT_BTS)
            .lieuDuelDuesDutBts(UPDATED_LIEU_DUEL_DUES_DUT_BTS)
            .serieLicenceComplete(UPDATED_SERIE_LICENCE_COMPLETE)
            .anneeLicenceComplete(UPDATED_ANNEE_LICENCE_COMPLETE)
            .mentionLicenceComplete(UPDATED_MENTION_LICENCE_COMPLETE)
            .lieuLicenceComplete(UPDATED_LIEU_LICENCE_COMPLETE)
            .serieMaster(UPDATED_SERIE_MASTER)
            .anneeMaster(UPDATED_ANNEE_MASTER)
            .mentionMaster(UPDATED_MENTION_MASTER)
            .lieu(UPDATED_LIEU)
            .serieDoctorat(UPDATED_SERIE_DOCTORAT)
            .anneeDoctorat(UPDATED_ANNEE_DOCTORAT)
            .mentionDoctorat(UPDATED_MENTION_DOCTORAT)
            .lieuDoctorat(UPDATED_LIEU_DOCTORAT)
            .nomPersonneAContacter(UPDATED_NOM_PERSONNE_A_CONTACTER)
            .nomDuMariPersonneAContacter(UPDATED_NOM_DU_MARI_PERSONNE_A_CONTACTER)
            .lienDeParentePersonneAContacter(UPDATED_LIEN_DE_PARENTE_PERSONNE_A_CONTACTER)
            .adressePersonneAContacter(UPDATED_ADRESSE_PERSONNE_A_CONTACTER)
            .rueQuartierPersonneAContacter(UPDATED_RUE_QUARTIER_PERSONNE_A_CONTACTER)
            .villePersonneAContacter(UPDATED_VILLE_PERSONNE_A_CONTACTER)
            .telephonePersonneAContacter(UPDATED_TELEPHONE_PERSONNE_A_CONTACTER)
            .prenomPersonneAContacter(UPDATED_PRENOM_PERSONNE_A_CONTACTER)
            .prenom2PersonneAContacter(UPDATED_PRENOM_2_PERSONNE_A_CONTACTER)
            .prenom3PersonneAContacter(UPDATED_PRENOM_3_PERSONNE_A_CONTACTER)
            .boitePostalePersonneAContacter(UPDATED_BOITE_POSTALE_PERSONNE_A_CONTACTER)
            .portPersonneAContacter(UPDATED_PORT_PERSONNE_A_CONTACTER)
            .faxPersonneAContacter(UPDATED_FAX_PERSONNE_A_CONTACTER)
            .emailPersonneAContacter(UPDATED_EMAIL_PERSONNE_A_CONTACTER)
            .responsableEstEtudiant(UPDATED_RESPONSABLE_EST_ETUDIANT)
            .personneAContacter(UPDATED_PERSONNE_A_CONTACTER)
            .statusEtudiant(UPDATED_STATUS_ETUDIANT);
        // Add required entity
        Inscription inscription;
        if (TestUtil.findAll(em, Inscription.class).isEmpty()) {
            inscription = InscriptionResourceIT.createUpdatedEntity(em);
            em.persist(inscription);
            em.flush();
        } else {
            inscription = TestUtil.findAll(em, Inscription.class).get(0);
        }
        etudiant.getEtudiants().add(inscription);
        return etudiant;
    }

    @BeforeEach
    public void initTest() {
        etudiant = createEntity(em);
    }

    @Test
    @Transactional
    public void createEtudiant() throws Exception {
        int databaseSizeBeforeCreate = etudiantRepository.findAll().size();
        // Create the Etudiant
        restEtudiantMockMvc.perform(post("/api/etudiants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etudiant)))
            .andExpect(status().isCreated());

        // Validate the Etudiant in the database
        List<Etudiant> etudiantList = etudiantRepository.findAll();
        assertThat(etudiantList).hasSize(databaseSizeBeforeCreate + 1);
        Etudiant testEtudiant = etudiantList.get(etudiantList.size() - 1);
        assertThat(testEtudiant.getNumIdentifiant()).isEqualTo(DEFAULT_NUM_IDENTIFIANT);
        assertThat(testEtudiant.getDateNaissance()).isEqualTo(DEFAULT_DATE_NAISSANCE);
        assertThat(testEtudiant.getLieuNaissance()).isEqualTo(DEFAULT_LIEU_NAISSANCE);
        assertThat(testEtudiant.getSexe()).isEqualTo(DEFAULT_SEXE);
        assertThat(testEtudiant.getIne()).isEqualTo(DEFAULT_INE);
        assertThat(testEtudiant.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testEtudiant.getNomDuMari()).isEqualTo(DEFAULT_NOM_DU_MARI);
        assertThat(testEtudiant.getRegionDeNaissance()).isEqualTo(DEFAULT_REGION_DE_NAISSANCE);
        assertThat(testEtudiant.getPrenom2()).isEqualTo(DEFAULT_PRENOM_2);
        assertThat(testEtudiant.getPrenom3()).isEqualTo(DEFAULT_PRENOM_3);
        assertThat(testEtudiant.getPaysDeNaissance()).isEqualTo(DEFAULT_PAYS_DE_NAISSANCE);
        assertThat(testEtudiant.getNationalite()).isEqualTo(DEFAULT_NATIONALITE);
        assertThat(testEtudiant.getAddresseADakar()).isEqualTo(DEFAULT_ADDRESSE_A_DAKAR);
        assertThat(testEtudiant.getBoitePostal()).isEqualTo(DEFAULT_BOITE_POSTAL);
        assertThat(testEtudiant.getPortable()).isEqualTo(DEFAULT_PORTABLE);
        assertThat(testEtudiant.isActiviteSalariee()).isEqualTo(DEFAULT_ACTIVITE_SALARIEE);
        assertThat(testEtudiant.getCategorieSocioprofessionnelle()).isEqualTo(DEFAULT_CATEGORIE_SOCIOPROFESSIONNELLE);
        assertThat(testEtudiant.getSituationFamiliale()).isEqualTo(DEFAULT_SITUATION_FAMILIALE);
        assertThat(testEtudiant.getNombreEnfant()).isEqualTo(DEFAULT_NOMBRE_ENFANT);
        assertThat(testEtudiant.getBourse()).isEqualTo(DEFAULT_BOURSE);
        assertThat(testEtudiant.getNatureBourse()).isEqualTo(DEFAULT_NATURE_BOURSE);
        assertThat(testEtudiant.getMontantBourse()).isEqualTo(DEFAULT_MONTANT_BOURSE);
        assertThat(testEtudiant.getOrganismeBoursier()).isEqualTo(DEFAULT_ORGANISME_BOURSIER);
        assertThat(testEtudiant.getSerieDiplome()).isEqualTo(DEFAULT_SERIE_DIPLOME);
        assertThat(testEtudiant.getAnneeDiplome()).isEqualTo(DEFAULT_ANNEE_DIPLOME);
        assertThat(testEtudiant.getMentionDiplome()).isEqualTo(DEFAULT_MENTION_DIPLOME);
        assertThat(testEtudiant.getLieuDiplome()).isEqualTo(DEFAULT_LIEU_DIPLOME);
        assertThat(testEtudiant.getSerieDuelDuesDutBts()).isEqualTo(DEFAULT_SERIE_DUEL_DUES_DUT_BTS);
        assertThat(testEtudiant.getAnneeDuelDuesDutBts()).isEqualTo(DEFAULT_ANNEE_DUEL_DUES_DUT_BTS);
        assertThat(testEtudiant.getMentionDuelDuesDutBts()).isEqualTo(DEFAULT_MENTION_DUEL_DUES_DUT_BTS);
        assertThat(testEtudiant.getLieuDuelDuesDutBts()).isEqualTo(DEFAULT_LIEU_DUEL_DUES_DUT_BTS);
        assertThat(testEtudiant.getSerieLicenceComplete()).isEqualTo(DEFAULT_SERIE_LICENCE_COMPLETE);
        assertThat(testEtudiant.getAnneeLicenceComplete()).isEqualTo(DEFAULT_ANNEE_LICENCE_COMPLETE);
        assertThat(testEtudiant.getMentionLicenceComplete()).isEqualTo(DEFAULT_MENTION_LICENCE_COMPLETE);
        assertThat(testEtudiant.getLieuLicenceComplete()).isEqualTo(DEFAULT_LIEU_LICENCE_COMPLETE);
        assertThat(testEtudiant.getSerieMaster()).isEqualTo(DEFAULT_SERIE_MASTER);
        assertThat(testEtudiant.getAnneeMaster()).isEqualTo(DEFAULT_ANNEE_MASTER);
        assertThat(testEtudiant.getMentionMaster()).isEqualTo(DEFAULT_MENTION_MASTER);
        assertThat(testEtudiant.getLieu()).isEqualTo(DEFAULT_LIEU);
        assertThat(testEtudiant.getSerieDoctorat()).isEqualTo(DEFAULT_SERIE_DOCTORAT);
        assertThat(testEtudiant.getAnneeDoctorat()).isEqualTo(DEFAULT_ANNEE_DOCTORAT);
        assertThat(testEtudiant.getMentionDoctorat()).isEqualTo(DEFAULT_MENTION_DOCTORAT);
        assertThat(testEtudiant.getLieuDoctorat()).isEqualTo(DEFAULT_LIEU_DOCTORAT);
        assertThat(testEtudiant.getNomPersonneAContacter()).isEqualTo(DEFAULT_NOM_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getNomDuMariPersonneAContacter()).isEqualTo(DEFAULT_NOM_DU_MARI_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getLienDeParentePersonneAContacter()).isEqualTo(DEFAULT_LIEN_DE_PARENTE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getAdressePersonneAContacter()).isEqualTo(DEFAULT_ADRESSE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getRueQuartierPersonneAContacter()).isEqualTo(DEFAULT_RUE_QUARTIER_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getVillePersonneAContacter()).isEqualTo(DEFAULT_VILLE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getTelephonePersonneAContacter()).isEqualTo(DEFAULT_TELEPHONE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getPrenomPersonneAContacter()).isEqualTo(DEFAULT_PRENOM_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getPrenom2PersonneAContacter()).isEqualTo(DEFAULT_PRENOM_2_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getPrenom3PersonneAContacter()).isEqualTo(DEFAULT_PRENOM_3_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getBoitePostalePersonneAContacter()).isEqualTo(DEFAULT_BOITE_POSTALE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getPortPersonneAContacter()).isEqualTo(DEFAULT_PORT_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getFaxPersonneAContacter()).isEqualTo(DEFAULT_FAX_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getEmailPersonneAContacter()).isEqualTo(DEFAULT_EMAIL_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.isResponsableEstEtudiant()).isEqualTo(DEFAULT_RESPONSABLE_EST_ETUDIANT);
        assertThat(testEtudiant.isPersonneAContacter()).isEqualTo(DEFAULT_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getStatusEtudiant()).isEqualTo(DEFAULT_STATUS_ETUDIANT);
    }

    @Test
    @Transactional
    public void createEtudiantWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = etudiantRepository.findAll().size();

        // Create the Etudiant with an existing ID
        etudiant.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEtudiantMockMvc.perform(post("/api/etudiants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etudiant)))
            .andExpect(status().isBadRequest());

        // Validate the Etudiant in the database
        List<Etudiant> etudiantList = etudiantRepository.findAll();
        assertThat(etudiantList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNumIdentifiantIsRequired() throws Exception {
        int databaseSizeBeforeTest = etudiantRepository.findAll().size();
        // set the field null
        etudiant.setNumIdentifiant(null);

        // Create the Etudiant, which fails.


        restEtudiantMockMvc.perform(post("/api/etudiants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etudiant)))
            .andExpect(status().isBadRequest());

        List<Etudiant> etudiantList = etudiantRepository.findAll();
        assertThat(etudiantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEtudiants() throws Exception {
        // Initialize the database
        etudiantRepository.saveAndFlush(etudiant);

        // Get all the etudiantList
        restEtudiantMockMvc.perform(get("/api/etudiants?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(etudiant.getId().intValue())))
            .andExpect(jsonPath("$.[*].numIdentifiant").value(hasItem(DEFAULT_NUM_IDENTIFIANT)))
            .andExpect(jsonPath("$.[*].dateNaissance").value(hasItem(DEFAULT_DATE_NAISSANCE.toString())))
            .andExpect(jsonPath("$.[*].lieuNaissance").value(hasItem(DEFAULT_LIEU_NAISSANCE)))
            .andExpect(jsonPath("$.[*].sexe").value(hasItem(DEFAULT_SEXE.toString())))
            .andExpect(jsonPath("$.[*].ine").value(hasItem(DEFAULT_INE)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].nomDuMari").value(hasItem(DEFAULT_NOM_DU_MARI)))
            .andExpect(jsonPath("$.[*].regionDeNaissance").value(hasItem(DEFAULT_REGION_DE_NAISSANCE)))
            .andExpect(jsonPath("$.[*].prenom2").value(hasItem(DEFAULT_PRENOM_2)))
            .andExpect(jsonPath("$.[*].prenom3").value(hasItem(DEFAULT_PRENOM_3)))
            .andExpect(jsonPath("$.[*].paysDeNaissance").value(hasItem(DEFAULT_PAYS_DE_NAISSANCE)))
            .andExpect(jsonPath("$.[*].nationalite").value(hasItem(DEFAULT_NATIONALITE)))
            .andExpect(jsonPath("$.[*].addresseADakar").value(hasItem(DEFAULT_ADDRESSE_A_DAKAR)))
            .andExpect(jsonPath("$.[*].boitePostal").value(hasItem(DEFAULT_BOITE_POSTAL)))
            .andExpect(jsonPath("$.[*].portable").value(hasItem(DEFAULT_PORTABLE)))
            .andExpect(jsonPath("$.[*].activiteSalariee").value(hasItem(DEFAULT_ACTIVITE_SALARIEE.booleanValue())))
            .andExpect(jsonPath("$.[*].categorieSocioprofessionnelle").value(hasItem(DEFAULT_CATEGORIE_SOCIOPROFESSIONNELLE)))
            .andExpect(jsonPath("$.[*].situationFamiliale").value(hasItem(DEFAULT_SITUATION_FAMILIALE)))
            .andExpect(jsonPath("$.[*].nombreEnfant").value(hasItem(DEFAULT_NOMBRE_ENFANT)))
            .andExpect(jsonPath("$.[*].bourse").value(hasItem(DEFAULT_BOURSE.toString())))
            .andExpect(jsonPath("$.[*].natureBourse").value(hasItem(DEFAULT_NATURE_BOURSE.toString())))
            .andExpect(jsonPath("$.[*].montantBourse").value(hasItem(DEFAULT_MONTANT_BOURSE)))
            .andExpect(jsonPath("$.[*].organismeBoursier").value(hasItem(DEFAULT_ORGANISME_BOURSIER)))
            .andExpect(jsonPath("$.[*].serieDiplome").value(hasItem(DEFAULT_SERIE_DIPLOME)))
            .andExpect(jsonPath("$.[*].anneeDiplome").value(hasItem(DEFAULT_ANNEE_DIPLOME)))
            .andExpect(jsonPath("$.[*].mentionDiplome").value(hasItem(DEFAULT_MENTION_DIPLOME)))
            .andExpect(jsonPath("$.[*].lieuDiplome").value(hasItem(DEFAULT_LIEU_DIPLOME)))
            .andExpect(jsonPath("$.[*].serieDuelDuesDutBts").value(hasItem(DEFAULT_SERIE_DUEL_DUES_DUT_BTS)))
            .andExpect(jsonPath("$.[*].anneeDuelDuesDutBts").value(hasItem(DEFAULT_ANNEE_DUEL_DUES_DUT_BTS)))
            .andExpect(jsonPath("$.[*].mentionDuelDuesDutBts").value(hasItem(DEFAULT_MENTION_DUEL_DUES_DUT_BTS)))
            .andExpect(jsonPath("$.[*].lieuDuelDuesDutBts").value(hasItem(DEFAULT_LIEU_DUEL_DUES_DUT_BTS)))
            .andExpect(jsonPath("$.[*].serieLicenceComplete").value(hasItem(DEFAULT_SERIE_LICENCE_COMPLETE)))
            .andExpect(jsonPath("$.[*].anneeLicenceComplete").value(hasItem(DEFAULT_ANNEE_LICENCE_COMPLETE)))
            .andExpect(jsonPath("$.[*].mentionLicenceComplete").value(hasItem(DEFAULT_MENTION_LICENCE_COMPLETE)))
            .andExpect(jsonPath("$.[*].lieuLicenceComplete").value(hasItem(DEFAULT_LIEU_LICENCE_COMPLETE)))
            .andExpect(jsonPath("$.[*].serieMaster").value(hasItem(DEFAULT_SERIE_MASTER)))
            .andExpect(jsonPath("$.[*].anneeMaster").value(hasItem(DEFAULT_ANNEE_MASTER)))
            .andExpect(jsonPath("$.[*].mentionMaster").value(hasItem(DEFAULT_MENTION_MASTER)))
            .andExpect(jsonPath("$.[*].lieu").value(hasItem(DEFAULT_LIEU)))
            .andExpect(jsonPath("$.[*].serieDoctorat").value(hasItem(DEFAULT_SERIE_DOCTORAT)))
            .andExpect(jsonPath("$.[*].anneeDoctorat").value(hasItem(DEFAULT_ANNEE_DOCTORAT)))
            .andExpect(jsonPath("$.[*].mentionDoctorat").value(hasItem(DEFAULT_MENTION_DOCTORAT)))
            .andExpect(jsonPath("$.[*].lieuDoctorat").value(hasItem(DEFAULT_LIEU_DOCTORAT)))
            .andExpect(jsonPath("$.[*].nomPersonneAContacter").value(hasItem(DEFAULT_NOM_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].nomDuMariPersonneAContacter").value(hasItem(DEFAULT_NOM_DU_MARI_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].lienDeParentePersonneAContacter").value(hasItem(DEFAULT_LIEN_DE_PARENTE_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].adressePersonneAContacter").value(hasItem(DEFAULT_ADRESSE_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].rueQuartierPersonneAContacter").value(hasItem(DEFAULT_RUE_QUARTIER_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].villePersonneAContacter").value(hasItem(DEFAULT_VILLE_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].telephonePersonneAContacter").value(hasItem(DEFAULT_TELEPHONE_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].prenomPersonneAContacter").value(hasItem(DEFAULT_PRENOM_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].prenom2PersonneAContacter").value(hasItem(DEFAULT_PRENOM_2_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].prenom3PersonneAContacter").value(hasItem(DEFAULT_PRENOM_3_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].boitePostalePersonneAContacter").value(hasItem(DEFAULT_BOITE_POSTALE_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].portPersonneAContacter").value(hasItem(DEFAULT_PORT_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].faxPersonneAContacter").value(hasItem(DEFAULT_FAX_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].emailPersonneAContacter").value(hasItem(DEFAULT_EMAIL_PERSONNE_A_CONTACTER)))
            .andExpect(jsonPath("$.[*].responsableEstEtudiant").value(hasItem(DEFAULT_RESPONSABLE_EST_ETUDIANT.booleanValue())))
            .andExpect(jsonPath("$.[*].personneAContacter").value(hasItem(DEFAULT_PERSONNE_A_CONTACTER.booleanValue())))
            .andExpect(jsonPath("$.[*].statusEtudiant").value(hasItem(DEFAULT_STATUS_ETUDIANT.toString())));
    }
    
    @Test
    @Transactional
    public void getEtudiant() throws Exception {
        // Initialize the database
        etudiantRepository.saveAndFlush(etudiant);

        // Get the etudiant
        restEtudiantMockMvc.perform(get("/api/etudiants/{id}", etudiant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(etudiant.getId().intValue()))
            .andExpect(jsonPath("$.numIdentifiant").value(DEFAULT_NUM_IDENTIFIANT))
            .andExpect(jsonPath("$.dateNaissance").value(DEFAULT_DATE_NAISSANCE.toString()))
            .andExpect(jsonPath("$.lieuNaissance").value(DEFAULT_LIEU_NAISSANCE))
            .andExpect(jsonPath("$.sexe").value(DEFAULT_SEXE.toString()))
            .andExpect(jsonPath("$.ine").value(DEFAULT_INE))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE))
            .andExpect(jsonPath("$.nomDuMari").value(DEFAULT_NOM_DU_MARI))
            .andExpect(jsonPath("$.regionDeNaissance").value(DEFAULT_REGION_DE_NAISSANCE))
            .andExpect(jsonPath("$.prenom2").value(DEFAULT_PRENOM_2))
            .andExpect(jsonPath("$.prenom3").value(DEFAULT_PRENOM_3))
            .andExpect(jsonPath("$.paysDeNaissance").value(DEFAULT_PAYS_DE_NAISSANCE))
            .andExpect(jsonPath("$.nationalite").value(DEFAULT_NATIONALITE))
            .andExpect(jsonPath("$.addresseADakar").value(DEFAULT_ADDRESSE_A_DAKAR))
            .andExpect(jsonPath("$.boitePostal").value(DEFAULT_BOITE_POSTAL))
            .andExpect(jsonPath("$.portable").value(DEFAULT_PORTABLE))
            .andExpect(jsonPath("$.activiteSalariee").value(DEFAULT_ACTIVITE_SALARIEE.booleanValue()))
            .andExpect(jsonPath("$.categorieSocioprofessionnelle").value(DEFAULT_CATEGORIE_SOCIOPROFESSIONNELLE))
            .andExpect(jsonPath("$.situationFamiliale").value(DEFAULT_SITUATION_FAMILIALE))
            .andExpect(jsonPath("$.nombreEnfant").value(DEFAULT_NOMBRE_ENFANT))
            .andExpect(jsonPath("$.bourse").value(DEFAULT_BOURSE.toString()))
            .andExpect(jsonPath("$.natureBourse").value(DEFAULT_NATURE_BOURSE.toString()))
            .andExpect(jsonPath("$.montantBourse").value(DEFAULT_MONTANT_BOURSE))
            .andExpect(jsonPath("$.organismeBoursier").value(DEFAULT_ORGANISME_BOURSIER))
            .andExpect(jsonPath("$.serieDiplome").value(DEFAULT_SERIE_DIPLOME))
            .andExpect(jsonPath("$.anneeDiplome").value(DEFAULT_ANNEE_DIPLOME))
            .andExpect(jsonPath("$.mentionDiplome").value(DEFAULT_MENTION_DIPLOME))
            .andExpect(jsonPath("$.lieuDiplome").value(DEFAULT_LIEU_DIPLOME))
            .andExpect(jsonPath("$.serieDuelDuesDutBts").value(DEFAULT_SERIE_DUEL_DUES_DUT_BTS))
            .andExpect(jsonPath("$.anneeDuelDuesDutBts").value(DEFAULT_ANNEE_DUEL_DUES_DUT_BTS))
            .andExpect(jsonPath("$.mentionDuelDuesDutBts").value(DEFAULT_MENTION_DUEL_DUES_DUT_BTS))
            .andExpect(jsonPath("$.lieuDuelDuesDutBts").value(DEFAULT_LIEU_DUEL_DUES_DUT_BTS))
            .andExpect(jsonPath("$.serieLicenceComplete").value(DEFAULT_SERIE_LICENCE_COMPLETE))
            .andExpect(jsonPath("$.anneeLicenceComplete").value(DEFAULT_ANNEE_LICENCE_COMPLETE))
            .andExpect(jsonPath("$.mentionLicenceComplete").value(DEFAULT_MENTION_LICENCE_COMPLETE))
            .andExpect(jsonPath("$.lieuLicenceComplete").value(DEFAULT_LIEU_LICENCE_COMPLETE))
            .andExpect(jsonPath("$.serieMaster").value(DEFAULT_SERIE_MASTER))
            .andExpect(jsonPath("$.anneeMaster").value(DEFAULT_ANNEE_MASTER))
            .andExpect(jsonPath("$.mentionMaster").value(DEFAULT_MENTION_MASTER))
            .andExpect(jsonPath("$.lieu").value(DEFAULT_LIEU))
            .andExpect(jsonPath("$.serieDoctorat").value(DEFAULT_SERIE_DOCTORAT))
            .andExpect(jsonPath("$.anneeDoctorat").value(DEFAULT_ANNEE_DOCTORAT))
            .andExpect(jsonPath("$.mentionDoctorat").value(DEFAULT_MENTION_DOCTORAT))
            .andExpect(jsonPath("$.lieuDoctorat").value(DEFAULT_LIEU_DOCTORAT))
            .andExpect(jsonPath("$.nomPersonneAContacter").value(DEFAULT_NOM_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.nomDuMariPersonneAContacter").value(DEFAULT_NOM_DU_MARI_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.lienDeParentePersonneAContacter").value(DEFAULT_LIEN_DE_PARENTE_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.adressePersonneAContacter").value(DEFAULT_ADRESSE_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.rueQuartierPersonneAContacter").value(DEFAULT_RUE_QUARTIER_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.villePersonneAContacter").value(DEFAULT_VILLE_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.telephonePersonneAContacter").value(DEFAULT_TELEPHONE_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.prenomPersonneAContacter").value(DEFAULT_PRENOM_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.prenom2PersonneAContacter").value(DEFAULT_PRENOM_2_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.prenom3PersonneAContacter").value(DEFAULT_PRENOM_3_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.boitePostalePersonneAContacter").value(DEFAULT_BOITE_POSTALE_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.portPersonneAContacter").value(DEFAULT_PORT_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.faxPersonneAContacter").value(DEFAULT_FAX_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.emailPersonneAContacter").value(DEFAULT_EMAIL_PERSONNE_A_CONTACTER))
            .andExpect(jsonPath("$.responsableEstEtudiant").value(DEFAULT_RESPONSABLE_EST_ETUDIANT.booleanValue()))
            .andExpect(jsonPath("$.personneAContacter").value(DEFAULT_PERSONNE_A_CONTACTER.booleanValue()))
            .andExpect(jsonPath("$.statusEtudiant").value(DEFAULT_STATUS_ETUDIANT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingEtudiant() throws Exception {
        // Get the etudiant
        restEtudiantMockMvc.perform(get("/api/etudiants/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEtudiant() throws Exception {
        // Initialize the database
        etudiantRepository.saveAndFlush(etudiant);

        int databaseSizeBeforeUpdate = etudiantRepository.findAll().size();

        // Update the etudiant
        Etudiant updatedEtudiant = etudiantRepository.findById(etudiant.getId()).get();
        // Disconnect from session so that the updates on updatedEtudiant are not directly saved in db
        em.detach(updatedEtudiant);
        updatedEtudiant
            .numIdentifiant(UPDATED_NUM_IDENTIFIANT)
            .dateNaissance(UPDATED_DATE_NAISSANCE)
            .lieuNaissance(UPDATED_LIEU_NAISSANCE)
            .sexe(UPDATED_SEXE)
            .ine(UPDATED_INE)
            .telephone(UPDATED_TELEPHONE)
            .nomDuMari(UPDATED_NOM_DU_MARI)
            .regionDeNaissance(UPDATED_REGION_DE_NAISSANCE)
            .prenom2(UPDATED_PRENOM_2)
            .prenom3(UPDATED_PRENOM_3)
            .paysDeNaissance(UPDATED_PAYS_DE_NAISSANCE)
            .nationalite(UPDATED_NATIONALITE)
            .addresseADakar(UPDATED_ADDRESSE_A_DAKAR)
            .boitePostal(UPDATED_BOITE_POSTAL)
            .portable(UPDATED_PORTABLE)
            .activiteSalariee(UPDATED_ACTIVITE_SALARIEE)
            .categorieSocioprofessionnelle(UPDATED_CATEGORIE_SOCIOPROFESSIONNELLE)
            .situationFamiliale(UPDATED_SITUATION_FAMILIALE)
            .nombreEnfant(UPDATED_NOMBRE_ENFANT)
            .bourse(UPDATED_BOURSE)
            .natureBourse(UPDATED_NATURE_BOURSE)
            .montantBourse(UPDATED_MONTANT_BOURSE)
            .organismeBoursier(UPDATED_ORGANISME_BOURSIER)
            .serieDiplome(UPDATED_SERIE_DIPLOME)
            .anneeDiplome(UPDATED_ANNEE_DIPLOME)
            .mentionDiplome(UPDATED_MENTION_DIPLOME)
            .lieuDiplome(UPDATED_LIEU_DIPLOME)
            .serieDuelDuesDutBts(UPDATED_SERIE_DUEL_DUES_DUT_BTS)
            .anneeDuelDuesDutBts(UPDATED_ANNEE_DUEL_DUES_DUT_BTS)
            .mentionDuelDuesDutBts(UPDATED_MENTION_DUEL_DUES_DUT_BTS)
            .lieuDuelDuesDutBts(UPDATED_LIEU_DUEL_DUES_DUT_BTS)
            .serieLicenceComplete(UPDATED_SERIE_LICENCE_COMPLETE)
            .anneeLicenceComplete(UPDATED_ANNEE_LICENCE_COMPLETE)
            .mentionLicenceComplete(UPDATED_MENTION_LICENCE_COMPLETE)
            .lieuLicenceComplete(UPDATED_LIEU_LICENCE_COMPLETE)
            .serieMaster(UPDATED_SERIE_MASTER)
            .anneeMaster(UPDATED_ANNEE_MASTER)
            .mentionMaster(UPDATED_MENTION_MASTER)
            .lieu(UPDATED_LIEU)
            .serieDoctorat(UPDATED_SERIE_DOCTORAT)
            .anneeDoctorat(UPDATED_ANNEE_DOCTORAT)
            .mentionDoctorat(UPDATED_MENTION_DOCTORAT)
            .lieuDoctorat(UPDATED_LIEU_DOCTORAT)
            .nomPersonneAContacter(UPDATED_NOM_PERSONNE_A_CONTACTER)
            .nomDuMariPersonneAContacter(UPDATED_NOM_DU_MARI_PERSONNE_A_CONTACTER)
            .lienDeParentePersonneAContacter(UPDATED_LIEN_DE_PARENTE_PERSONNE_A_CONTACTER)
            .adressePersonneAContacter(UPDATED_ADRESSE_PERSONNE_A_CONTACTER)
            .rueQuartierPersonneAContacter(UPDATED_RUE_QUARTIER_PERSONNE_A_CONTACTER)
            .villePersonneAContacter(UPDATED_VILLE_PERSONNE_A_CONTACTER)
            .telephonePersonneAContacter(UPDATED_TELEPHONE_PERSONNE_A_CONTACTER)
            .prenomPersonneAContacter(UPDATED_PRENOM_PERSONNE_A_CONTACTER)
            .prenom2PersonneAContacter(UPDATED_PRENOM_2_PERSONNE_A_CONTACTER)
            .prenom3PersonneAContacter(UPDATED_PRENOM_3_PERSONNE_A_CONTACTER)
            .boitePostalePersonneAContacter(UPDATED_BOITE_POSTALE_PERSONNE_A_CONTACTER)
            .portPersonneAContacter(UPDATED_PORT_PERSONNE_A_CONTACTER)
            .faxPersonneAContacter(UPDATED_FAX_PERSONNE_A_CONTACTER)
            .emailPersonneAContacter(UPDATED_EMAIL_PERSONNE_A_CONTACTER)
            .responsableEstEtudiant(UPDATED_RESPONSABLE_EST_ETUDIANT)
            .personneAContacter(UPDATED_PERSONNE_A_CONTACTER)
            .statusEtudiant(UPDATED_STATUS_ETUDIANT);

        restEtudiantMockMvc.perform(put("/api/etudiants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedEtudiant)))
            .andExpect(status().isOk());

        // Validate the Etudiant in the database
        List<Etudiant> etudiantList = etudiantRepository.findAll();
        assertThat(etudiantList).hasSize(databaseSizeBeforeUpdate);
        Etudiant testEtudiant = etudiantList.get(etudiantList.size() - 1);
        assertThat(testEtudiant.getNumIdentifiant()).isEqualTo(UPDATED_NUM_IDENTIFIANT);
        assertThat(testEtudiant.getDateNaissance()).isEqualTo(UPDATED_DATE_NAISSANCE);
        assertThat(testEtudiant.getLieuNaissance()).isEqualTo(UPDATED_LIEU_NAISSANCE);
        assertThat(testEtudiant.getSexe()).isEqualTo(UPDATED_SEXE);
        assertThat(testEtudiant.getIne()).isEqualTo(UPDATED_INE);
        assertThat(testEtudiant.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testEtudiant.getNomDuMari()).isEqualTo(UPDATED_NOM_DU_MARI);
        assertThat(testEtudiant.getRegionDeNaissance()).isEqualTo(UPDATED_REGION_DE_NAISSANCE);
        assertThat(testEtudiant.getPrenom2()).isEqualTo(UPDATED_PRENOM_2);
        assertThat(testEtudiant.getPrenom3()).isEqualTo(UPDATED_PRENOM_3);
        assertThat(testEtudiant.getPaysDeNaissance()).isEqualTo(UPDATED_PAYS_DE_NAISSANCE);
        assertThat(testEtudiant.getNationalite()).isEqualTo(UPDATED_NATIONALITE);
        assertThat(testEtudiant.getAddresseADakar()).isEqualTo(UPDATED_ADDRESSE_A_DAKAR);
        assertThat(testEtudiant.getBoitePostal()).isEqualTo(UPDATED_BOITE_POSTAL);
        assertThat(testEtudiant.getPortable()).isEqualTo(UPDATED_PORTABLE);
        assertThat(testEtudiant.isActiviteSalariee()).isEqualTo(UPDATED_ACTIVITE_SALARIEE);
        assertThat(testEtudiant.getCategorieSocioprofessionnelle()).isEqualTo(UPDATED_CATEGORIE_SOCIOPROFESSIONNELLE);
        assertThat(testEtudiant.getSituationFamiliale()).isEqualTo(UPDATED_SITUATION_FAMILIALE);
        assertThat(testEtudiant.getNombreEnfant()).isEqualTo(UPDATED_NOMBRE_ENFANT);
        assertThat(testEtudiant.getBourse()).isEqualTo(UPDATED_BOURSE);
        assertThat(testEtudiant.getNatureBourse()).isEqualTo(UPDATED_NATURE_BOURSE);
        assertThat(testEtudiant.getMontantBourse()).isEqualTo(UPDATED_MONTANT_BOURSE);
        assertThat(testEtudiant.getOrganismeBoursier()).isEqualTo(UPDATED_ORGANISME_BOURSIER);
        assertThat(testEtudiant.getSerieDiplome()).isEqualTo(UPDATED_SERIE_DIPLOME);
        assertThat(testEtudiant.getAnneeDiplome()).isEqualTo(UPDATED_ANNEE_DIPLOME);
        assertThat(testEtudiant.getMentionDiplome()).isEqualTo(UPDATED_MENTION_DIPLOME);
        assertThat(testEtudiant.getLieuDiplome()).isEqualTo(UPDATED_LIEU_DIPLOME);
        assertThat(testEtudiant.getSerieDuelDuesDutBts()).isEqualTo(UPDATED_SERIE_DUEL_DUES_DUT_BTS);
        assertThat(testEtudiant.getAnneeDuelDuesDutBts()).isEqualTo(UPDATED_ANNEE_DUEL_DUES_DUT_BTS);
        assertThat(testEtudiant.getMentionDuelDuesDutBts()).isEqualTo(UPDATED_MENTION_DUEL_DUES_DUT_BTS);
        assertThat(testEtudiant.getLieuDuelDuesDutBts()).isEqualTo(UPDATED_LIEU_DUEL_DUES_DUT_BTS);
        assertThat(testEtudiant.getSerieLicenceComplete()).isEqualTo(UPDATED_SERIE_LICENCE_COMPLETE);
        assertThat(testEtudiant.getAnneeLicenceComplete()).isEqualTo(UPDATED_ANNEE_LICENCE_COMPLETE);
        assertThat(testEtudiant.getMentionLicenceComplete()).isEqualTo(UPDATED_MENTION_LICENCE_COMPLETE);
        assertThat(testEtudiant.getLieuLicenceComplete()).isEqualTo(UPDATED_LIEU_LICENCE_COMPLETE);
        assertThat(testEtudiant.getSerieMaster()).isEqualTo(UPDATED_SERIE_MASTER);
        assertThat(testEtudiant.getAnneeMaster()).isEqualTo(UPDATED_ANNEE_MASTER);
        assertThat(testEtudiant.getMentionMaster()).isEqualTo(UPDATED_MENTION_MASTER);
        assertThat(testEtudiant.getLieu()).isEqualTo(UPDATED_LIEU);
        assertThat(testEtudiant.getSerieDoctorat()).isEqualTo(UPDATED_SERIE_DOCTORAT);
        assertThat(testEtudiant.getAnneeDoctorat()).isEqualTo(UPDATED_ANNEE_DOCTORAT);
        assertThat(testEtudiant.getMentionDoctorat()).isEqualTo(UPDATED_MENTION_DOCTORAT);
        assertThat(testEtudiant.getLieuDoctorat()).isEqualTo(UPDATED_LIEU_DOCTORAT);
        assertThat(testEtudiant.getNomPersonneAContacter()).isEqualTo(UPDATED_NOM_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getNomDuMariPersonneAContacter()).isEqualTo(UPDATED_NOM_DU_MARI_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getLienDeParentePersonneAContacter()).isEqualTo(UPDATED_LIEN_DE_PARENTE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getAdressePersonneAContacter()).isEqualTo(UPDATED_ADRESSE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getRueQuartierPersonneAContacter()).isEqualTo(UPDATED_RUE_QUARTIER_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getVillePersonneAContacter()).isEqualTo(UPDATED_VILLE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getTelephonePersonneAContacter()).isEqualTo(UPDATED_TELEPHONE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getPrenomPersonneAContacter()).isEqualTo(UPDATED_PRENOM_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getPrenom2PersonneAContacter()).isEqualTo(UPDATED_PRENOM_2_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getPrenom3PersonneAContacter()).isEqualTo(UPDATED_PRENOM_3_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getBoitePostalePersonneAContacter()).isEqualTo(UPDATED_BOITE_POSTALE_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getPortPersonneAContacter()).isEqualTo(UPDATED_PORT_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getFaxPersonneAContacter()).isEqualTo(UPDATED_FAX_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getEmailPersonneAContacter()).isEqualTo(UPDATED_EMAIL_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.isResponsableEstEtudiant()).isEqualTo(UPDATED_RESPONSABLE_EST_ETUDIANT);
        assertThat(testEtudiant.isPersonneAContacter()).isEqualTo(UPDATED_PERSONNE_A_CONTACTER);
        assertThat(testEtudiant.getStatusEtudiant()).isEqualTo(UPDATED_STATUS_ETUDIANT);
    }

    @Test
    @Transactional
    public void updateNonExistingEtudiant() throws Exception {
        int databaseSizeBeforeUpdate = etudiantRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEtudiantMockMvc.perform(put("/api/etudiants")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(etudiant)))
            .andExpect(status().isBadRequest());

        // Validate the Etudiant in the database
        List<Etudiant> etudiantList = etudiantRepository.findAll();
        assertThat(etudiantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEtudiant() throws Exception {
        // Initialize the database
        etudiantRepository.saveAndFlush(etudiant);

        int databaseSizeBeforeDelete = etudiantRepository.findAll().size();

        // Delete the etudiant
        restEtudiantMockMvc.perform(delete("/api/etudiants/{id}", etudiant.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Etudiant> etudiantList = etudiantRepository.findAll();
        assertThat(etudiantList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
