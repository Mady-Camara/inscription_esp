package sn.esp.inscription.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.esp.inscription.web.rest.TestUtil;

public class AgentScolariteTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentScolarite.class);
        AgentScolarite agentScolarite1 = new AgentScolarite();
        agentScolarite1.setId(1L);
        AgentScolarite agentScolarite2 = new AgentScolarite();
        agentScolarite2.setId(agentScolarite1.getId());
        assertThat(agentScolarite1).isEqualTo(agentScolarite2);
        agentScolarite2.setId(2L);
        assertThat(agentScolarite1).isNotEqualTo(agentScolarite2);
        agentScolarite1.setId(null);
        assertThat(agentScolarite1).isNotEqualTo(agentScolarite2);
    }
}
