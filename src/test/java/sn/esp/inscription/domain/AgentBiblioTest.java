package sn.esp.inscription.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.esp.inscription.web.rest.TestUtil;

public class AgentBiblioTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AgentBiblio.class);
        AgentBiblio agentBiblio1 = new AgentBiblio();
        agentBiblio1.setId(1L);
        AgentBiblio agentBiblio2 = new AgentBiblio();
        agentBiblio2.setId(agentBiblio1.getId());
        assertThat(agentBiblio1).isEqualTo(agentBiblio2);
        agentBiblio2.setId(2L);
        assertThat(agentBiblio1).isNotEqualTo(agentBiblio2);
        agentBiblio1.setId(null);
        assertThat(agentBiblio1).isNotEqualTo(agentBiblio2);
    }
}
