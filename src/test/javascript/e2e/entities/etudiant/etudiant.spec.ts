import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  EtudiantComponentsPage,
  /* EtudiantDeleteDialog, */
  EtudiantUpdatePage,
} from './etudiant.page-object';

const expect = chai.expect;

describe('Etudiant e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let etudiantComponentsPage: EtudiantComponentsPage;
  let etudiantUpdatePage: EtudiantUpdatePage;
  /* let etudiantDeleteDialog: EtudiantDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Etudiants', async () => {
    await navBarPage.goToEntity('etudiant');
    etudiantComponentsPage = new EtudiantComponentsPage();
    await browser.wait(ec.visibilityOf(etudiantComponentsPage.title), 5000);
    expect(await etudiantComponentsPage.getTitle()).to.eq('inscriptionEspApp.etudiant.home.title');
    await browser.wait(ec.or(ec.visibilityOf(etudiantComponentsPage.entities), ec.visibilityOf(etudiantComponentsPage.noResult)), 1000);
  });

  it('should load create Etudiant page', async () => {
    await etudiantComponentsPage.clickOnCreateButton();
    etudiantUpdatePage = new EtudiantUpdatePage();
    expect(await etudiantUpdatePage.getPageTitle()).to.eq('inscriptionEspApp.etudiant.home.createOrEditLabel');
    await etudiantUpdatePage.cancel();
  });

  /* it('should create and save Etudiants', async () => {
        const nbButtonsBeforeCreate = await etudiantComponentsPage.countDeleteButtons();

        await etudiantComponentsPage.clickOnCreateButton();

        await promise.all([
            etudiantUpdatePage.setNumIdentifiantInput('numIdentifiant'),
            etudiantUpdatePage.setDateNaissanceInput('2000-12-31'),
            etudiantUpdatePage.setLieuNaissanceInput('lieuNaissance'),
            etudiantUpdatePage.sexeSelectLastOption(),
            etudiantUpdatePage.setIneInput('ine'),
            etudiantUpdatePage.setTelephoneInput('telephone'),
            etudiantUpdatePage.setNomDuMariInput('nomDuMari'),
            etudiantUpdatePage.setRegionDeNaissanceInput('regionDeNaissance'),
            etudiantUpdatePage.setPrenom2Input('prenom2'),
            etudiantUpdatePage.setPrenom3Input('prenom3'),
            etudiantUpdatePage.setPaysDeNaissanceInput('paysDeNaissance'),
            etudiantUpdatePage.setNationaliteInput('nationalite'),
            etudiantUpdatePage.setAddresseADakarInput('addresseADakar'),
            etudiantUpdatePage.setBoitePostalInput('boitePostal'),
            etudiantUpdatePage.setPortableInput('portable'),
            etudiantUpdatePage.setCategorieSocioprofessionnelleInput('categorieSocioprofessionnelle'),
            etudiantUpdatePage.setSituationFamilialeInput('situationFamiliale'),
            etudiantUpdatePage.setNombreEnfantInput('5'),
            etudiantUpdatePage.bourseSelectLastOption(),
            etudiantUpdatePage.natureBourseSelectLastOption(),
            etudiantUpdatePage.setMontantBourseInput('5'),
            etudiantUpdatePage.setOrganismeBoursierInput('organismeBoursier'),
            etudiantUpdatePage.setSerieDiplomeInput('serieDiplome'),
            etudiantUpdatePage.setAnneeDiplomeInput('anneeDiplome'),
            etudiantUpdatePage.setMentionDiplomeInput('mentionDiplome'),
            etudiantUpdatePage.setLieuDiplomeInput('lieuDiplome'),
            etudiantUpdatePage.setSerieDuelDuesDutBtsInput('serieDuelDuesDutBts'),
            etudiantUpdatePage.setAnneeDuelDuesDutBtsInput('anneeDuelDuesDutBts'),
            etudiantUpdatePage.setMentionDuelDuesDutBtsInput('mentionDuelDuesDutBts'),
            etudiantUpdatePage.setLieuDuelDuesDutBtsInput('lieuDuelDuesDutBts'),
            etudiantUpdatePage.setSerieLicenceCompleteInput('serieLicenceComplete'),
            etudiantUpdatePage.setAnneeLicenceCompleteInput('anneeLicenceComplete'),
            etudiantUpdatePage.setMentionLicenceCompleteInput('mentionLicenceComplete'),
            etudiantUpdatePage.setLieuLicenceCompleteInput('lieuLicenceComplete'),
            etudiantUpdatePage.setSerieMasterInput('serieMaster'),
            etudiantUpdatePage.setAnneeMasterInput('anneeMaster'),
            etudiantUpdatePage.setMentionMasterInput('mentionMaster'),
            etudiantUpdatePage.setLieuInput('lieu'),
            etudiantUpdatePage.setSerieDoctoratInput('serieDoctorat'),
            etudiantUpdatePage.setAnneeDoctoratInput('anneeDoctorat'),
            etudiantUpdatePage.setMentionDoctoratInput('mentionDoctorat'),
            etudiantUpdatePage.setLieuDoctoratInput('lieuDoctorat'),
            etudiantUpdatePage.setNomPersonneAContacterInput('nomPersonneAContacter'),
            etudiantUpdatePage.setNomDuMariPersonneAContacterInput('nomDuMariPersonneAContacter'),
            etudiantUpdatePage.setLienDeParentePersonneAContacterInput('lienDeParentePersonneAContacter'),
            etudiantUpdatePage.setAdressePersonneAContacterInput('adressePersonneAContacter'),
            etudiantUpdatePage.setRueQuartierPersonneAContacterInput('rueQuartierPersonneAContacter'),
            etudiantUpdatePage.setVillePersonneAContacterInput('villePersonneAContacter'),
            etudiantUpdatePage.setTelephonePersonneAContacterInput('telephonePersonneAContacter'),
            etudiantUpdatePage.setPrenomPersonneAContacterInput('prenomPersonneAContacter'),
            etudiantUpdatePage.setPrenom2PersonneAContacterInput('prenom2PersonneAContacter'),
            etudiantUpdatePage.setPrenom3PersonneAContacterInput('prenom3PersonneAContacter'),
            etudiantUpdatePage.setBoitePostalePersonneAContacterInput('boitePostalePersonneAContacter'),
            etudiantUpdatePage.setPortPersonneAContacterInput('portPersonneAContacter'),
            etudiantUpdatePage.setFaxPersonneAContacterInput('faxPersonneAContacter'),
            etudiantUpdatePage.setEmailPersonneAContacterInput('emailPersonneAContacter'),
            etudiantUpdatePage.statusEtudiantSelectLastOption(),
            etudiantUpdatePage.userSelectLastOption(),
        ]);

        expect(await etudiantUpdatePage.getNumIdentifiantInput()).to.eq('numIdentifiant', 'Expected NumIdentifiant value to be equals to numIdentifiant');
        expect(await etudiantUpdatePage.getDateNaissanceInput()).to.eq('2000-12-31', 'Expected dateNaissance value to be equals to 2000-12-31');
        expect(await etudiantUpdatePage.getLieuNaissanceInput()).to.eq('lieuNaissance', 'Expected LieuNaissance value to be equals to lieuNaissance');
        expect(await etudiantUpdatePage.getIneInput()).to.eq('ine', 'Expected Ine value to be equals to ine');
        expect(await etudiantUpdatePage.getTelephoneInput()).to.eq('telephone', 'Expected Telephone value to be equals to telephone');
        expect(await etudiantUpdatePage.getNomDuMariInput()).to.eq('nomDuMari', 'Expected NomDuMari value to be equals to nomDuMari');
        expect(await etudiantUpdatePage.getRegionDeNaissanceInput()).to.eq('regionDeNaissance', 'Expected RegionDeNaissance value to be equals to regionDeNaissance');
        expect(await etudiantUpdatePage.getPrenom2Input()).to.eq('prenom2', 'Expected Prenom2 value to be equals to prenom2');
        expect(await etudiantUpdatePage.getPrenom3Input()).to.eq('prenom3', 'Expected Prenom3 value to be equals to prenom3');
        expect(await etudiantUpdatePage.getPaysDeNaissanceInput()).to.eq('paysDeNaissance', 'Expected PaysDeNaissance value to be equals to paysDeNaissance');
        expect(await etudiantUpdatePage.getNationaliteInput()).to.eq('nationalite', 'Expected Nationalite value to be equals to nationalite');
        expect(await etudiantUpdatePage.getAddresseADakarInput()).to.eq('addresseADakar', 'Expected AddresseADakar value to be equals to addresseADakar');
        expect(await etudiantUpdatePage.getBoitePostalInput()).to.eq('boitePostal', 'Expected BoitePostal value to be equals to boitePostal');
        expect(await etudiantUpdatePage.getPortableInput()).to.eq('portable', 'Expected Portable value to be equals to portable');
        const selectedActiviteSalariee = etudiantUpdatePage.getActiviteSalarieeInput();
        if (await selectedActiviteSalariee.isSelected()) {
            await etudiantUpdatePage.getActiviteSalarieeInput().click();
            expect(await etudiantUpdatePage.getActiviteSalarieeInput().isSelected(), 'Expected activiteSalariee not to be selected').to.be.false;
        } else {
            await etudiantUpdatePage.getActiviteSalarieeInput().click();
            expect(await etudiantUpdatePage.getActiviteSalarieeInput().isSelected(), 'Expected activiteSalariee to be selected').to.be.true;
        }
        expect(await etudiantUpdatePage.getCategorieSocioprofessionnelleInput()).to.eq('categorieSocioprofessionnelle', 'Expected CategorieSocioprofessionnelle value to be equals to categorieSocioprofessionnelle');
        expect(await etudiantUpdatePage.getSituationFamilialeInput()).to.eq('situationFamiliale', 'Expected SituationFamiliale value to be equals to situationFamiliale');
        expect(await etudiantUpdatePage.getNombreEnfantInput()).to.eq('5', 'Expected nombreEnfant value to be equals to 5');
        expect(await etudiantUpdatePage.getMontantBourseInput()).to.eq('5', 'Expected montantBourse value to be equals to 5');
        expect(await etudiantUpdatePage.getOrganismeBoursierInput()).to.eq('organismeBoursier', 'Expected OrganismeBoursier value to be equals to organismeBoursier');
        expect(await etudiantUpdatePage.getSerieDiplomeInput()).to.eq('serieDiplome', 'Expected SerieDiplome value to be equals to serieDiplome');
        expect(await etudiantUpdatePage.getAnneeDiplomeInput()).to.eq('anneeDiplome', 'Expected AnneeDiplome value to be equals to anneeDiplome');
        expect(await etudiantUpdatePage.getMentionDiplomeInput()).to.eq('mentionDiplome', 'Expected MentionDiplome value to be equals to mentionDiplome');
        expect(await etudiantUpdatePage.getLieuDiplomeInput()).to.eq('lieuDiplome', 'Expected LieuDiplome value to be equals to lieuDiplome');
        expect(await etudiantUpdatePage.getSerieDuelDuesDutBtsInput()).to.eq('serieDuelDuesDutBts', 'Expected SerieDuelDuesDutBts value to be equals to serieDuelDuesDutBts');
        expect(await etudiantUpdatePage.getAnneeDuelDuesDutBtsInput()).to.eq('anneeDuelDuesDutBts', 'Expected AnneeDuelDuesDutBts value to be equals to anneeDuelDuesDutBts');
        expect(await etudiantUpdatePage.getMentionDuelDuesDutBtsInput()).to.eq('mentionDuelDuesDutBts', 'Expected MentionDuelDuesDutBts value to be equals to mentionDuelDuesDutBts');
        expect(await etudiantUpdatePage.getLieuDuelDuesDutBtsInput()).to.eq('lieuDuelDuesDutBts', 'Expected LieuDuelDuesDutBts value to be equals to lieuDuelDuesDutBts');
        expect(await etudiantUpdatePage.getSerieLicenceCompleteInput()).to.eq('serieLicenceComplete', 'Expected SerieLicenceComplete value to be equals to serieLicenceComplete');
        expect(await etudiantUpdatePage.getAnneeLicenceCompleteInput()).to.eq('anneeLicenceComplete', 'Expected AnneeLicenceComplete value to be equals to anneeLicenceComplete');
        expect(await etudiantUpdatePage.getMentionLicenceCompleteInput()).to.eq('mentionLicenceComplete', 'Expected MentionLicenceComplete value to be equals to mentionLicenceComplete');
        expect(await etudiantUpdatePage.getLieuLicenceCompleteInput()).to.eq('lieuLicenceComplete', 'Expected LieuLicenceComplete value to be equals to lieuLicenceComplete');
        expect(await etudiantUpdatePage.getSerieMasterInput()).to.eq('serieMaster', 'Expected SerieMaster value to be equals to serieMaster');
        expect(await etudiantUpdatePage.getAnneeMasterInput()).to.eq('anneeMaster', 'Expected AnneeMaster value to be equals to anneeMaster');
        expect(await etudiantUpdatePage.getMentionMasterInput()).to.eq('mentionMaster', 'Expected MentionMaster value to be equals to mentionMaster');
        expect(await etudiantUpdatePage.getLieuInput()).to.eq('lieu', 'Expected Lieu value to be equals to lieu');
        expect(await etudiantUpdatePage.getSerieDoctoratInput()).to.eq('serieDoctorat', 'Expected SerieDoctorat value to be equals to serieDoctorat');
        expect(await etudiantUpdatePage.getAnneeDoctoratInput()).to.eq('anneeDoctorat', 'Expected AnneeDoctorat value to be equals to anneeDoctorat');
        expect(await etudiantUpdatePage.getMentionDoctoratInput()).to.eq('mentionDoctorat', 'Expected MentionDoctorat value to be equals to mentionDoctorat');
        expect(await etudiantUpdatePage.getLieuDoctoratInput()).to.eq('lieuDoctorat', 'Expected LieuDoctorat value to be equals to lieuDoctorat');
        expect(await etudiantUpdatePage.getNomPersonneAContacterInput()).to.eq('nomPersonneAContacter', 'Expected NomPersonneAContacter value to be equals to nomPersonneAContacter');
        expect(await etudiantUpdatePage.getNomDuMariPersonneAContacterInput()).to.eq('nomDuMariPersonneAContacter', 'Expected NomDuMariPersonneAContacter value to be equals to nomDuMariPersonneAContacter');
        expect(await etudiantUpdatePage.getLienDeParentePersonneAContacterInput()).to.eq('lienDeParentePersonneAContacter', 'Expected LienDeParentePersonneAContacter value to be equals to lienDeParentePersonneAContacter');
        expect(await etudiantUpdatePage.getAdressePersonneAContacterInput()).to.eq('adressePersonneAContacter', 'Expected AdressePersonneAContacter value to be equals to adressePersonneAContacter');
        expect(await etudiantUpdatePage.getRueQuartierPersonneAContacterInput()).to.eq('rueQuartierPersonneAContacter', 'Expected RueQuartierPersonneAContacter value to be equals to rueQuartierPersonneAContacter');
        expect(await etudiantUpdatePage.getVillePersonneAContacterInput()).to.eq('villePersonneAContacter', 'Expected VillePersonneAContacter value to be equals to villePersonneAContacter');
        expect(await etudiantUpdatePage.getTelephonePersonneAContacterInput()).to.eq('telephonePersonneAContacter', 'Expected TelephonePersonneAContacter value to be equals to telephonePersonneAContacter');
        expect(await etudiantUpdatePage.getPrenomPersonneAContacterInput()).to.eq('prenomPersonneAContacter', 'Expected PrenomPersonneAContacter value to be equals to prenomPersonneAContacter');
        expect(await etudiantUpdatePage.getPrenom2PersonneAContacterInput()).to.eq('prenom2PersonneAContacter', 'Expected Prenom2PersonneAContacter value to be equals to prenom2PersonneAContacter');
        expect(await etudiantUpdatePage.getPrenom3PersonneAContacterInput()).to.eq('prenom3PersonneAContacter', 'Expected Prenom3PersonneAContacter value to be equals to prenom3PersonneAContacter');
        expect(await etudiantUpdatePage.getBoitePostalePersonneAContacterInput()).to.eq('boitePostalePersonneAContacter', 'Expected BoitePostalePersonneAContacter value to be equals to boitePostalePersonneAContacter');
        expect(await etudiantUpdatePage.getPortPersonneAContacterInput()).to.eq('portPersonneAContacter', 'Expected PortPersonneAContacter value to be equals to portPersonneAContacter');
        expect(await etudiantUpdatePage.getFaxPersonneAContacterInput()).to.eq('faxPersonneAContacter', 'Expected FaxPersonneAContacter value to be equals to faxPersonneAContacter');
        expect(await etudiantUpdatePage.getEmailPersonneAContacterInput()).to.eq('emailPersonneAContacter', 'Expected EmailPersonneAContacter value to be equals to emailPersonneAContacter');
        const selectedResponsableEstEtudiant = etudiantUpdatePage.getResponsableEstEtudiantInput();
        if (await selectedResponsableEstEtudiant.isSelected()) {
            await etudiantUpdatePage.getResponsableEstEtudiantInput().click();
            expect(await etudiantUpdatePage.getResponsableEstEtudiantInput().isSelected(), 'Expected responsableEstEtudiant not to be selected').to.be.false;
        } else {
            await etudiantUpdatePage.getResponsableEstEtudiantInput().click();
            expect(await etudiantUpdatePage.getResponsableEstEtudiantInput().isSelected(), 'Expected responsableEstEtudiant to be selected').to.be.true;
        }
        const selectedPersonneAContacter = etudiantUpdatePage.getPersonneAContacterInput();
        if (await selectedPersonneAContacter.isSelected()) {
            await etudiantUpdatePage.getPersonneAContacterInput().click();
            expect(await etudiantUpdatePage.getPersonneAContacterInput().isSelected(), 'Expected personneAContacter not to be selected').to.be.false;
        } else {
            await etudiantUpdatePage.getPersonneAContacterInput().click();
            expect(await etudiantUpdatePage.getPersonneAContacterInput().isSelected(), 'Expected personneAContacter to be selected').to.be.true;
        }

        await etudiantUpdatePage.save();
        expect(await etudiantUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await etudiantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Etudiant', async () => {
        const nbButtonsBeforeDelete = await etudiantComponentsPage.countDeleteButtons();
        await etudiantComponentsPage.clickOnLastDeleteButton();

        etudiantDeleteDialog = new EtudiantDeleteDialog();
        expect(await etudiantDeleteDialog.getDialogTitle())
            .to.eq('inscriptionEspApp.etudiant.delete.question');
        await etudiantDeleteDialog.clickOnConfirmButton();

        expect(await etudiantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
