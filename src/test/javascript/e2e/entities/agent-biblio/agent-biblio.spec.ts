import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AgentBiblioComponentsPage, AgentBiblioDeleteDialog, AgentBiblioUpdatePage } from './agent-biblio.page-object';

const expect = chai.expect;

describe('AgentBiblio e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let agentBiblioComponentsPage: AgentBiblioComponentsPage;
  let agentBiblioUpdatePage: AgentBiblioUpdatePage;
  let agentBiblioDeleteDialog: AgentBiblioDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AgentBiblios', async () => {
    await navBarPage.goToEntity('agent-biblio');
    agentBiblioComponentsPage = new AgentBiblioComponentsPage();
    await browser.wait(ec.visibilityOf(agentBiblioComponentsPage.title), 5000);
    expect(await agentBiblioComponentsPage.getTitle()).to.eq('inscriptionEspApp.agentBiblio.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(agentBiblioComponentsPage.entities), ec.visibilityOf(agentBiblioComponentsPage.noResult)),
      1000
    );
  });

  it('should load create AgentBiblio page', async () => {
    await agentBiblioComponentsPage.clickOnCreateButton();
    agentBiblioUpdatePage = new AgentBiblioUpdatePage();
    expect(await agentBiblioUpdatePage.getPageTitle()).to.eq('inscriptionEspApp.agentBiblio.home.createOrEditLabel');
    await agentBiblioUpdatePage.cancel();
  });

  it('should create and save AgentBiblios', async () => {
    const nbButtonsBeforeCreate = await agentBiblioComponentsPage.countDeleteButtons();

    await agentBiblioComponentsPage.clickOnCreateButton();

    await promise.all([
      agentBiblioUpdatePage.setMatriculeInput('matricule'),
      agentBiblioUpdatePage.setEmailInput('email'),
      agentBiblioUpdatePage.setMotDePasseInput('motDePasse'),
      agentBiblioUpdatePage.setNomInput('nom'),
      agentBiblioUpdatePage.setPrenomInput('prenom'),
      agentBiblioUpdatePage.userSelectLastOption(),
    ]);

    expect(await agentBiblioUpdatePage.getMatriculeInput()).to.eq('matricule', 'Expected Matricule value to be equals to matricule');
    expect(await agentBiblioUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
    expect(await agentBiblioUpdatePage.getMotDePasseInput()).to.eq('motDePasse', 'Expected MotDePasse value to be equals to motDePasse');
    expect(await agentBiblioUpdatePage.getNomInput()).to.eq('nom', 'Expected Nom value to be equals to nom');
    expect(await agentBiblioUpdatePage.getPrenomInput()).to.eq('prenom', 'Expected Prenom value to be equals to prenom');

    await agentBiblioUpdatePage.save();
    expect(await agentBiblioUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await agentBiblioComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last AgentBiblio', async () => {
    const nbButtonsBeforeDelete = await agentBiblioComponentsPage.countDeleteButtons();
    await agentBiblioComponentsPage.clickOnLastDeleteButton();

    agentBiblioDeleteDialog = new AgentBiblioDeleteDialog();
    expect(await agentBiblioDeleteDialog.getDialogTitle()).to.eq('inscriptionEspApp.agentBiblio.delete.question');
    await agentBiblioDeleteDialog.clickOnConfirmButton();

    expect(await agentBiblioComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
