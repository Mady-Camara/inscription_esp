import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  AnneeUniversitaireComponentsPage,
  AnneeUniversitaireDeleteDialog,
  AnneeUniversitaireUpdatePage,
} from './annee-universitaire.page-object';

const expect = chai.expect;

describe('AnneeUniversitaire e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let anneeUniversitaireComponentsPage: AnneeUniversitaireComponentsPage;
  let anneeUniversitaireUpdatePage: AnneeUniversitaireUpdatePage;
  let anneeUniversitaireDeleteDialog: AnneeUniversitaireDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AnneeUniversitaires', async () => {
    await navBarPage.goToEntity('annee-universitaire');
    anneeUniversitaireComponentsPage = new AnneeUniversitaireComponentsPage();
    await browser.wait(ec.visibilityOf(anneeUniversitaireComponentsPage.title), 5000);
    expect(await anneeUniversitaireComponentsPage.getTitle()).to.eq('inscriptionEspApp.anneeUniversitaire.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(anneeUniversitaireComponentsPage.entities), ec.visibilityOf(anneeUniversitaireComponentsPage.noResult)),
      1000
    );
  });

  it('should load create AnneeUniversitaire page', async () => {
    await anneeUniversitaireComponentsPage.clickOnCreateButton();
    anneeUniversitaireUpdatePage = new AnneeUniversitaireUpdatePage();
    expect(await anneeUniversitaireUpdatePage.getPageTitle()).to.eq('inscriptionEspApp.anneeUniversitaire.home.createOrEditLabel');
    await anneeUniversitaireUpdatePage.cancel();
  });

  it('should create and save AnneeUniversitaires', async () => {
    const nbButtonsBeforeCreate = await anneeUniversitaireComponentsPage.countDeleteButtons();

    await anneeUniversitaireComponentsPage.clickOnCreateButton();

    await promise.all([anneeUniversitaireUpdatePage.setLibelleInput('libelle')]);

    const selectedIsActive = anneeUniversitaireUpdatePage.getIsActiveInput();
    if (await selectedIsActive.isSelected()) {
      await anneeUniversitaireUpdatePage.getIsActiveInput().click();
      expect(await anneeUniversitaireUpdatePage.getIsActiveInput().isSelected(), 'Expected isActive not to be selected').to.be.false;
    } else {
      await anneeUniversitaireUpdatePage.getIsActiveInput().click();
      expect(await anneeUniversitaireUpdatePage.getIsActiveInput().isSelected(), 'Expected isActive to be selected').to.be.true;
    }
    expect(await anneeUniversitaireUpdatePage.getLibelleInput()).to.eq('libelle', 'Expected Libelle value to be equals to libelle');

    await anneeUniversitaireUpdatePage.save();
    expect(await anneeUniversitaireUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await anneeUniversitaireComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last AnneeUniversitaire', async () => {
    const nbButtonsBeforeDelete = await anneeUniversitaireComponentsPage.countDeleteButtons();
    await anneeUniversitaireComponentsPage.clickOnLastDeleteButton();

    anneeUniversitaireDeleteDialog = new AnneeUniversitaireDeleteDialog();
    expect(await anneeUniversitaireDeleteDialog.getDialogTitle()).to.eq('inscriptionEspApp.anneeUniversitaire.delete.question');
    await anneeUniversitaireDeleteDialog.clickOnConfirmButton();

    expect(await anneeUniversitaireComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
