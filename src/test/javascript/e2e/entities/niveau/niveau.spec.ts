import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { NiveauComponentsPage, NiveauDeleteDialog, NiveauUpdatePage } from './niveau.page-object';

const expect = chai.expect;

describe('Niveau e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let niveauComponentsPage: NiveauComponentsPage;
  let niveauUpdatePage: NiveauUpdatePage;
  let niveauDeleteDialog: NiveauDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Niveaus', async () => {
    await navBarPage.goToEntity('niveau');
    niveauComponentsPage = new NiveauComponentsPage();
    await browser.wait(ec.visibilityOf(niveauComponentsPage.title), 5000);
    expect(await niveauComponentsPage.getTitle()).to.eq('inscriptionEspApp.niveau.home.title');
    await browser.wait(ec.or(ec.visibilityOf(niveauComponentsPage.entities), ec.visibilityOf(niveauComponentsPage.noResult)), 1000);
  });

  it('should load create Niveau page', async () => {
    await niveauComponentsPage.clickOnCreateButton();
    niveauUpdatePage = new NiveauUpdatePage();
    expect(await niveauUpdatePage.getPageTitle()).to.eq('inscriptionEspApp.niveau.home.createOrEditLabel');
    await niveauUpdatePage.cancel();
  });

  it('should create and save Niveaus', async () => {
    const nbButtonsBeforeCreate = await niveauComponentsPage.countDeleteButtons();

    await niveauComponentsPage.clickOnCreateButton();

    await promise.all([
      niveauUpdatePage.setCodeNiveauInput('codeNiveau'),
      niveauUpdatePage.setLibelleLongInput('libelleLong'),
      niveauUpdatePage.formationSelectLastOption(),
    ]);

    expect(await niveauUpdatePage.getCodeNiveauInput()).to.eq('codeNiveau', 'Expected CodeNiveau value to be equals to codeNiveau');
    expect(await niveauUpdatePage.getLibelleLongInput()).to.eq('libelleLong', 'Expected LibelleLong value to be equals to libelleLong');

    await niveauUpdatePage.save();
    expect(await niveauUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await niveauComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Niveau', async () => {
    const nbButtonsBeforeDelete = await niveauComponentsPage.countDeleteButtons();
    await niveauComponentsPage.clickOnLastDeleteButton();

    niveauDeleteDialog = new NiveauDeleteDialog();
    expect(await niveauDeleteDialog.getDialogTitle()).to.eq('inscriptionEspApp.niveau.delete.question');
    await niveauDeleteDialog.clickOnConfirmButton();

    expect(await niveauComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
