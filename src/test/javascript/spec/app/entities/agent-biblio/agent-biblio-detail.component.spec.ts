import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { InscriptionEspTestModule } from '../../../test.module';
import { AgentBiblioDetailComponent } from 'app/entities/agent-biblio/agent-biblio-detail.component';
import { AgentBiblio } from 'app/shared/model/agent-biblio.model';

describe('Component Tests', () => {
  describe('AgentBiblio Management Detail Component', () => {
    let comp: AgentBiblioDetailComponent;
    let fixture: ComponentFixture<AgentBiblioDetailComponent>;
    const route = ({ data: of({ agentBiblio: new AgentBiblio(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [InscriptionEspTestModule],
        declarations: [AgentBiblioDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AgentBiblioDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AgentBiblioDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load agentBiblio on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.agentBiblio).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
