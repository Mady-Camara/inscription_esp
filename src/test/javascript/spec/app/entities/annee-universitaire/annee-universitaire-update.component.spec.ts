import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { InscriptionEspTestModule } from '../../../test.module';
import { AnneeUniversitaireUpdateComponent } from 'app/entities/annee-universitaire/annee-universitaire-update.component';
import { AnneeUniversitaireService } from 'app/entities/annee-universitaire/annee-universitaire.service';
import { AnneeUniversitaire } from 'app/shared/model/annee-universitaire.model';

describe('Component Tests', () => {
  describe('AnneeUniversitaire Management Update Component', () => {
    let comp: AnneeUniversitaireUpdateComponent;
    let fixture: ComponentFixture<AnneeUniversitaireUpdateComponent>;
    let service: AnneeUniversitaireService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [InscriptionEspTestModule],
        declarations: [AnneeUniversitaireUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AnneeUniversitaireUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AnneeUniversitaireUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AnneeUniversitaireService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AnneeUniversitaire(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AnneeUniversitaire();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
