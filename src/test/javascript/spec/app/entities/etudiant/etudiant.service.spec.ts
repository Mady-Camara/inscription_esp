import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { EtudiantService } from 'app/entities/etudiant/etudiant.service';
import { IEtudiant, Etudiant } from 'app/shared/model/etudiant.model';
import { EnumSexe } from 'app/shared/model/enumerations/enum-sexe.model';
import { EnumBourse } from 'app/shared/model/enumerations/enum-bourse.model';
import { EnumNatureBourse } from 'app/shared/model/enumerations/enum-nature-bourse.model';
import { EnumStatusEtudiant } from 'app/shared/model/enumerations/enum-status-etudiant.model';

describe('Service Tests', () => {
  describe('Etudiant Service', () => {
    let injector: TestBed;
    let service: EtudiantService;
    let httpMock: HttpTestingController;
    let elemDefault: IEtudiant;
    let expectedResult: IEtudiant | IEtudiant[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(EtudiantService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Etudiant(
        0,
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        EnumSexe.Masculin,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        'AAAAAAA',
        'AAAAAAA',
        0,
        EnumBourse.NONBOURSIER,
        EnumNatureBourse.NATIONALE,
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        false,
        EnumStatusEtudiant.REGIMENORMAL
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateNaissance: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Etudiant', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateNaissance: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateNaissance: currentDate,
          },
          returnedFromService
        );

        service.create(new Etudiant()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Etudiant', () => {
        const returnedFromService = Object.assign(
          {
            numIdentifiant: 'BBBBBB',
            dateNaissance: currentDate.format(DATE_FORMAT),
            lieuNaissance: 'BBBBBB',
            sexe: 'BBBBBB',
            ine: 'BBBBBB',
            telephone: 'BBBBBB',
            nomDuMari: 'BBBBBB',
            regionDeNaissance: 'BBBBBB',
            prenom2: 'BBBBBB',
            prenom3: 'BBBBBB',
            paysDeNaissance: 'BBBBBB',
            nationalite: 'BBBBBB',
            addresseADakar: 'BBBBBB',
            boitePostal: 'BBBBBB',
            portable: 'BBBBBB',
            activiteSalariee: true,
            categorieSocioprofessionnelle: 'BBBBBB',
            situationFamiliale: 'BBBBBB',
            nombreEnfant: 1,
            bourse: 'BBBBBB',
            natureBourse: 'BBBBBB',
            montantBourse: 1,
            organismeBoursier: 'BBBBBB',
            serieDiplome: 'BBBBBB',
            anneeDiplome: 'BBBBBB',
            mentionDiplome: 'BBBBBB',
            lieuDiplome: 'BBBBBB',
            serieDuelDuesDutBts: 'BBBBBB',
            anneeDuelDuesDutBts: 'BBBBBB',
            mentionDuelDuesDutBts: 'BBBBBB',
            lieuDuelDuesDutBts: 'BBBBBB',
            serieLicenceComplete: 'BBBBBB',
            anneeLicenceComplete: 'BBBBBB',
            mentionLicenceComplete: 'BBBBBB',
            lieuLicenceComplete: 'BBBBBB',
            serieMaster: 'BBBBBB',
            anneeMaster: 'BBBBBB',
            mentionMaster: 'BBBBBB',
            lieu: 'BBBBBB',
            serieDoctorat: 'BBBBBB',
            anneeDoctorat: 'BBBBBB',
            mentionDoctorat: 'BBBBBB',
            lieuDoctorat: 'BBBBBB',
            nomPersonneAContacter: 'BBBBBB',
            nomDuMariPersonneAContacter: 'BBBBBB',
            lienDeParentePersonneAContacter: 'BBBBBB',
            adressePersonneAContacter: 'BBBBBB',
            rueQuartierPersonneAContacter: 'BBBBBB',
            villePersonneAContacter: 'BBBBBB',
            telephonePersonneAContacter: 'BBBBBB',
            prenomPersonneAContacter: 'BBBBBB',
            prenom2PersonneAContacter: 'BBBBBB',
            prenom3PersonneAContacter: 'BBBBBB',
            boitePostalePersonneAContacter: 'BBBBBB',
            portPersonneAContacter: 'BBBBBB',
            faxPersonneAContacter: 'BBBBBB',
            emailPersonneAContacter: 'BBBBBB',
            responsableEstEtudiant: true,
            personneAContacter: true,
            statusEtudiant: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateNaissance: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Etudiant', () => {
        const returnedFromService = Object.assign(
          {
            numIdentifiant: 'BBBBBB',
            dateNaissance: currentDate.format(DATE_FORMAT),
            lieuNaissance: 'BBBBBB',
            sexe: 'BBBBBB',
            ine: 'BBBBBB',
            telephone: 'BBBBBB',
            nomDuMari: 'BBBBBB',
            regionDeNaissance: 'BBBBBB',
            prenom2: 'BBBBBB',
            prenom3: 'BBBBBB',
            paysDeNaissance: 'BBBBBB',
            nationalite: 'BBBBBB',
            addresseADakar: 'BBBBBB',
            boitePostal: 'BBBBBB',
            portable: 'BBBBBB',
            activiteSalariee: true,
            categorieSocioprofessionnelle: 'BBBBBB',
            situationFamiliale: 'BBBBBB',
            nombreEnfant: 1,
            bourse: 'BBBBBB',
            natureBourse: 'BBBBBB',
            montantBourse: 1,
            organismeBoursier: 'BBBBBB',
            serieDiplome: 'BBBBBB',
            anneeDiplome: 'BBBBBB',
            mentionDiplome: 'BBBBBB',
            lieuDiplome: 'BBBBBB',
            serieDuelDuesDutBts: 'BBBBBB',
            anneeDuelDuesDutBts: 'BBBBBB',
            mentionDuelDuesDutBts: 'BBBBBB',
            lieuDuelDuesDutBts: 'BBBBBB',
            serieLicenceComplete: 'BBBBBB',
            anneeLicenceComplete: 'BBBBBB',
            mentionLicenceComplete: 'BBBBBB',
            lieuLicenceComplete: 'BBBBBB',
            serieMaster: 'BBBBBB',
            anneeMaster: 'BBBBBB',
            mentionMaster: 'BBBBBB',
            lieu: 'BBBBBB',
            serieDoctorat: 'BBBBBB',
            anneeDoctorat: 'BBBBBB',
            mentionDoctorat: 'BBBBBB',
            lieuDoctorat: 'BBBBBB',
            nomPersonneAContacter: 'BBBBBB',
            nomDuMariPersonneAContacter: 'BBBBBB',
            lienDeParentePersonneAContacter: 'BBBBBB',
            adressePersonneAContacter: 'BBBBBB',
            rueQuartierPersonneAContacter: 'BBBBBB',
            villePersonneAContacter: 'BBBBBB',
            telephonePersonneAContacter: 'BBBBBB',
            prenomPersonneAContacter: 'BBBBBB',
            prenom2PersonneAContacter: 'BBBBBB',
            prenom3PersonneAContacter: 'BBBBBB',
            boitePostalePersonneAContacter: 'BBBBBB',
            portPersonneAContacter: 'BBBBBB',
            faxPersonneAContacter: 'BBBBBB',
            emailPersonneAContacter: 'BBBBBB',
            responsableEstEtudiant: true,
            personneAContacter: true,
            statusEtudiant: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateNaissance: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Etudiant', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
